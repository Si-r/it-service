<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimRolePermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_role_permission', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('permission_id')->comment('สิทธิ์การเข้าใช้งาน')->unsigned();
            $table->string('role')->comment('สิทธิ์การเข้าถึง');


            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('dim_role_permission')->insert(
            array(
              'permission_id' => '1',
              'role' => 'ManageUser',
            )
        );

        DB::table('dim_role_permission')->insert(
            array(
              'permission_id' => '1',
              'role' => 'ManageUserEdit',
            )
        );

        DB::table('dim_role_permission')->insert(
            array(
              'permission_id' => '1',
              'role' => 'ManageUserDelete',
            )
        );

        DB::table('dim_role_permission')->insert(
            array(
              'permission_id' => '1',
              'role' => 'ManagePermission',
            )
        );

        DB::table('dim_role_permission')->insert(
            array(
              'permission_id' => '1',
              'role' => 'ManagePermissionEdit',
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_role_permission');
    }
}
