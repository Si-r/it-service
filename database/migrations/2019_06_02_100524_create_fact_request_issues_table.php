<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactRequestIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_request_issues', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('รหัสข้อมูลตาราง');
            $table->bigInteger('client_id')->comment('รหัสข้อมูลผู้สร้าง');
            $table->bigInteger('request_issues_id')->comment('รหัสข้อมูลปัญหา');
            $table->bigInteger('equipment_id')->comment('รหัสข้อมูลอุปกรณ์');

            $table->string('issues_name')->comment('ชื่อ');
            $table->text('issues_comment')->comment('ข้อคิดเห็น');

            $table->enum('issues_level', ['Problems', 'Incidents', 'None'])->default('None')->comment('ตัวเลือก ปัญหา');

            $table->bigInteger('modify_id')->nullable()->comment('รหัสข้อมูลแก้ไข');
            $table->enum('modify_status', ['Pending', 'Successful', 'Unsuccessful'])->default('Pending')->comment('รหัสข้อมูลสถานะแก้ไข');

            $table->bigInteger('impact_id')->nullable()->comment('รหัสข้อมูล ผลกระทบ');
            $table->bigInteger('priority_id')->nullable()->comment('รหัสข้อมูล ความสำคัญ');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_request_issues');
    }
}
