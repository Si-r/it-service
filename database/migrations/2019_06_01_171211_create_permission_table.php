<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission', function (Blueprint $table) {

            $table->bigIncrements('id')->comment('รหัสข้อมูลตาราง');

            $table->string('permission_name')->comment('ชื่อสิทธิ์เข้าใช้')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
        DB::table('permission')->insert(
            array(
                'permission_name' => 'admin',


            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_permission');
    }
}
