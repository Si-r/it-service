<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Userpermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('userpermission', function (Blueprint $table) {

          $table->bigIncrements('id')->comment('รหัสข้อมูลตาราง');

          $table->string('username')->comment('ชื่อผู้เข้าใช้')->nullable();
          $table->string('permission_id')->comment('ชื่อสิทธิ์เข้าใช้')->nullable();

          $table->timestamps();
          $table->softDeletes();
      });
      DB::table('userpermission')->insert(
          array(
              'username' => 'admin',
              'permission_id' => '1'
          )
      );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
