<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFactRequestGeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_request_general', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('รหัสข้อมูลตาราง');
			$table->bigInteger('client_id')->comment('รหัสข้อมูลผู้สร้าง');

			$table->bigInteger('request_general_id')->comment('รหัสข้อมูลคำร้อง');
			$table->bigInteger('equipment_id')->comment('อุปกรณ์');

			$table->dateTime('return_date')->nullable()->comment('วันที่เวลาคืน');
			$table->dateTime('access_date')->nullable()->comment('วันที่เวลายืม');
			$table->string('access_time')->comment('เวลาที่ยืม');
			$table->enum('access_sort', ['Hour', 'Day', 'Year'])->default('Hour')->comment('ตัวเลือกเวลาที่ยืม');

            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_request_general');
    }
}
