/* eslint-disable camelcase */
/* eslint-disable no-console */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
import React from "react";
import axios from "axios";
import Swal from "sweetalert2";

const HocValidateUser = WrappedComponent => {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        cilent_id: "",
        permission_name: "",
        user_right: ""
      };
    }

    getPermission = async () => {
      const { history } = this.props;
      try {
        const response = await axios.get("/api/getAuthenticatedUser", {
          headers: { Authorization: `Bearer ${sessionStorage.Token}` }
        });
        if (response.status === 200) {
          this.setState({
            cilent_id: response.data.user.id,
            user_right: response.data.user.user_right
          });
        }
      } catch (err) {
        Swal.fire("คุณไม่มีสิทธิ์เข้าถึง url นี้", "กรุณาทำการล็อคอินใหม่", "error");
        history.push("/");
        console.log(err);
      }
    };

    componentDidMount() {
      this.getPermission();
    }

    render() {
      const { cilent_id, permission_name, user_right } = this.state;
      return (
        <div className="min-width-460-md">
          <WrappedComponent
            {...this.props}
            permission_name={permission_name}
            client_id={cilent_id}
            user_right={user_right}
          />
        </div>
      );
    }
  };
};
export default HocValidateUser;
