/* eslint-disable no-alert */
/* eslint-disable no-shadow */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/button-has-type */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-console */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable import/extensions */
import axios from "axios";
import React, { Component } from "react";

import Swal from "sweetalert2/dist/sweetalert2.js";

import { FaUserAlt } from "react-icons/fa";
import IMG from "../../../images/img-01.png";

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreateNewProject = this.handleCreateNewProject.bind(this);
  }

  componentDidMount() {}

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreateNewProject(event) {
    event.preventDefault();

    const project = {
      username: this.state.username,
      password: this.state.password
    };

    axios
      .post("/api/login", project)
      .then(response => {
        // redirect to the homepage
        if (response.data.data.token != null) {
          sessionStorage.setItem("Token", response.data.data.token);
          axios
            .get("api/getAuthenticatedUser", {
              headers: { Authorization: `Bearer ${sessionStorage.Token}` }
            })
            .then(response => {
              if (response.status === 200) {
                Swal.fire({
                  title: "Loading...",
                  onBeforeOpen: () => {
                    Swal.showLoading();
                  }
                });

                sessionStorage.setItem("email", response.data.user.email);
                sessionStorage.setItem("username", response.data.user.username);
                sessionStorage.setItem("last_login", response.data.user.updated_at);
                sessionStorage.setItem("is_block", response.data.user.is_block);
                sessionStorage.setItem("client_id", response.data.user.id);
                sessionStorage.setItem("user_right", response.data.user.user_right);


                axios
                  .post("api/permission/GetPermissionByUsername", {
                      username: response.data.user.username
                  })
                  .then(res => {
                    axios
                      .post("api/permission/getPermission", {
                          permission_id: res.data.permission_id
                      })
                      .then(res => {
                        sessionStorage.setItem(
                          "permission_name",
                          res.data.getpermission.permission_name
                        );
                      })
                      .catch(err =>
                        {console.log(err)}
                      )
                    
                    axios
                      .get(`api/rolepermission/getRolePermission/${res.data.permission_id}`)
                      .then(res => {
                        const listRole = res.data;
                        const arrRole = [];

                        listRole.map((role) => {
                          arrRole.push(role.role);

                          return "";
                        });

                        for (let i = 0; i < arrRole.length; i += 1) {
                          sessionStorage.setItem(arrRole[i], "1");
                        }
                        window.location.href = "/Dashboard";
                      })
                      .catch(err => {
                        console.log(err);
                      });
                  })
                  .catch(err => {
                    console.log(err);
                  });
              }
            })
            .catch(err => {
              console.log(err);
            });
        } else {
          alert(response.data.error);
        }
      })
      .catch(_error => {
        Swal.fire({
          type: "error",
          title: "Login error",
          text: "Incorrect username or password."
        });
      });
  }

  render() {
    return (
      <div className="limiter">
        <div className="container-login100">
          <div className="wrap-login100">
            <div className="login100-pic js-tilt" data-tilt>
              <img src={IMG} />
            </div>

            <form className="login100-form validate-form" onSubmit={this.handleCreateNewProject}>
              <span className="login100-form-title">Member Login</span>

              <div
                className="wrap-input100 validate-input"
                data-validate="Valid email is required: ex@abc.xyz"
              >
                <input
                  className="input100"
                  type="text"
                  name="username"
                  value={this.state.username}
                  onChange={this.handleFieldChange}
                  placeholder="Username"
                />
                <span className="focus-input100" />
                <span className="symbol-input100">
                  <i aria-hidden="true">
                    <FaUserAlt />
                  </i>
                </span>
              </div>

              <div className="wrap-input100 validate-input" data-validate="Password is required">
                <input
                  className="input100"
                  type="password"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleFieldChange}
                  placeholder="Password"
                />
                <span className="focus-input100" />
                <span className="symbol-input100">
                  <i className="fa fa-lock" aria-hidden="true">
                    {" "}
                  </i>
                </span>
              </div>

              <div className="container-login100-form-btn">
                <button className="login100-form-btn">Login</button>
              </div>

              <div className="text-center p-t-12">
                <span className="txt1">Forgot</span>
                <a className="txt2" href="#">
                  Username / Password?
                </a>
              </div>

              <div className="text-center p-t-136">
                <a className="txt2" href="#">
                  Create your Account
                  <i className="fa fa-long-arrow-right m-l-5" aria-hidden="true" />
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
