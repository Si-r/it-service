/* eslint-disable no-unused-vars */
import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManagePriority() {


  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'name',
      field: 'name',
      sort: 'asc',
    },
    {
      label: 'value',
      field: 'value',
      sort: 'asc',
      col: '10%'
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
      col: '25%'
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/priority"
        columns={columns}
        name="priority"
        headname=" List Priority - ข้อมูลความสำคัญ "
        headTablename="ตารางแสดงข้อมูลความสำคัญ"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManagePriority/AddPriority"
        addbutton="Add Priority"
        manage="ManagePriority"
        updateurl="/ManagePriority/UpdatePriority"
        viewurl="/ManagePriority/ViewPriority"
      />
    </div>
  );
}

export default HocValidateUser(ManagePriority);
