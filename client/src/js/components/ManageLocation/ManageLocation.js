/* eslint-disable no-unused-vars */
import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManageLocation() {
  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Name',
      field: 'name',
      sort: 'asc',
    },
    {
      label: 'Address',
      field: 'full_location',
      sort: 'asc',
    },
    {
      label: 'Address',
      field: 'full_address',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/location"
        columns={columns}
        name="location"
        headname=" List Location - ข้อมูลตำแหน่ง "
        headTablename="ตารางแสดงข้อมูลตำแหน่ง"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageLocation/AddLocation"
        addbutton="Add Location"
        manage="ManageLocation"
        updateurl="/ManageLocation/UpdateLocation"
        viewurl="/ManageLocation/ViewLocation"
      />
    </div>
  );
}

export default HocValidateUser(ManageLocation);
