/* eslint-disable import/order */
/* eslint-disable jsx-a11y/label-has-for */
import React, * as react from "react";
import ImageUploader from "react-images-upload";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import HocValidateUser from "../../../HocValidateUser";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import { FormLabel, Icon } from "@material-ui/core";
import Modal from "react-bootstrap/Modal";
import AddAddress from "../ManageAddress/AddAddress";
import FormData from 'form-data'
import clsx from "clsx";

class AddLocation extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",
      address_1: "",
      address_2: "",
      address_3: "",
      address_latitude: 13.75398,
      address_longitude: 100.50144,
      address_id: "",
      image_id: "1",
      image_show: "default",
      image: [],
      getpermission: [],
      getaddress: [],
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      errors: [],
      isModalOpen: false
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.onChangeUploadFile = this.onChangeUploadFile.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.setModalShow = this.setModalShow.bind(this);
  }

  onMarkerDragEnd = (one, two, three) => {
    const { latLng } = three;
    const lat = latLng.lat();
    const lng = latLng.lng();

    this.setState({
      address_latitude: lat,
      address_longitude: lng
    });
  };

  onMarkerClick = (props, marker) => {
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });
  };

  onMapClicked = props => {
    console.log(props);
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };

  onDrop(pictureDataURLs) {
    this.setState({
      // eslint-disable-next-line react/no-access-state-in-setstate
      image: this.state.image.concat(pictureDataURLs)
    });
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  onChangeUploadFile(event) {
    this.setState({
      image: event.target.files[0]
    });
    console.log(this.state.selectedFile);
  }

  handleCreate(event) {
    event.preventDefault();

    // eslint-disable-next-line prefer-const
    let data = new FormData();
    data.append('image', this.state.image[0]);
    data.append('client_id', this.props.client_id);

    const config = {
      headers: {
        // eslint-disable-next-line no-underscore-dangle
        'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
      }
    }

    axios
      .post("/api/uploadImage", data, config)
      .then(res => {
        const insertdata = {
          client_id: sessionStorage.client_id,
          name: this.state.name,
          address_1: this.state.address_1,
          address_2: this.state.address_2,
          address_3: this.state.address_3,
          address_latitude: this.state.address_latitude,
          address_longitude: this.state.address_longitude,
          address_id: this.state.address_id,
          image_id: res.data.image_id,
          image_show: this.state.image_show
        };
        console.log(insertdata);
        axios
          .post("/api/location/store", insertdata)
          .then(() => {
            Swal.fire("Successfully", "Add data successfully ", "success");
          })
          .catch(error => {
            this.setState({
              errors: error.response.data.errors
            });
            console.log(error.response.data.errors);

            Swal.fire("Errors", "check the value of a form field", "error");
          });
      })
      .catch(err => {
        console.log(err);
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return("");
  }

  componentDidMount() {
    axios
      .get("/api/address/index")
      .then(res => {
        this.setState({
          getaddress: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getaddress: []
        });
      });
  }

  setModalShow(isOpen) {
    if(isOpen === false) {
      this.componentDidMount();
    }

    this.setState({
      isModalOpen: isOpen
    });
  }

  modalAddAddress() {
    return (
      <Modal
        show={this.state.isModalOpen}
        onHide={() => this.setModalShow(false)}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">Custom Modal Styling</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddAddress />
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    const { getaddress } = this.state;
    const style = {
      width: "95%",
      height: "95%",
      marginLeft: "20px"
    };
    return (
      <div style={{ paddingTop: "30px" }}>
        {this.modalAddAddress()}
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feEmailAddress">Name</FormLabel>
                        <FormInput
                          id="name"
                          name="name"
                          className={`form-control ${this.hasErrorFor("name") ? "is-invalid" : ""}`}
                          placeholder="ชื่อที่ตั้ง"
                          type="text"
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("name")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="address_1">Address 1</FormLabel>
                        <FormInput
                          id="address_1"
                          name="address_1"
                          className={`form-control ${
                            this.hasErrorFor("address_1") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกที่"
                          type="text"
                          value={this.state.address_1}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("address_1")}
                      </Col>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="address_2">Address 2</FormLabel>
                        <FormInput
                          id="address_2"
                          name="address_2"
                          className={`form-control ${
                            this.hasErrorFor("address_2") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกที่ เพิ่มเติม"
                          type="text"
                          value={this.state.address_2}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("address_2")}
                      </Col>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="address_3">Address 3</FormLabel>
                        <FormInput
                          id="address_3"
                          name="address_3"
                          className={`form-control ${
                            this.hasErrorFor("address_3") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกที่ เพิ่มเติม"
                          type="text"
                          value={this.state.address_3}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("address_3")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feInputState">Image Show</FormLabel>
                        <FormSelect
                          id="feInputState"
                          name="image_show"
                          value={this.state.image_show}
                          onChange={this.handleFieldChange}
                        >
                          <option value="default">default</option>
                          <option value="image1">image1</option>
                          <option value="image2">image2</option>
                        </FormSelect>
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel>Address</FormLabel>
                        &nbsp;
                        <Button
                          theme="link"
                          className="float-right p-b-0 iconAddOption"
                          size="sm"
                          type="button"
                          onClick={() => this.setModalShow(true)}
                        >
                          <Icon
                            className={clsx("iconHover", "fa fa-plus-circle")}
                            color="error"
                            style={{ fontSize: 24 }}
                          />
                        </Button>
                        <FormSelect
                          id="Address"
                          name="address_id"
                          className={`form-control ${
                            this.hasErrorFor("address_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.address_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getaddress.map((val, idx) => (
                            <option key={idx.toString()} value={val.id}>
                              {val.province.name}
                              &nbsp;
                              {val.district.name}
                              &nbsp;
                              {val.sub_district.name}
                              &nbsp;
                              {val.postal_code.code}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("location_id")}
                      </Col>

                      <Col md="12" className="form-group">
                        <div className="form-group">
                          <FormLabel htmlFor="image">เลือกรูปภาพโปรไฟล์</FormLabel>
                          <input
                            className={`form-control ${
                              this.hasErrorFor("image") ? "is-invalid" : ""
                            }`}
                            hidden
                          />
                          <ImageUploader
                            type="file"
                            withIcon
                            value={this.state.image}
                            buttonText="เลือกรูปภาพ"
                            onChange={this.onDrop}
                            imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                            maxFileSize={2020215}
                            singleImage
                            label="ไฟล์ขนาดต้องไม่เกิน 2 MB, สกุลไฟล์: jpg,png,gif"
                            withPreview
                            fileSizeError="ขนาดไฟล์ใหญ่เกินไป"
                            fileTypeError="ประเภทไฟล์ไม่ถูกต้อง"
                          />
                          {this.renderErrorFor("image")}
                        </div>
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="12" className="form-group">
                        <div style={{ width: "100%", height: 400 }}>
                          <Map
                            google={this.props.google}
                            style={style}
                            initialCenter={{
                              lat: this.state.address_latitude,
                              lng: this.state.address_longitude
                            }}
                            zoom={10}
                            onClick={this.onMapClicked}
                          >
                            <Marker
                              onClick={this.onMarkerClick}
                              position={{
                                lat: this.state.address_latitude,
                                lng: this.state.address_longitude
                              }}
                              name="ตำแหน่งปัจจุบัน"
                              draggable
                              onDragend={this.onMarkerDragEnd}
                            />
                            <InfoWindow
                              marker={this.state.activeMarker}
                              visible={this.state.showingInfoWindow}
                            >
                              <div>
                                <h1>{this.state.selectedPlace.name}</h1>
                              </div>
                            </InfoWindow>
                          </Map>
                        </div>
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Create New Location</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyDnmGzOiSJeXAFo2uFsEDOa92cYcT7waO0"
})(HocValidateUser(AddLocation));
