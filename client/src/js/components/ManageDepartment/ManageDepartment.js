/* eslint-disable no-unused-vars */
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

const drawerWidth = 240;

function ManageDepartment() {
  // const columns = ["id", "name", "Action"];
  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Department Name',
      field: 'name',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
      col: '20%'
    }
  ];
  return (
    <div>
      <DataTable
        url="/api/department"
        columns={columns}
        name="department"
        headname=" List Department - ข้อมูลแผนก "
        headTablename="ตารางแสดงข้อมูลแผนก"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageDepartment/AddDepartment"
        addbutton="Add Department"
        manage="ManageDepartment"
        updateurl="/ManageDepartment/UpdateDepartment"
        viewurl="/ManageDepartment/ViewDepartment"
      />
    </div>
  );
}

export default HocValidateUser(ManageDepartment);
