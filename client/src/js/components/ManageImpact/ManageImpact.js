/* eslint-disable no-unused-vars */
import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";



function ManageImpact() {

  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'name',
      field: 'name',
      sort: 'asc',
    },
    {
      label: 'value',
      field: 'value',
      sort: 'asc',
      col: '10%'
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
      col: '25%'
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/impact"
        columns={columns}
        name="impact"
        headname=" List Impact - ข้อมูลผลกระทบ "
        headTablename="ตารางแสดงข้อมูลผลกระทบ"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageImpact/AddImpact"
        addbutton="Add Impact"
        manage="ManageImpact"
        updateurl="/ManageImpact/UpdateImpact"
        viewurl="/ManageImpact/ViewImpact"
      />
    </div>
  );
}

export default HocValidateUser(ManageImpact);
