import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import FroalaEditor from "react-froala-wysiwyg";
import HocValidateUser from "../../../HocValidateUser";
import froalaConfig from "../../../data/froala-config";


class AddMessage extends react.Component {
  constructor(props) {
    super(props);
    this.config = froalaConfig;
    this.state = {
      client_id: sessionStorage.client_id,
      tital: "",
      detail: "",
      status: "",
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.handleModelChange = this.handleModelChange.bind(this);
  }

  handleModelChange(detail) {
    this.setState({
      detail
    });
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      tital: this.state.tital,
      detail: this.state.detail,
      status: this.state.status
    };
    console.log(insertdata);

    axios
      .post("/api/message/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          tital: "",
          detail: "",
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="12" className="form-group">
                        <label htmlFor="feEmailAddress">Tital</label>
                        <FormInput
                          id="tital"
                          name="tital"
                          className={`form-control ${
                            this.hasErrorFor("tital") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อผู้ใช้"
                          type="text"
                          value={this.state.tital}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("tital")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col xs="12" className="form-group">
                        <div className="form-group">
                          <label htmlFor="detail">Detail</label>

                          <FroalaEditor
                            className={`form-control ${
                              this.hasErrorFor("detail") ? "is-invalid" : ""
                            }`}
                            tag="textarea"
                            config={this.config}
                            model={this.state.detail}
                            onModelChange={this.handleModelChange}
                          />
                          {this.renderErrorFor("detail")}
                        </div>
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="4" className="form-group">
                        <label htmlFor="feInputState">Status</label>
                        <FormSelect
                          id="status"
                          name="status"
                          className={`form-control ${
                            this.hasErrorFor("status") ? "is-invalid" : ""
                          }`}
                          value={this.state.status}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>
                          <option value="O">Open</option>
                          <option value="C">Close</option>
                        </FormSelect>
                        {this.renderErrorFor("status")}
                      </Col>
                    </Row>

                    <Button type="submit">Create New Message</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddMessage);
