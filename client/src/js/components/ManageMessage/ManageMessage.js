/* eslint-disable no-unused-vars */
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManageMessage() {

  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'tital',
      field: 'tital',
      sort: 'asc',
    },
    {
      label: 'detail',
      field: 'detail',
      sort: 'asc',
    },
    {
      label: 'status',
      field: 'status',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/message"
        columns={columns}
        name="message"
        headname=" List Message - ข้อมูลข้อความ "
        headTablename="ตารางแสดงข้อมูลข้อความ"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageMessage/AddMessage"
        addbutton="Add Message"
        manage="ManageMessage"
        updateurl="/ManageMessage/UpdateMessage"
        viewurl="/ManageMessage/ViewMessage"
      />
    </div>
  );
}

export default HocValidateUser(ManageMessage);
