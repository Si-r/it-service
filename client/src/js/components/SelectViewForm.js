/* eslint-disable eqeqeq */
import React, * as react from "react";
import PropTypes from "prop-types";
import ViewUser from "./ManageUser/ViewUser";

export default class SelectViewForm extends react.Component {
  render() {
    return (
      <div>
        {this.props.ChooseUpdateForm == "user" ? (
          <ViewUser id={this.props.id} updateurl={this.props.updateurl} />
        ) : null}
      </div>
    );
  }
}

SelectViewForm.propTypes = {
  id: PropTypes.number,
  ChooseUpdateForm: PropTypes.string
};
