import React, * as react from "react";
import { ListGroup, ListGroupItem, Row, Col, Form, Container, Button } from "shards-react";
import axios from "axios";

import { FormLabel } from "@material-ui/core";
import HocValidateUser from "../../../HocValidateUser";

const today = new Date();
const date = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
const time = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;
const dateTime = `${date} ${time}`;

class ViewRequestIssuses extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client: [],
      created_at: "",
      updated_at: "",
      name: "",
      status: "",
      auditor_user_id: sessionStorage.client_id,
      approval_user_id: sessionStorage.client_id,
      audit_timestamp: dateTime,
      approval_timestamp: dateTime,
      getFactRequestIssues: [],
      errors: []
    };
  }

  componentDidMount() {
    axios
      .get(`/api/requestissues/update/${this.props.match.params.id}`)
      .then(res => {
        this.setState({
          updated_at: res.data.updated_at,
          created_at: res.data.created_at,
          client: res.data.client,
          name: res.data.name,
          status: res.data.status,
          getFactRequestIssues: res.data.fact_request_issues
        });
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.log(err);
      });
  }

  equipmentItems() {
    const { getFactRequestIssues } = this.state;

    return getFactRequestIssues.map((factRequestIssue, idx) => (
      <div key={idx.toString()}>
        <hr
          className="hr-text"
          data-content={`ข้อมูลใช้งานอุปกรณ์ ${factRequestIssue.dim_equipment.name}`}
        />
        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="Equipment">Equipment</FormLabel>
            <p>{factRequestIssue.dim_equipment.name}</p>
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="AccessDate">Issues Name ชื่อปัญหา</FormLabel>
            <p>{factRequestIssue.issues_name}</p>
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="AccessDate">Issues Laravel ระดับของปัญหา</FormLabel>
            <p>{factRequestIssue.issues_level}</p>
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="AccessDate">Issues Detail รายละเอียด</FormLabel>
            <p>{factRequestIssue.issues_comment}</p>
          </Col>
        </Row>
        {this.modifyItems(factRequestIssue.modify)}

        {this.priorityItems(factRequestIssue.priority)}

        {this.impactItems(factRequestIssue.impact)}
      </div>
    ));
  }

  modifyItems = (modifyData) => {
    if (modifyData == null) {
      return "";
    }

    return (
      <Row form>
        <Col md="6" className="form-group">
          <FormLabel htmlFor="ReturnDate">Modify ข้อมูลการแก้ไข</FormLabel>
          <p>{modifyData.name}</p>
        </Col>
      </Row>
    );
  }

  priorityItems = (priorityData) => {
    if (priorityData == null) {
      return "";
    }

    return (
      <Row form>
        <Col md="6" className="form-group">
          <FormLabel htmlFor="ReturnDate">Priority ส่งผลกระทบ</FormLabel>
          <p>{priorityData.name}</p>
        </Col>
      </Row>
    );
  }

  impactItems = (impactData) => {
    if (impactData == null) {
      return "";
    }

    return (
      <Row form>
        <Col md="6" className="form-group">
          <FormLabel htmlFor="ReturnDate">Impact ระดับผลกระทบ</FormLabel>
          <p>{impactData.name}</p>
        </Col>
      </Row>
    );
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="name">Title</FormLabel>
                        <p>{this.state.name}</p>
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="status">Status</FormLabel>
                        <p>{this.state.status}</p>
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="name">Created by</FormLabel>
                        <p>{this.state.client.username}</p>
                      </Col>

                      <Col md="3" className="form-group">
                        <FormLabel htmlFor="status">Created at</FormLabel>
                        <p>{this.state.created_at}</p>
                      </Col>
                      <Col md="3" className="form-group">
                        <FormLabel htmlFor="status">Update at</FormLabel>
                        <p>{this.state.created_at}</p>
                      </Col>
                    </Row>
                  </Form>
                </Col>
              </Row>

              <Row>
                <Col>{this.equipmentItems()}</Col>
              </Row>
              <Button theme="secondary" type="button" onClick={() => this.props.history.goBack()}>
                Back
              </Button>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(ViewRequestIssuses);
