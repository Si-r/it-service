
import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import clsx from "clsx";
import Modal from "react-bootstrap/Modal";
import { FormLabel, Icon } from "@material-ui/core";
import AddModify from "../ManageModify/AddModify";
import HocValidateUser from "../../../HocValidateUser";

const today = new Date();
const date = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
const time = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;
const dateTime = `${date} ${time}`;

class UpdateRequestIssuses extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      issues_name: "",
      status: "",
      auditor_user_id: this.props.client_id,
      approval_user_id: this.props.client_id,
      audit_timestamp: dateTime,
      approval_timestamp: dateTime,
      Impact_id:  "",
      priority_id:  "",
      impact_id:  "",
      getFactRequestIssues: [],
      getImpactItems: [],
      getPriorityItems: [],
      getModifyItems: [],
      modifyStatus: "",
      showIsApprove: false,
      isModalModify: false,

      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSelectChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    },() => {
      if(this.state.status === 'Approve') {
        this.setState({
          showIsApprove: true
        });
      }else{
        this.setState({
          showIsApprove: false
        });
      }
    });
  }

  handleUpdate(event) {
    event.preventDefault();
    let insertdata;
    if(this.state.status === "Approval") {
      insertdata = {
        client_id: sessionStorage.client_id,
        issues_name: this.state.issues_name,
        status: this.state.status,
        auditor_user_id: this.props.client_id,
        approval_user_id: this.props.client_id,
        audit_timestamp: dateTime,
        approval_timestamp: dateTime,
        modify_id: this.state.modify_id,
        priority_id: this.state.priority_id,
        impact_id: this.state.impact_id
      };
    }else {
      insertdata = {
        client_id: sessionStorage.client_id,
        issues_name: this.state.issues_name,
        status: this.state.status,
        auditor_user_id: this.props.client_id,
        approval_user_id: this.props.client_id,
        audit_timestamp: dateTime,
        approval_timestamp: dateTime,
        modify_id: this.state.modify_id,
        priority_id: this.state.priority_id,
        impact_id: this.state.impact_id
      };
    }

    axios
      .put(`/api/requestissues/update/${this.props.match.params.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          showIsApprove: false,
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return "";
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get(`/api/requestissues/update/${this.props.match.params.id}`)
      .then(res => {
        this.setState({
          issues_name: res.data.name,
          status: res.data.status,
          getFactRequestIssues: res.data.fact_request_issues
        }, () => {
          if(sessionStorage.user_right === "staff" && this.state.status === "Pending") {
            const insertdata = {
              auditor_user_id: sessionStorage.client_id,
              audit_timestamp: this.state.audit_timestamp
            };
            axios
            .put(`/api/requestissues/updateAudit/${this.props.match.params.id}`, insertdata)
            .then(() => {
              this.setState({
                status: 'Auditor',
              });
            });
          }
        });
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.log(err);
      });


      this.updateItems();
  }

  updateItems() {
    axios
      .get(`/api/impact/index`)
      .then(res => {
        this.setState({
          getImpactItems: res.data
        });
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.log(err);
      });

      axios
      .get(`/api/modify/index`)
      .then(res => {
        this.setState({
          getModifyItems: res.data
        });
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.log(err);
      });

      axios
      .get(`/api/priority/index`)
      .then(res => {
        this.setState({
          getPriorityItems: res.data
        });
      })
      .catch(err => {
        // eslint-disable-next-line no-console
        console.log(err);
      });
  }

  equipmentItems() {
    const { getFactRequestIssues } = this.state;

    return getFactRequestIssues.map((factRequestIssue, idx) => (
      <div key={idx.toString()}>
        <hr
          className="hr-text"
          data-content={`ข้อมูลใช้งานอุปกรณ์ ${factRequestIssue.dim_equipment.name}`}
        />
        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="Equipment">Equipment</FormLabel>
            <p>{factRequestIssue.dim_equipment.name}</p>
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="AccessDate">Issues Name ชื่อปัญหา</FormLabel>
            <p>{factRequestIssue.issues_name}</p>
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="AccessDate">Issues Laravel ระดับของปัญหา</FormLabel>
            <p>{factRequestIssue.issues_level}</p>
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="AccessDate">Issues Detail รายละเอียด</FormLabel>
            <p>{factRequestIssue.issues_comment}</p>
          </Col>
        </Row>
        {this.modifyItems(factRequestIssue.modify)}

        {this.priorityItems(factRequestIssue.priority)}

        {this.impactItems(factRequestIssue.impact)}
      </div>
    ));
  }

  modifyItems = (modifyData) => {
    if (modifyData == null) {
      return "";
    }

    return (
      <Row form>
        <Col md="6" className="form-group">
          <FormLabel htmlFor="ReturnDate">Modify ข้อมูลการแก้ไข</FormLabel>
          <p>{modifyData.name}</p>
        </Col>
      </Row>
    );
  }

  priorityItems  = (priorityData) => {
    if (priorityData == null) {
      return "";
    }

    return (
      <Row form>
        <Col md="6" className="form-group">
          <FormLabel htmlFor="ReturnDate">Priority ส่งผลกระทบ</FormLabel>
          <p>{priorityData.name}</p>
        </Col>
      </Row>
    );
  }

  impactItems = (impactData) => {
    if (impactData == null) {
      return "";
    }

    return (
      <Row form>
        <Col md="6" className="form-group">
          <FormLabel htmlFor="ReturnDate">Impact ระดับผลกระทบ</FormLabel>
          <p>{impactData.name}</p>
        </Col>
      </Row>
    );
  }

  approveFormDetail = () => {
    const { getImpactItems, getModifyItems, getPriorityItems } = this.state;

    return (
      <div>
        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="modify_id">Assign Modify Issue</FormLabel>
            <FormSelect
              id="modify_id"
              name="modify_id"
              className={`form-control ${
                this.hasErrorFor("modify_id") ? "is-invalid" : ""
              }`}
              value={this.state.modify_id}
              onChange={this.handleSelectChange}
            >
              <option value="">Choose...</option>
              {getModifyItems.map((modifyItems, idx) => (
                <option key={idx.toString()} value={modifyItems.id}>
                  {modifyItems.name}
                </option>
              ))}
            </FormSelect>
            {this.renderErrorFor("modify_id")}
          </Col>
          <Col className="">
            <Button
              theme="link"
              className="float-left p-b-0 iconAddOption"
              size="sm"
              type="button"
              onClick={() => this.setModalShow(true)}
            >
              <Icon
                className={clsx("iconHover", "fa fa-plus-circle")}
                color="error"
                style={{ fontSize: 24 }}
              />
            </Button>
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="priority_id">Priority Level</FormLabel>
            <FormSelect
              id="priority_id"
              name="priority_id"
              className={`form-control ${
                this.hasErrorFor("priority_id") ? "is-invalid" : ""
              }`}
              value={this.state.priority_id}
              onChange={this.handleSelectChange}
            >
              <option value="">Choose...</option>
              {getPriorityItems.map((priorityItems, idx) => (
                <option key={idx.toString()} value={priorityItems.id}>
                  {priorityItems.name}
                </option>
              ))}
            </FormSelect>
            {this.renderErrorFor("priority_id")}
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="impact_id">Impact Level</FormLabel>
            <FormSelect
              id="impact_id"
              name="impact_id"
              className={`form-control ${
                this.hasErrorFor("impact_id") ? "is-invalid" : ""
              }`}
              value={this.state.impact_id}
              onChange={this.handleSelectChange}
            >
              <option value="">Choose...</option>
              {getImpactItems.map((impactItem, idx) => (
                <option key={idx.toString()} value={impactItem.id}>
                  {impactItem.name}
                </option>
              ))}
            </FormSelect>
            {this.renderErrorFor("impact_id")}
          </Col>
        </Row>
      </div>
    );
  }

  setModalShow(isOpen) {
    if (isOpen === false) {
      this.updateItems();
    }

    this.setState({
      isModalModify: isOpen
    });
  }

  modalModify() {
    return (
      <Modal
        show={this.state.isModalModify}
        onHide={() => this.setModalShow(false, 'Location')}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Modify (เพิ่มข้อมูลการซ่อมซอม)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddModify />
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        {this.modalModify()}
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleUpdate}>
                    <Row form>
                      <Col md="12" className="form-group">
                        <FormLabel htmlFor="feName">Name</FormLabel>
                        <FormInput
                          id="feName"
                          name="issues_name"
                          className={
                            `form-control ${this.hasErrorFor("issues_name") ? "is-invalid" : ""}`
                          }
                          placeholder="กรอกชื่อผู้ใช้"
                          type="text"
                          value={this.state.issues_name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("issues_name")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="status">Status</FormLabel>
                        <FormSelect
                          id="status"
                          name="status"
                          className={`form-control ${
                            this.hasErrorFor("status") ? "is-invalid" : ""
                          }`}
                          value={this.state.status}
                          onChange={this.handleSelectChange}
                        >
                          <option value="">Choose...</option>
                          <option value="Pending">Pending</option>
                          <option value="Approve">Approve</option>
                          <option value="Auditor">Auditor</option>
                          <option value="Rejected">Rejected</option>
                        </FormSelect>
                        {this.renderErrorFor("status")}
                      </Col>
                    </Row>

                    <Row>
                      <Col>{this.equipmentItems()}</Col>
                    </Row>

                    {
                      this.state.showIsApprove &&
                      (
                        this.approveFormDetail()
                      )
                    }

                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Update Request Issues</Button>
                  </Form>
                </Col>
              </Row>

            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(UpdateRequestIssuses);
