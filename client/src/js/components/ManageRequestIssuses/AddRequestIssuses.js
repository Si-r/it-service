import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import { FormLabel } from "@material-ui/core";
import HocValidateUser from "../../../HocValidateUser";

const today = new Date();
const date = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
const time = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;
const dateTime = `${date} ${time}`;

class AddRequestIssues extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",
      status: "Pending",
      issues_level: "Problems",
      issues_comment: "",
      issues_name: "",
      equipment_id: "",
      auditor_user_id: this.props.client_id,
      approval_user_id: this.props.client_id,
      audit_timestamp: dateTime,
      approval_timestamp: dateTime,

      getEquipment: [],
      getImpact: [],
      getPriority: [],
      getModify: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      name: this.state.name,
      status: this.state.status,
      issues_level: this.state.issues_level,
      issues_comment: this.state.issues_comment,
      issues_name: this.state.issues_name,
      equipment_id: this.state.equipment_id,
      // auditor_user_id: this.props.client_id,
      // approval_user_id: this.props.client_id,
      // audit_timestamp: dateTime,
      // approval_timestamp: dateTime
    };

    axios
      .post("/api/requestissues/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return ("");
  }

  componentDidMount() {
    axios
    .get("/api/equipment/index")
    .then(res => {
      this.setState({
        getEquipment: res.data
      });
    })
    .catch(err => {
      console.log(err);
      this.setState({
        getEquipment: []
      });
    });
  }

  render() {
    const { getEquipment } = this.state;

    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="name">Title</FormLabel>
                        <FormInput
                          id="name"
                          name="name"
                          className={`form-control ${this.hasErrorFor("name") ? "is-invalid" : ""}`}
                          placeholder="หัวข้อที่แจ้ง"
                          type="text"
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("name")}
                      </Col>
                    </Row>

                    <hr className="hr-text" data-content="ข้อมูลรายงานอุปกรณ์" />

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="Equipment">Equipment</FormLabel>
                        <FormSelect
                          id="Equipment"
                          name="equipment_id"
                          className={`form-control ${
                            this.hasErrorFor("equipment_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.person_responsible_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getEquipment.map((equipment, idx) => (
                            <option key={idx.toString()} value={equipment.id}>
                              {equipment.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("equipment_id")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="issues_name">Issues ปัญหาที่พบ</FormLabel>
                        <FormInput
                          id="issues_name"
                          name="issues_name"
                          className={`form-control
                            ${this.hasErrorFor("issues_name") ? "is-invalid" : ""}`}
                          placeholder="ปัญหาที่พบ"
                          type="text"
                          value={this.state.issues_name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("issues_name")}
                      </Col>

                      <Col md="2" className="form-group">
                        <FormLabel htmlFor="issues_level">Level</FormLabel>
                        <FormSelect
                          id="issues_level"
                          name="issues_level"
                          value={this.state.issues_level}
                          onChange={this.handleFieldChange}
                          className={`form-control ${
                            this.hasErrorFor("issues_level") ? "is-invalid" : ""
                          }`}
                        >
                          <option value="Problems">Problems</option>
                          <option value="Incidents">Incidents</option>
                          <option value="None">None</option>
                        </FormSelect>
                      </Col>

                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="issues_comment">Comment อธิบายเพิ่มเติม</FormLabel>
                        <textarea
                          id="issues_comment"
                          name="issues_comment"
                          className={`form-control ${
                            this.hasErrorFor("issues_comment") ? "is-invalid" : ""
                          }`}
                          placeholder="อธิบายเพิ่มเติม"
                          type="text"
                          value={this.state.issues_comment}
                          onChange={this.handleFieldChange}
                        />
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Create New Request Issues</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddRequestIssues);
