/* eslint-disable no-unused-vars */
import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManageRequestIssuses() {
  // const columns = [
  //   "id",
  //   "name",
  //   "status",
  //   "auditor_user_id",
  //   "approval_user_id",
  //   "audit_timestamp",
  //   "approval_timestamp",
  //   "Action"
  // ];

  const columns = [
    {
      label: "ID",
      field: "id",
      sort: "asc"
    },
    {
      label: "Name",
      field: "name",
      sort: "asc"
    },
    {
      label: "status",
      field: "status",
      sort: "asc"
    },
    {
      label: "Approval By",
      field: "approval_user",
      subField: "username",
      sort: "name"
    },
    {
      label: "Audit Timestamp",
      field: "audit_timestamp",
      sort: "asc"
    },
    {
      label: "Approval Timestamp",
      field: "approval_timestamp",
      sort: "asc"
    },
    {
      label: "Action",
      field: "Action",
      sort: "asc",
      col: "20%"
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/requestissues"
        columns={columns}
        name="requestissues"
        headname=" List Request Issues - ข้อมูลคำร้องปัญหา"
        headTablename="ตารางแสดงข้อมูลคำร้องปัญหา"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageRequestIssuses/AddRequestIssues"
        addbutton="Add RequestIssues"
        manage="ManageRequestIssuses"
        updateurl="/ManageRequestIssuses/UpdateRequestIssues"
        viewurl="/ManageRequestIssuses/ViewRequestIssues"
      />
    </div>
  );
}

export default HocValidateUser(ManageRequestIssuses);
