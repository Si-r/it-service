/* eslint-disable no-unused-vars */
import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManagePostalCode() {

  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Postal Code',
      field: 'code',
      sort: 'asc',
    },
    {
      label: 'Province',
      field: 'province',
      subField: 'name',
      sort: 'asc',
    },
    {
      label: 'District',
      field: 'district',
      subField: 'name',
      sort: 'asc',
    },
    {
      label: 'Sub District',
      field: 'sub_district',
      subField: 'name',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      col: '20%',
      sort: 'asc',
    }
  ];


  return (
    <div>
      <DataTable
        url="/api/postalcode"
        columns={columns}
        name="postalcode"
        headname=" List Postal Code - ข้อมูลรหัสไปรษณีย์ "
        headTablename="ตารางแสดงข้อมูลรหัสไปรษณีย์"
        edit="แก้ไข"
        delete="ลบ"
        addbutton="empty"
        manage="ManagePostalCode"
        updateurl="/ManagePostalCode/UpdatePostalCode"
        viewurl="/ManagePostalCode/ViewPostalCode"
      />
    </div>
  );
}

export default HocValidateUser(ManagePostalCode);
