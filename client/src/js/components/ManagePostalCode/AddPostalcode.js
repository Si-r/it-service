import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";

import HocValidateUser from "../../../HocValidateUser";
import { FormLabel } from "@material-ui/core";

class AddPostalCode extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
      sub_district_id: "",
      district_id: "",
      province_id: "",

      getsub_district: [],
      getdistrict: [],
      getprovince: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      code: this.state.code,
      sub_district_id: this.state.sub_district_id,
      district_id: this.state.district_id,
      province_id: this.state.province_id
    };
    console.log(insertdata);

    axios
      .post("/api/postalcode/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          code: "",
          sub_district_id: "",
          district_id: "",
          province_id: "",
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return ("");
  }

  componentDidMount() {
    axios
      .get("/api/subdistrict/index")
      .then(res => {
        this.setState({
          getsubdistrict: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getsubdistrict: []
        });
      });

    axios
      .get("/api/district/index")
      .then(res => {
        this.setState({
          getdistrict: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getdistrict: []
        });
      });

    axios
      .get("/api/province/index")
      .then(res => {
        this.setState({
          getprovince: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getprovince: []
        });
      });
  }

  render() {
    const { getdistrict, getprovince } = this.state;
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="12" className="form-group">
                        <FormLabel htmlFor="code">PostalCode</FormLabel>
                        <FormInput
                          id="code"
                          name="code"
                          className={`form-control ${this.hasErrorFor("code") ? "is-invalid" : ""}`}
                          placeholder="กรอกชื่อผู้ใช้"
                          type="text"
                          value={this.state.code}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("code")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="district_id">District</FormLabel>
                        <FormSelect
                          id="district_id"
                          name="district_id"
                          className={`form-control ${
                            this.hasErrorFor("district_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.district_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getdistrict.map((district, idx) => (
                            <option key={idx.toString()} value={district.id}>
                              {district.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("district_id")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="sub_district_id">Sub District</FormLabel>
                        <FormSelect
                          id="sub_district_id"
                          name="sub_district_id"
                          className={`form-control ${
                            this.hasErrorFor("sub_district_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.sub_district_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>


                        </FormSelect>
                        {this.renderErrorFor("sub_district_id")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="province_id">Province</FormLabel>
                        <FormSelect
                          id="province_id"
                          name="province_id"
                          className={`form-control ${
                            this.hasErrorFor("province_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.province_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getprovince.map((province, idx) => (
                            <option key={idx.toString()} value={province.id}>
                              {province.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("province_id")}
                      </Col>
                    </Row>

                    <Button type="submit">Create New PostalCode</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddPostalCode);
