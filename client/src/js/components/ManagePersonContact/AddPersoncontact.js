import React, * as react from "react";

import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormSelect,
  Button,
  Container,
  FormInput
} from "shards-react";
import { FormLabel, Icon } from "@material-ui/core";
import Swal from "sweetalert2";
import axios from "axios";
import Modal from "react-bootstrap/Modal";
import clsx from "clsx";
import HocValidateUser from "../../../HocValidateUser";
import AddLocation from "../ManageLocation/AddLocation";
import AddPersonResponsible from "../ManagePersonResponsible/AddPersonResponsible";
import AddCompany from "../ManageCompany/AddCompany"
import PageTitle from "../../../components/common/PageTitle";

class AddPersoncontact extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      company_id: "",
      person_responsible_id: "",
      first_name: "",
      last_name: "",
      nick_name: "",
      telephone: "",
      email: "",
      id_card: "",
      id_employee: "",
      location_id: "",
      isModalOpen: false,
      isModelPersonResponsible: false,
      isModelPersonAddCompany: false,

      getcompany: [],
      getpersonresponsible: [],
      getlocation: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.setModalShow = this.setModalShow.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      company_id: this.state.company_id,
      person_responsible_id: this.state.person_responsible_id,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      nick_name: this.state.nick_name,
      telephone: this.state.telephone,
      email: this.state.email,
      id_card: this.state.id_card,
      id_employee: this.state.id_employee,
      location_id: this.state.location_id,
    };

    axios
      .post("/api/personcontact/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          company_id: "",
          person_responsible_id: "",
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get("/api/company/index")
      .then(res => {
        this.setState({
          getcompany: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getcompany: []
        });
      });

    axios
      .get("/api/personresponsible/index")
      .then(res => {
        this.setState({
          getpersonresponsible: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getpersonresponsible: []
        });
      });

    axios
      .get("/api/location/index")
      .then(res => {
        this.setState({
          getlocation: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getlocation: []
        });
      });
  }

  setModalShow(isOpen, isItem) {
    if (isOpen === false) {
      this.componentDidMount();
    }

    if(isItem === "Location") {
      this.setState({
        isModalOpen: isOpen
      });
    }else if(isItem === "PersonResponsible") {
      this.setState({
        isModelPersonResponsible: isOpen
      });
    }else{
      this.setState({
        isModelCompany: isOpen
      });
    }
  }

  modalAddLocation() {
    return (
      <Modal
        show={this.state.isModalOpen}
        onHide={() => this.setModalShow(false, 'Location')}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Location (เพิ่มข้อมูลตำเหน่ง)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddLocation />
        </Modal.Body>
      </Modal>
    );
  }

  modalAddPersonResponsible() {
    return (
      <Modal
        show={this.state.isModelPersonResponsible}
        onHide={() => this.setModalShow(false, 'PersonResponsible')}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Person Responsible (เพิ่มข้อมูลผู้รับผิดชอบ)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddPersonResponsible />
        </Modal.Body>
      </Modal>
    );
  }

  modalAddCompany() {
    return (
      <Modal
        show={this.state.isModelCompany}
        onHide={() => this.setModalShow(false, 'Company')}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Company (เพิ่มข้อมูลบริษัท)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddCompany />
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    const { getpersonresponsible, getcompany, getlocation } = this.state;
    return (
      <div>
        {this.modalAddLocation()}
        {this.modalAddPersonResponsible()}
        {this.modalAddCompany()}
        <Container>
          <Row className="page-header py-4">
            <PageTitle
              title='Add Person Contact (เพิ่มข้อมูลผู้ติดต่อ)'
              className="text-sm-left"
            />
          </Row>
          <ListGroup flush className="br-10">
            <ListGroupItem className="p-3 br-10">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="person_responsible_id">Person Responsible (ผู้รับผิดชอบ)</FormLabel>
                        <Button
                          theme="link"
                          className="float-right p-b-0 iconAddOption"
                          size="sm"
                          type="button"
                          onClick={() => this.setModalShow(true, 'PersonResponsible')}
                        >
                          <Icon
                            className={clsx("iconHover", "fa fa-plus-circle")}
                            color="error"
                            style={{ fontSize: 24 }}
                          />
                        </Button>
                        <FormSelect
                          id="person_responsible_id"
                          name="person_responsible_id"
                          className={`form-control ${
                            this.hasErrorFor("person_responsible_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.person_responsible_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getpersonresponsible.map((personresponsible, idx) => (
                            <option key={idx.toString()} value={personresponsible.id}>
                              {personresponsible.first_name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("person_responsible_id")}
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="company_id">Company</FormLabel>
                        <Button
                          theme="link"
                          className="float-right p-b-0 iconAddOption"
                          size="sm"
                          type="button"
                          onClick={() => this.setModalShow(true, 'Company')}
                        >
                          <Icon
                            className={clsx("iconHover", "fa fa-plus-circle")}
                            color="error"
                            style={{ fontSize: 24 }}
                          />
                        </Button>
                        <FormSelect
                          id="company_id"
                          name="company_id"
                          className={`form-control ${
                            this.hasErrorFor("company_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.company_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getcompany.map((company, idx) => (
                            <option key={idx.toString()} value={company.id}>
                              {company.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("company_id")}
                      </Col>
                    </Row>

                    <hr className="hr-text" data-content="ข้อมูลบุคลากร" />

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feFirstName">First Name (ชื่อ)</FormLabel>
                        <FormInput
                          id="feFirstName"
                          name="first_name"
                          className={`form-control ${
                            this.hasErrorFor("first_name") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อ"
                          type="text"
                          value={this.state.first_name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("first_name")}
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feLastName">Last Name (นามสกุล)</FormLabel>
                        <FormInput
                          id="feLastName"
                          name="last_name"
                          className={`form-control ${
                            this.hasErrorFor("last_name") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกนามสกุล"
                          type="text"
                          value={this.state.last_name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("last_name")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feNickName">NickName (ชื่อเล่น)</FormLabel>
                        <FormInput
                          id="feNickName"
                          name="nick_name"
                          className={`form-control ${
                            this.hasErrorFor("nick_name") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อเล่น"
                          type="text"
                          value={this.state.nick_name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("nick_name")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feTelephone">Telephone (เบอร์โทร)</FormLabel>
                        <FormInput
                          id="feTelephone"
                          name="telephone"
                          className={`form-control ${
                            this.hasErrorFor("telephone") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกเบอร์โทร"
                          type="text"
                          value={this.state.telephone}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("telephone")}
                      </Col>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feEmail">Email (อีเมลล์)</FormLabel>
                        <FormInput
                          id="feEmail"
                          name="email"
                          className={`form-control ${
                            this.hasErrorFor("email") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกอีเมลล์"
                          type="text"
                          value={this.state.email}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("email")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feIdentificationNo">
                          Identification No (เลขบัตรประชาชน)
                        </FormLabel>
                        <FormInput
                          id="feIdentificationNo"
                          name="id_card"
                          className={`form-control ${
                            this.hasErrorFor("telephone") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกเลขบัตรประชาชน"
                          type="text"
                          value={this.state.id_card}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("telephone")}
                      </Col>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feEmployee">Employee No (เลขประจำตัวพนักงาน)</FormLabel>
                        <FormInput
                          id="feEmployee"
                          name="id_employee"
                          className={`form-control ${
                            this.hasErrorFor("email") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกเลขประจำตัวพนักงาน"
                          type="text"
                          value={this.state.id_employee}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("email")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6">
                        <Row form>
                          <Col md="12">
                            <FormLabel htmlFor="feLocation">Location (ตำเหน่งที่ตั้ง)</FormLabel>
                            <Button
                              theme="link"
                              className="float-right p-b-0 iconAddOption"
                              size="sm"
                              type="button"
                              onClick={() => this.setModalShow(true, 'Location')}
                            >
                              <Icon
                                className={clsx("iconHover", "fa fa-plus-circle")}
                                color="error"
                                style={{ fontSize: 24 }}
                              />
                            </Button>
                          </Col>
                        </Row>
                        <Row>
                          <Col md="12">
                            <FormSelect
                              id="feLocation"
                              name="location_id"
                              className={`form-control ${
                                this.hasErrorFor("location_id") ? "is-invalid" : ""
                              }`}
                              value={this.state.location_id}
                              onChange={this.handleFieldChange}
                            >
                              <option value="">Choose...</option>

                              {getlocation.map((location, idx) => (
                                <option key={idx.toString()} value={location.id}>
                                  {location.name}
                                </option>
                              ))}
                            </FormSelect>
                          </Col>
                        </Row>
                        {this.renderErrorFor("location_id")}
                      </Col>
                    </Row>
                    <Row className="m-t-15">
                      <Col md="12">
                        <Button
                          theme="secondary"
                          type="button"
                          onClick={() => this.props.history.goBack()}
                        >
                          Back
                        </Button>
                        &nbsp;
                        <Button type="submit">Create New Person Contact</Button>
                      </Col>
                    </Row>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddPersoncontact);
