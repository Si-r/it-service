import React, * as react from "react";
import {
  Row,
  Col,
  Button,
  Form,
  FormInput,
  FormSelect
} from "shards-react";
import { FormLabel, Icon } from "@material-ui/core";
import Axios from "axios";
import Swal from "sweetalert2";
import clsx from "clsx";
import PropTypes from "prop-types";

import HocValidateUser from "../../../HocValidateUser";


class AddFactPersonContact extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      person_contact_id: this.props.person_contact_id,
      first_name: "",
      last_name: "",
      nick_name: "",
      telephone: "",
      email: "",
      id_card: "",
      id_employee: "",
      location_id: "",
      getlocation: [],
      isModalOpen: false,
      errors: []
    }

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.setModalShow = this.setModalShow.bind(this);
  }

  setModalShow(isOpen) {
    if (isOpen === false) {
      this.componentDidMount();
    }

    this.setState({
      isModalOpen: isOpen
    });

  }

  componentDidMount() {
    Axios
      .get("/api/location/index")
      .then(res => {
        this.setState({
          getlocation: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getlocation: []
        });
      });
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      person_contact_id: this.state.person_contact_id,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      nick_name: this.state.nick_name,
      telephone: this.state.telephone,
      email: this.state.email,
      id_card: this.state.id_card,
      id_employee: this.state.id_employee,
      location_id: this.state.location_id,
    };

    console.log()

    Axios
      .post("/api/personcontact/factStore", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  render() {
    const { getlocation } = this.state;
    return(
      <Form onSubmit={this.handleCreate}>
        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="first_name">First Name (ชื่อ)</FormLabel>
            <FormInput
              id="first_name"
              name="first_name"
              className={`form-control ${
                this.hasErrorFor("first_name") ? "is-invalid" : ""
              }`}
              placeholder="กรอกชื่อ"
              type="text"
              value={this.state.first_name}
              onChange={this.handleFieldChange}
            />
            {this.renderErrorFor("first_name")}
          </Col>

          <Col md="6" className="form-group">
            <FormLabel htmlFor="feLastName">Last Name (นามสกุล)</FormLabel>
            <FormInput
              id="feLastName"
              name="last_name"
              className={`form-control ${
                this.hasErrorFor("last_name") ? "is-invalid" : ""
              }`}
              placeholder="กรอกนามสกุล"
              type="text"
              value={this.state.last_name}
              onChange={this.handleFieldChange}
            />
            {this.renderErrorFor("last_name")}
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="feNickName">NickName (ชื่อเล่น)</FormLabel>
            <FormInput
              id="feNickName"
              name="nick_name"
              className={`form-control ${
                this.hasErrorFor("nick_name") ? "is-invalid" : ""
              }`}
              placeholder="กรอกชื่อเล่น"
              type="text"
              value={this.state.nick_name}
              onChange={this.handleFieldChange}
            />
            {this.renderErrorFor("nick_name")}
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="feTelephone">Telephone (เบอร์โทร)</FormLabel>
            <FormInput
              id="feTelephone"
              name="telephone"
              className={`form-control ${
                this.hasErrorFor("telephone") ? "is-invalid" : ""
              }`}
              placeholder="กรอกเบอร์โทร"
              type="text"
              value={this.state.telephone}
              onChange={this.handleFieldChange}
            />
            {this.renderErrorFor("telephone")}
          </Col>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="feEmail">Email (อีเมลล์)</FormLabel>
            <FormInput
              id="feEmail"
              name="email"
              className={`form-control ${
                this.hasErrorFor("email") ? "is-invalid" : ""
              }`}
              placeholder="กรอกอีเมลล์"
              type="text"
              value={this.state.email}
              onChange={this.handleFieldChange}
            />
            {this.renderErrorFor("email")}
          </Col>
        </Row>

        <Row form>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="feIdentificationNo">
              Identification No (เลขบัตรประชาชน)
            </FormLabel>
            <FormInput
              id="feIdentificationNo"
              name="id_card"
              className={`form-control ${
                this.hasErrorFor("telephone") ? "is-invalid" : ""
              }`}
              placeholder="กรอกเลขบัตรประชาชน"
              type="text"
              value={this.state.id_card}
              onChange={this.handleFieldChange}
            />
            {this.renderErrorFor("telephone")}
          </Col>
          <Col md="6" className="form-group">
            <FormLabel htmlFor="feEmployee">Employee No (เลขประจำตัวพนักงาน)</FormLabel>
            <FormInput
              id="feEmployee"
              name="id_employee"
              className={`form-control ${
                this.hasErrorFor("email") ? "is-invalid" : ""
              }`}
              placeholder="กรอกเลขประจำตัวพนักงาน"
              type="text"
              value={this.state.id_employee}
              onChange={this.handleFieldChange}
            />
            {this.renderErrorFor("email")}
          </Col>
        </Row>

        <Row form>
          <Col md="6">
            <Row form>
              <Col md="12">
                <FormLabel htmlFor="feLocation">Location (ตำเหน่งที่ตั้ง)</FormLabel>
                <Button
                  theme="link"
                  className="float-right p-b-0 iconAddOption"
                  size="sm"
                  type="button"
                  onClick={() => this.setModalShow(true)}
                >
                  <Icon
                    className={clsx("iconHover", "fa fa-plus-circle")}
                    color="error"
                    style={{ fontSize: 24 }}
                  />
                </Button>
              </Col>
            </Row>
            <Row>
              <Col md="12">
                <FormSelect
                  id="feLocation"
                  name="location_id"
                  className={`form-control ${
                    this.hasErrorFor("location_id") ? "is-invalid" : ""
                  }`}
                  value={this.state.location_id}
                  onChange={this.handleFieldChange}
                >
                  <option value="">Choose...</option>

                  {getlocation.map((location, idx) => (
                    <option key={idx.toString()} value={location.id}>
                      {location.name}
                    </option>
                  ))}
                </FormSelect>
              </Col>
            </Row>
            {this.renderErrorFor("location_id")}
          </Col>
        </Row>
        <Row className="m-t-15">
          <Col md="12">
            <Button
              theme="secondary"
              type="button"
              onClick={() => this.props.history.goBack()}
            >
              Back
            </Button>
            &nbsp;
            <Button type="submit">Create New Person Contact</Button>
          </Col>
        </Row>
      </Form>
    )
  }
}

AddFactPersonContact.propTypes = {
  person_contact_id: PropTypes.number
}
export default HocValidateUser(AddFactPersonContact);
