import React, * as react from "react";

import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Container,
  Button,
} from "shards-react";
import { Collapse } from "reactstrap";
import Swal from "sweetalert2";
import axios from "axios";
import Modal from "react-bootstrap/Modal";
import { FormLabel } from "@material-ui/core";
import HocValidateUser from "../../../HocValidateUser";
import AddFactPersonContact from "./AddFactPersonContact";

class ViewPersonContact extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      getPersonResponsible: [],
      getCompany: [],
      getDimPersonContact: [],
      getFactPersonContact: [],
      itemsCollapse: [],
      isModalOpen:false,
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.toggleNotifications = this.toggleNotifications.bind(this);
    this.setModalShow = this.setModalShow.bind(this);

  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      company_id: this.state.company_id,
      person_responsible_id: this.state.person_responsible_id
    };

    axios
      .put(`/api/personcontact/update/${this.props.match.params.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return ("");
  }

  componentDidMount() {
    axios
      .get(`/api/personcontact/update/${this.props.match.params.id}`)
      .then(res => {
        this.setState({
          getPersonResponsible: res.data,
          getCompany: res.data.dim_company,
          getDimPersonContact: res.data.dim_person_responsible,
          getFactPersonContact: res.data.fact_person_contact,
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getpersonresponsible: []
        });
      });
  }

  setModalShow(isOpen) {
    if(isOpen === false) {
      this.componentDidMount();
    }

    this.setState({
      isModalOpen: isOpen
    });
  }

  toggleNotifications = (idx) => {
    const { itemsCollapse } = this.state;
    itemsCollapse[idx] = !itemsCollapse[idx];
    this.setState({
      itemsCollapse
    });
  }

  deleteFact (id) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger"
      },
      buttonsStyling: false
    });

    axios
    .delete(`/api/personcontact/factdelete/${id}`)
    .then(response => {
      swalWithBootstrapButtons.fire(
        "ลบข้อมูลสำเร็จ!",
        "ข้อมูลได้ถูกลบออกจากตารางแล้ว",
        "success"
      );
      console.log(response.data);
      this.componentDidMount();
    })
    .catch(e => {
      swalWithBootstrapButtons.fire(
        "ลบข้อมูลไม่สำเร็จ!",
        "เกิดข้อผิดพลาดในการส่งข้อมูล",
        "error"
      );
      console.log(e);
    });
  }

  factPersonContact = () => {
    const {getFactPersonContact} = this.state;

    return(
      <Container fluid className="main-content-container px-4">
        <Row className="card-collapse-view-header">
          <Col md="1">ID</Col>
          <Col md="6">Name</Col>
          <Col md="3">ID Employee</Col>
          <Col md="2" className="text-center">&nbsp;&nbsp;&nbsp;&nbsp;Action</Col>
        </Row>
        {
          getFactPersonContact.map((value, idx) => {
            const rows = (
              <Row key={idx.toString()}>
                <Col md="12" className="card-collapse-view-body">
                  <Row>
                    <Col md="1" onClick={() => this.toggleNotifications(idx.toString())}>
                      {value.id}
                    </Col>
                    <Col md="6" onClick={() => this.toggleNotifications(idx.toString())}>
                      {value.first_name}
                      &nbsp;
                      {value.last_name}
                    </Col>
                    <Col md="3" onClick={() => this.toggleNotifications(idx.toString())}>{value.id_employee}</Col>
                    <Col md="2" className="text-center">
                      <Button
                        type="button"
                        onClick={() => this.deleteFact(value.id)}
                        className="btn btn-danger"
                      >
                        Delete
                      </Button>

                    </Col>
                  </Row>
                </Col>

                <Collapse
                  isOpen={this.state.itemsCollapse[idx]}
                  className="col-md-12 card-collapse-view-body-sub"
                >
                  <Row>
                    <Col md="4">
                      <Col md="12">
                        Identity No
                      </Col>
                      <Col md="12">
                        {value.id_card}
                      </Col>
                    </Col>

                    <Col md="4">
                      <Col md="12">
                        Employee No
                      </Col>
                      <Col md="12">
                        {value.id_employee}
                      </Col>
                    </Col>

                    <Col md="4">
                      <Col md="12">
                        Email
                      </Col>
                      <Col md="12">
                        {value.email}
                      </Col>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="8">
                      <Col md="12">
                        Name
                      </Col>
                      <Col md="12">
                        {value.first_name}
                        &nbsp;
                        {value.last_name}
                      </Col>
                    </Col>

                    <Col md="4">
                      <Col md="12">
                        Nick Name
                      </Col>
                      <Col md="12">
                        {value.nick_name}
                      </Col>
                    </Col>
                  </Row>
                </Collapse>
              </Row>
            );

            return (rows)
          })
        }

      </Container>
    )
  }

  modalAddFectPersonContact() {
    // const {getDimPersonContact} = this.state;
    return (
      <Modal
        show={this.state.isModalOpen}
        onHide={() => this.setModalShow(false)}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Person Contact (เพิ่มข้อมูลผู้มาติดต่อ)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddFactPersonContact
            person_contact_id={this.state.getDimPersonContact.id}
          />
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    const { getPersonResponsible, getCompany } = this.state;
    return (
      <div style={{ paddingTop: "30px" }}>
        {this.modalAddFectPersonContact()}
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row form>
                <Col md="6" className="form-group">
                  <FormLabel>Person Responsible</FormLabel>
                  <p>{getPersonResponsible.person_responsible_full_name}</p>

                  {this.renderErrorFor("person_responsible_full_name")}
                </Col>

                <Col md="6" className="form-group">
                  <FormLabel htmlFor="feCompany">Company</FormLabel>
                  <p>{getCompany.name}</p>
                  {this.renderErrorFor("company_id")}
                </Col>
              </Row>
              <hr className="hr-text" data-content="ข้อมูลบุคลากร" />

              {this.factPersonContact()}

              <Row className="m-t-15">
                <Col md="12">
                  <Button
                    theme="secondary"
                    type="button"
                    onClick={() => this.props.history.goBack()}
                  >
                    Back
                  </Button>
                  &nbsp;
                  <Button
                    type="button"
                    onClick={() => this.setModalShow(true)}
                  >
                    Add New Person Contact
                  </Button>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(ViewPersonContact);
