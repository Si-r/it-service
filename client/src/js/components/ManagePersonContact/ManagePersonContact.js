/* eslint-disable no-unused-vars */
import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManagePersonContact() {
  // const columns = ["id", "company_id", "person_responsible_id", "Action"];
  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Company',
      field: 'dim_company',
      sort: 'asc',
      subField: 'name'
    },
    {
      label: 'Person Responsible',
      field: 'person_responsible_full_name',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
      col: '20%'
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/personcontact"
        columns={columns}
        name="personcontact"
        headname=" List Person Contact - ข้อมูลผู้ติดต่อ "
        headTablename="ตารางแสดงข้อมูลผู้ติดต่อ"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManagePersonContact/AddPersonContact"
        addbutton="Add PersonContact"
        manage="ManagePersonContact"
        updateurl="/ManagePersonContact/UpdatePersonContact"
        viewurl="/ManagePersonContact/ViewPersonContact"
      />
    </div>
  );
}

export default HocValidateUser(ManagePersonContact);
