/* eslint-disable func-names */
/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/require-default-props */
/* eslint-disable react/no-array-index-key */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/button-has-type */
/* eslint-disable new-cap */
/* eslint-disable eqeqeq */
/* eslint-disable no-console */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
import * as reactBootstrapTable from "react-bootstrap-table";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import {
  FaPen,
  FaTrash,
  FaFileExcel,
  FaCheckSquare,
  FaFilePdf,
  FaPlusSquare,
  FaEye
} from "react-icons/fa";
import Swal from "sweetalert2";
import { Container, Row, Col, Card, CardHeader, CardBody, Button } from "shards-react";
import jsPDF from "jspdf";
import { confirmAlert } from "react-confirm-alert";
import SelectViewForm from "./SelectViewForm";
import PageTitle from "../../components/common/PageTitle";

class DataTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      message: "loading..."
    };
  }

  customConfirm = async (_next, dropRowKeys) => {
    const dropRowKeysStr = dropRowKeys;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger"
      },
      buttonsStyling: false
    });
    const result = await swalWithBootstrapButtons.fire({
      title: "คุณแน่ใจว่าจะลบข้อมูลนี้?",
      text: "เมื่อลบแล้วจะไม่สามารถย้อนกลับได้",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "ยืนยันการลบข้อมูล",
      cancelButtonText: "ยกเลิก",
      reverseButtons: true
    });
    if (result.value) {
      axios
        .delete(`${this.props.url}/delete_selected`, { data: { foo: dropRowKeysStr } })
        .then(response => {
          swalWithBootstrapButtons.fire(
            "ลบข้อมูลสำเร็จ!",
            "ข้อมูลได้ถูกลบออกจากตารางแล้ว",
            "success"
          );
          this.RefreshState();
        })
        .catch(e => {
          swalWithBootstrapButtons.fire(
            "ลบข้อมูลไม่สำเร็จ!",
            "เกิดข้อผิดพลาดในการส่งข้อมูล",
            "error"
          );
        });
    } else if (
      // Read more about handling dismissals
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire("ยกเลิก", "ข้อมูลยังไม่ได้ทำการลบออก", "error");
    }
  };

  createHeaders = keys => {
    const newkeys = keys.filter(word => word != "Action" && word != "id");
    return newkeys.map(key => ({
      name: key,
      prompt: key,
      width: 45,
      align: "center",
      padding: 0
    }));
  };

  CreatePdf = () => {
    const doc = new jsPDF();
    // const header = this.createHeaders(this.props.columns);

    // doc.table(1, 1, this.state.products, header);

    doc.save(`${this.props.name}.pdf`);
  };

  createCustomButtonGroup = props => {
    return (
      <reactBootstrapTable.ButtonGroup className="my-custom-class" sizeClass="btn-group-sm">
        <Button
          className="mb-2 mr-2 btn rounded"
          theme="secondary"
          type="button"
          onClick={() => this.RefreshState()}
        >
          Refresh
        </Button>
        <Link
          to={{
            pathname: this.props.addlink
          }}
        >
          <button className="mb-2 mr-2 btn btn-success rounded">
            <i style={{ fontSize: "1.5em", paddingRight: "5px" }}>
              <FaPlusSquare />
            </i>
            {this.props.addbutton}
          </button>
        </Link>
        &nbsp;
        {props.exportCSVBtn}
        {props.insertBtn}
        {props.showSelectedOnlyBtn}
        {props.deleteBtn}
      </reactBootstrapTable.ButtonGroup>
    );
  };

  createCustomPDF = () => {
    return (
      <button
        className="mb-2 mr-2 btn btn-outline-warning rounded  "
        onClick={() => this.CreatePdf()}
      >
        <i style={{ fontSize: "1.5em", color: "#ff8300", paddingRight: "5px" }}>
          <FaFilePdf />
        </i>
        PDF
      </button>
    );
  };

  createCustomDeleteButton = onClick => {
    if (sessionStorage.getItem(`${this.props.manage}Delete`) == "1") {
      return (
        <button onClick={onClick} className="mb-2 mr-2 btn btn-outline-danger rounded">
          <i style={{ fontSize: "1.5em", paddingRight: "5px" }}>
            <FaTrash />
          </i>
          Delete Select
        </button>
      );
    }
    return <p />;
  };

  createCustomExportCSVButton = onClick => {
    return (
      <button onClick={onClick} className="mb-2 mr-2 btn btn-outline-success rounded ">
        <i style={{ fontSize: "1.5em", paddingRight: "5px" }}>
          <FaFileExcel />
        </i>
        Excel
      </button>
    );
  };

  createCustomShowSelectButton = onClick => {
    return (
      <button onClick={onClick} className="mb-2 mr-2 btn btn-outline-primary btn-lg rounded">
        <i style={{ fontSize: "1.5em", paddingRight: "5px" }}>
          <FaCheckSquare />
        </i>
        Show Select
      </button>
    );
  };

  HandleView = id => {
    confirmAlert({
      onClickOutside: () => {
        this.RefreshState();
      },
      customUI: ({ onClose }) => {
        return (
          <div style={{ height: "100%", width: "100%", left: 50 }}>
            <SelectViewForm
              id={id}
              updateurl={`${this.props.url}/update/${id}`}
              ChooseUpdateForm={this.props.name}
            />
            <footer className="modal-footer">
              <button
                type="button"
                className="btn btn-danger"
                onClick={() => {
                  this.RefreshState();
                  onClose();
                }}
              >
                ปิด
              </button>
            </footer>
          </div>
        );
      }
    });
  };

  HandleDelete = async row => {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger"
      },
      buttonsStyling: false
    });
    const result = await swalWithBootstrapButtons.fire({
      title: "คุณแน่ใจว่าจะลบข้อมูลนี้?",
      text: "เมื่อลบแล้วจะไม่สามารถย้อนกลับได้",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "ยืนยันการลบข้อมูล",
      cancelButtonText: "ยกเลิก",
      reverseButtons: true
    });
    if (result.value) {
      axios
        .delete(`${this.props.url}/delete/${row}`)
        .then(response => {
          swalWithBootstrapButtons.fire(
            "ลบข้อมูลสำเร็จ!",
            "ข้อมูลได้ถูกลบออกจากตารางแล้ว",
            "success"
          );
          this.RefreshState();
        })
        .catch(e => {
          swalWithBootstrapButtons.fire(
            "ลบข้อมูลไม่สำเร็จ!",
            "เกิดข้อผิดพลาดในการส่งข้อมูล",
            "error"
          );
        });
    } else if (
      // Read more about handling dismissals
      result.dismiss === Swal.DismissReason.cancel
    ) {
      swalWithBootstrapButtons.fire("ยกเลิก", "ข้อมูลยังไม่ได้ทำการลบออก", "error");
    }
  };

  csvFormatter = (cell, row) => {
    return `${row.id}: ${cell} USD`;
  };

  priceFormatter = (_cell, row) => {
    return (
      <div className="row p-l-r-10">
        {// eslint-disable-next-line no-nested-ternary
        sessionStorage.getItem(`${this.props.manage}View`) == "1" ? (
          this.props.viewurl != undefined ? (
            <div className="">
              <Link to={{ pathname: `${this.props.viewurl}/${row.id}` }}>
                <button className="btn btn-sm btn-success m-l-10">
                  <i>
                    <FaEye />
                  </i>
                  View
                </button>
              </Link>
            </div>
          ) : null
        ) : null}
        {// eslint-disable-next-line no-nested-ternary
        sessionStorage.getItem(`${this.props.manage}Edit`) == "1" ? (
          this.props.updateurl != undefined ? (
            <div className="">
              <Link to={{ pathname: `${this.props.updateurl}/${row.id}` }}>
                <button className="btn btn-sm btn-warning m-l-10">
                  <i>
                    <FaPen />
                  </i>
                  Update
                </button>
              </Link>
            </div>
          ) : null
        ) : null}

        {// eslint-disable-next-line no-nested-ternary
        sessionStorage.getItem(`${this.props.manage}Delete`) == "1" ? (
          <div className="">
            <button
              onClick={() => this.HandleDelete(row.id)}
              className="btn btn-sm btn-danger m-l-10"
            >
              <i>
                <FaTrash />
              </i>
              Delete
            </button>
          </div>
        ) : null}
      </div>
    );
  };

  RefreshState = () => {
    axios
      .get(`${this.props.url}/index`, { headers: { User: sessionStorage.client_id } })
      .then(response => {
        this.setState({
          products: response.data
        });
      })
      .catch(() => {
        Swal.fire("Errors", "Error in use", "error");
      });
  };

  componentDidMount() {
    axios
      .get(`${this.props.url}/index`, { headers: { User: sessionStorage.client_id } })
      .then(response => {
        if (response.data[0] != null) {
          this.setState({
            products: response.data
          });
        } else {
          this.setState({
            message: "ไม่พบข้อมูล"
          });
        }
      })
      .catch(() => {
        Swal.fire("Errors", "Error in use", "error");
      });
  }

  render() {
    const options = {
      noDataText: this.state.message,
      handleConfirmDeleteRow: this.customConfirm,
      defaultSortOrder: "desc",
      exportCSVText: "my_export",
      deleteBtn: this.createCustomDeleteButton,
      exportCSVBtn: this.createCustomExportCSVButton,
      showSelectedOnlyBtn: this.createCustomShowSelectButton,
      insertBtn: this.createCustomPDF,
      btnGroup: this.createCustomButtonGroup
    };

    const selectRow = {
      mode: "checkbox",
      showOnlySelected: true,
      bgColor: "#fff1f3"
    };

    return (
      <Container fluid className="main-content-container px-4">
        {/* Page Header */}
        <Row className="page-header py-4">
          <PageTitle title={this.props.headname} className="text-sm-left" />
        </Row>
        <Row>
          <Col>
            <Card small className="mb-4">
              <CardHeader className="border-bottom">
                <h6 className="m-0">{this.props.headTablename}</h6>
              </CardHeader>
              <CardBody className="p-0 pb-3">
                <div style={{ padding: "25px" }}>
                  <reactBootstrapTable.BootstrapTable
                    data={this.state.products}
                    options={options}
                    selectRow={selectRow}
                    insertRow
                    deleteRow
                    pagination
                    multiColumnSearch
                    search
                    exportCSV
                    striped
                    bordered
                    hover
                  >
                    {this.props.columns.map((colum, idx) => {
                      let col = "";
                      if (colum.col !== undefined) {
                        // eslint-disable-next-line prefer-destructuring
                        col = colum.col;
                      }
                      // Object.values(values).forEach(function (value) {
                      if (colum.field == "id") {
                        return (
                          <reactBootstrapTable.TableHeaderColumn
                            key={idx}
                            dataField={colum.field}
                            searchable={false}
                            isKey
                            hidden
                          >
                            {colum.label}
                          </reactBootstrapTable.TableHeaderColumn>
                        );
                      }
                      if (colum.field == "Action") {
                        if (
                          sessionStorage.getItem(`${this.props.manage}View`) == "1" ||
                          sessionStorage.getItem(`${this.props.manage}Edit`) == "1" ||
                          sessionStorage.getItem(`${this.props.manage}Delete`) == "1"
                        ) {
                          return (
                            <reactBootstrapTable.TableHeaderColumn
                              key={idx}
                              dataFormat={this.priceFormatter}
                              dataField="action"
                              headerAlign="center"
                              dataAlign="center"
                              ali
                              export={false}
                              width={col}
                            >
                              Action
                            </reactBootstrapTable.TableHeaderColumn>
                          );
                        }
                      } else if (colum.subField !== undefined) {
                          const td = (
                            <reactBootstrapTable.TableHeaderColumn
                              key={idx}
                              tdStyle={{ whiteSpace: "normal" }}
                              dataField={colum.field}
                              dataFormat={cell => {
                                try {
                                  return cell[colum.subField];
                                } catch (error) {
                                  return "-";
                                }
                              }}
                              width={col}
                            >
                              {colum.label}
                            </reactBootstrapTable.TableHeaderColumn>
                          );
                          return td;
                      } else {
                        return (
                          <reactBootstrapTable.TableHeaderColumn
                            key={idx}
                            tdStyle={{ whiteSpace: "normal" }}
                            dataField={colum.field}
                            dataFormat={cell => {
                              try {
                                return cell != null ? cell:"-";
                              } catch (error) {
                                return "";
                              }
                            }}
                            width={col}
                          >
                            {colum.label}
                          </reactBootstrapTable.TableHeaderColumn>
                        );
                      }
                    })}
                  </reactBootstrapTable.BootstrapTable>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

DataTable.propTypes = {
  name: PropTypes.string,
  headname: PropTypes.string,
  headTablename: PropTypes.string,
  edit: PropTypes.string,
  review: PropTypes.string,
  addlink: PropTypes.string,
  addbutton: PropTypes.string,
  columns: PropTypes.array,
  manage: PropTypes.string,
  updateurl: PropTypes.string,
  viewurl: PropTypes.string
};

export default DataTable;
