import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";

import HocValidateUser from "../../../HocValidateUser";

class UpdateProvince extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",

      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      name: this.state.name
    };
    console.log(insertdata);

    axios
      .put(`/api/province/update/${this.props.match.params.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  componentDidMount() {
    axios
      .get(`/api/province/update/${this.props.match.params.id}`)
      .then(res => {
        this.setState({
          name: res.data.user.name
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="12" className="form-group">
                        <label htmlFor="feEmailAddress">Name</label>
                        <FormInput
                          id="name"
                          name="name"
                          className={`form-control ${this.hasErrorFor("name") ? "is-invalid" : ""}`}
                          placeholder="กรอกชื่อผู้ใช้"
                          type="text"
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("name")}
                      </Col>
                    </Row>

                    <Button theme="secondary" type="button" onClick={() => this.props.history.goBack()}>
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Update Province</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(UpdateProvince);
