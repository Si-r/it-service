import React, * as react from "react";
import {
  Row,
  Col,
  CardHeader,
  CardBody,
} from "shards-react";
import Axios from "axios";
import SidebarActions from "../../../components/add-new-post/SidebarActions";
import SidebarCategories from "../../../components/add-new-post/SidebarCategories";
import SidebarLine from "../../../components/add-new-post/SidebarLine";
import ModifyItems from "./ModifyItems";
import NewsItems from "./NewsItems";

class Profile extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      firstName: "",
      lastName: "",
      nickName: "",
      telephone: "",
      telephoneEmergency: "",
    };
  }

  componentDidMount() {
    Axios
      .get(`/api/user/profile/${sessionStorage.client_id}`)
      .then(res => {
        this.setState({
          firstName: res.data.first_name,
          lastName: res.data.last_name,
          nickName: res.data.nick_name,
          telephone: res.data.telephone,
          telephoneEmergency: res.data.telephone_emergency,
        });
      })
      .catch(er => {
        console.log(er);
      });
  }

  render() {
    return (
      <div>
        <CardHeader className="border-bottom">
          <h6 className="m-0">{`Welcome ${sessionStorage.email}`}</h6>
        </CardHeader>
        <CardBody className="pt-10 ">
          <Row>
            <Col sm="9">
              <Row>
                <ModifyItems />
              </Row>
              <Row className="mt-2">
                <NewsItems />
              </Row>
              &nbsp;
            </Col>
            <Col lg="3" md="12">
              <SidebarActions firstName={this.state.firstName} lastName={this.state.lastName} />
              <SidebarCategories />
              <SidebarLine />
            </Col>
          </Row>
          <canvas height="120" ref={this.canvasRef} style={{ maxWidth: "100% !important" }} />
        </CardBody>
      </div>
    );
  }
}

export default Profile;
