import React, * as react from "react";
import {
  Card,
  Row,
  Col,
  CardHeader,
  CardBody,
  Button
} from "shards-react";
import Axios from "axios";
import Modal from "react-bootstrap/Modal";
import ViewModify from "../ManageModify/ViewModify";

class ModifyItems extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      modifyItems: [],
      isModalModify: false,
      modifyId: ""
    };

    this.setModalShow = this.setModalShow.bind(this);
  }

  componentDidMount() {
    Axios
      .get("/api/modify/indexWelcome")
      .then(res => {
        this.setState({
          modifyItems: res.data
        });
      })
      .catch(er => {
        console.log(er);
      });
  }

  setModalShow(isOpen) {
    this.setState({
      isModalModify: isOpen
    });
  }

  modalModify() {
    return (
      <Modal
        show={this.state.isModalModify}
        onHide={() => this.setModalShow(false)}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Modify #
            {this.state.modifyId}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ViewModify viewId={this.state.modifyId} />
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    const {modifyItems} = this.state
    return (
      <Card className="col-12 mb-12 h-100">
        {this.modalModify()}
        <CardHeader className="border-bottom">
          <h6 className="m-0">Modify</h6>
        </CardHeader>
        <CardBody className="pt-2 pl-3">
          <Row>
            <Col sm="3" className="">
              ชื่อ
            </Col>
            <Col sm="3">
              ผู้รับผิดชอบ
            </Col>
            <Col sm="3">
              วันที่สร้าง
            </Col>
            <Col sm="3">
              link
            </Col>
          </Row>
          {modifyItems.map((modifyItem,index) => (
            <Row key={index.toString()}>
              <Col sm="3" className="">
                {modifyItem.name}
              </Col>
              <Col sm="3">
                {modifyItem.full_name}
              </Col>
              <Col sm="3">
                {modifyItem.created_at}
              </Col>
              <Col sm="3">
                <Button
                  theme="secondary"
                  type="button"
                  onClick={() => this.setState({
                    isModalModify: true,
                    modifyId: modifyItem.id
                  })}
                >
                  View
                </Button>
              </Col>
            </Row>
            ))
          }
        </CardBody>
      </Card>
    );
  }
}

export default ModifyItems;
