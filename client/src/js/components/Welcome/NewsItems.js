import React, * as react from "react";
import {
  Card,
  Row,
  Col,
  CardHeader,
  CardBody,
  Button
} from "shards-react";
import Modal from "react-bootstrap/Modal";
import Axios from "axios";
import ViewNews from "../ManageNews/ViewNews";

class NewsItems extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      newsItems: [],
      isModalNews : false,
      newsId: ""
    };

    this.setModalShow = this.setModalShow.bind(this);
  }

  componentDidMount() {
    Axios
      .get("/api/news/indexWelcome")
      .then(res => {
        this.setState({
          newsItems: res.data.newsItems
        });
      })
      .catch(er => {
        console.log(er);
      });
  }

  setModalShow(isOpen) {
    this.setState({
      isModalNews: isOpen
    });
  }

  modalNews() {
    return (
      <Modal
        show={this.state.isModalNews}
        onHide={() => this.setModalShow(false)}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Modify #
            {this.state.newsId}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ViewNews viewId={this.state.newsId} />
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    const {newsItems} = this.state
    return (
      <Card className="col-12 mb-12 h-100">
        {this.modalNews()}
        <CardHeader className="border-bottom">
          <h6 className="m-0">News</h6>
        </CardHeader>
        <CardBody className="pt-2 pl-3">
          <Row>
            <Col sm="3" className="">
              ชื่อ
            </Col>
            <Col sm="3">
              ผู้สร้าง
            </Col>
            <Col sm="3">
              วันที่สร้าง
            </Col>
            <Col sm="3">
              link
            </Col>
          </Row>
          {newsItems.map((newsItem,index) => (
            <Row key={index.toString()}>
              <Col sm="3" className="">
                {newsItem.name}
              </Col>
              <Col sm="3">
                {newsItem.client.username}
              </Col>
              <Col sm="3">
                {newsItem.created_at}
              </Col>
              <Col sm="3">
                <Button
                  theme="secondary"
                  type="button"
                  onClick={() => this.setState({
                    isModalNews: true,
                    newsId: newsItem.id
                  })}
                >
                  View
                </Button>
              </Col>
            </Row>
            ))
          }
        </CardBody>
      </Card>
    );
  }
}

export default NewsItems;
