import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import { DatePickerInput } from 'rc-datepicker';
import { FormLabel } from "@material-ui/core";
import moment from "moment";
import HocValidateUser from "../../../HocValidateUser";

const today = new Date();
const date = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
const time = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;
const dateTime = `${date} ${time}`;

class AddRequestGeneral extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",
      status: "Pending",
      access_date: "",
      access_time: "",
      return_date: "",
      equipment_id: "",
      access_sort: "Hour",
      auditor_user_id: this.props.client_id,
      approval_user_id: this.props.client_id,
      audit_timestamp: dateTime,
      approval_timestamp: dateTime,
      getEquipment: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleChange(targetName, strDate) {
    this.setState({
      [targetName]: moment(strDate).format('YYYY-MM-DD HH:mm:ss')
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      name: this.state.name,
      status: this.state.status,
      access_date:  this.state.access_date,
      access_time:  this.state.access_time,
      return_date:  this.state.return_date,
      equipment_id: this.state.equipment_id,
      access_sort: this.state.access_sort,
    };

    axios
      .post("/api/requestgeneral/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return ("");
  }

  componentDidMount() {
    axios
    .get("/api/equipment/index")
    .then(res => {
      this.setState({
        getEquipment: res.data
      });
    })
    .catch(err => {
      console.log(err);
      this.setState({
        getEquipment: []
      });
    });
  }

  render() {
    const { getEquipment } = this.state;
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="name">Title</FormLabel>
                        <FormInput
                          id="name"
                          name="name"
                          className={`form-control ${this.hasErrorFor("name") ? "is-invalid" : ""}`}
                          placeholder="หัวข้อคำร้อง"
                          type="text"
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("name")}
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="status">Status</FormLabel>
                        <FormSelect
                          id="status"
                          name="status"
                          className={`form-control ${
                            this.hasErrorFor("status") ? "is-invalid" : ""
                          }`}
                          value={this.state.status}
                          onChange={this.handleFieldChange}
                        >
                          <option value="Pending">Pending</option>
                        </FormSelect>
                        {this.renderErrorFor("status")}
                      </Col>
                    </Row>

                    <hr className="hr-text" data-content="ข้อมูลใช้งานอุปกรณ์" />

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="Equipment">Equipment</FormLabel>
                        <FormSelect
                          id="Equipment"
                          name="equipment_id"
                          className={`form-control ${
                            this.hasErrorFor("equipment_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.person_responsible_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getEquipment.map((equipment, idx) => (
                            <option key={idx.toString()} value={equipment.id}>
                              {equipment.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("equipment_id")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="AccessDate">Access Date วันที่ขอเข้าใช้งาน</FormLabel>
                        <DatePickerInput
                          id="AccessDate"
                          name="access_date"
                          onChange={(e) => this.handleChange("access_date", e)}
                          value={this.state.access_date}
                          showOnInputClick
                          returnFormat="YYYY-MM-DD HH:mm:ss"
                          format="YYYY-MM-DD HH:mm:ss"
                          floating
                          className={`form-datepicker ${
                            this.hasErrorFor("access_date") ? "is-invalid" : ""
                          }`}
                        />
                      </Col>

                      <Col md="4" className="form-group">
                        <FormLabel htmlFor="access_time">Access time เวลาที่ขอเข้าใช้งาน</FormLabel>
                        <FormInput
                          id="access_time"
                          name="access_time"
                          className={`form-control ${
                            this.hasErrorFor("access_time") ? "is-invalid" : ""
                          }`}
                          placeholder="จำนวนเวลา"
                          type="text"
                          value={this.state.access_time}
                          onChange={this.handleFieldChange}
                        />
                      </Col>
                      <Col md="2" className="form-group">
                        <FormLabel htmlFor="access_time">Unit time หน่วย</FormLabel>
                        <FormSelect
                          id="access_sort"
                          name="access_sort"
                          value={this.state.access_sort}
                          onChange={this.handleFieldChange}
                          className={`form-control ${
                            this.hasErrorFor("access_sort") ? "is-invalid" : ""
                          }`}
                        >
                          <option value="Hour">Hour</option>
                          <option value="Day">Day</option>
                          <option value="Year">Year</option>
                        </FormSelect>
                      </Col>

                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="ReturnDate">Return Date วันที่คืน</FormLabel>
                        <DatePickerInput
                          id="ReturnDate"
                          name="return_date"
                          onChange={(e) => this.handleChange("return_date", e)}
                          value={this.state.return_date}
                          returnFormat="YYYY-MM-DD HH:mm:ss"
                          format="YYYY-MM-DD HH:mm:ss"
                          floating
                          showOnInputClick
                          className={`form-datepicker ${
                            this.hasErrorFor("return_date") ? "is-invalid" : ""
                          }`}
                        />
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Create New Request Genaral</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddRequestGeneral);
