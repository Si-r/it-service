import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManageRequestGeneral() {
  // const columns = [
  //   "id",
  //   "name",
  //   "status",
  //   "auditor_user_id",
  //   "approval_user_id",
  //   "audit_timestamp",
  //   "approval_timestamp",
  //   "Action"
  // ];

  const columns = [
    {
      label: "ID",
      field: "id",
      sort: "asc"
    },
    {
      label: "Name",
      field: "name",
      sort: "asc"
    },
    {
      label: "status",
      field: "status",
      sort: "asc"
    },
    {
      label: "Approval By",
      field: "approval_user",
      subField: "username",
      sort: "name"
    },
    {
      label: "Audit Timestamp",
      field: "audit_timestamp",
      sort: "asc"
    },
    {
      label: "Approval Timestamp",
      field: "approval_timestamp",
      sort: "asc"
    },
    {
      label: "Action",
      field: "Action",
      sort: "asc",
      col: "20%"
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/requestgeneral"
        columns={columns}
        name="requestgeneral"
        headname=" List Request General - ข้อมูลคำขอทั่วไป"
        headTablename="ตารางแสดงข้อมูลคำขอทั่วไป"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageRequestGeneral/AddRequestGeneral"
        addbutton="Add RequestGeneral"
        manage="ManageRequestGeneral"
        updateurl="/ManageRequestGeneral/UpdateRequestGeneral"
        viewurl="/ManageRequestGeneral/ViewRequestGeneral"
      />
    </div>
  );
}

export default HocValidateUser(ManageRequestGeneral);
