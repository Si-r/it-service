import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  Button,
  Container,
  FormSelect
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import Modal from "react-bootstrap/Modal";
import { FormLabel } from "@material-ui/core";
import moment from "moment";
import HocValidateUser from "../../../HocValidateUser";
import AddFactRequestGeneral from "./AddFactRequestGeneral";

const today = new Date();
const date = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
const time = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;
const dateTime = `${date} ${time}`;

class UpdateRequestGeneral extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      id: "",
      name: "",
      status: "",
      access_date: "",
      access_time: "",
      return_date: "",
      equipment_id: "",
      access_sort: "Hour",
      auditor_user_id: sessionStorage.client_id,
      approval_user_id: sessionStorage.client_id,

      isModelFactRequestGeneral: false,

      getRequestGeneral: [],
      audit_timestamp: dateTime,
      approval_timestamp: dateTime,
      getEquipment: [],
      getFactRequestGeneral: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.setModalShow = this.setModalShow.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSelectChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleChange(targetName, strDate) {
    this.setState({
      [targetName]: moment(strDate).format('YYYY-MM-DD HH:mm:ss')
    });
  }

  handleUpdate(event) {
    event.preventDefault();
    let insertdata;
    if(this.state.status === "Approve" || this.state.status === "Rejected") {
      insertdata = {
        client_id: sessionStorage.client_id,
        name: this.state.name,
        status: this.state.status,
        access_date:  this.state.access_date,
        access_time:  this.state.access_time,
        return_date:  this.state.return_date,
        equipment_id: this.state.equipment_id,
        access_sort: this.state.access_sort,
        approval_user_id: sessionStorage.client_id,
        approval_timestamp: this.state.approval_timestamp
      };
    }else {
      insertdata = {
        client_id: sessionStorage.client_id,
        name: this.state.name,
        status: this.state.status,
        access_date:  this.state.access_date,
        access_time:  this.state.access_time,
        return_date:  this.state.return_date,
        equipment_id: this.state.equipment_id,
        access_sort: this.state.access_sort
      };
    }

    axios
      .put(`/api/requestgeneral/update/${this.props.match.params.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Update data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return ("");
  }

  componentDidMount() {
    axios
    .get(`/api/requestgeneral/select/${this.props.match.params.id}`)
    .then(res => {
      this.setState({
        getRequestGeneral: res.data,
        id: res.data.id,
        name: res.data.name,
        status: res.data.status,
        getFactRequestGeneral: res.data.fact_request_general
      }, () => {
        if(sessionStorage.user_right === "staff" && this.state.status === "Pending") {
          const insertdata = {
            auditor_user_id: sessionStorage.client_id,
            audit_timestamp: this.state.audit_timestamp
          };
          axios
          .put(`/api/requestgeneral/updateAudit/${this.props.match.params.id}`, insertdata)
          .then(() => {
            this.setState({
              status: 'Auditor',
            });
          });
        }
      });
    })
    .catch(err => {
      console.log(err);
      this.setState({
        getRequestGeneral: []
      });
    });
  }

  equipmentItems() {
    const { getFactRequestGeneral } = this.state;
    return(
      getFactRequestGeneral.map((factRequestGeneral, idx) =>
      (
        <div key={idx.toString()}>
          <hr
            className="hr-text"
            data-content={`ข้อมูลใช้งานอุปกรณ์ ${factRequestGeneral.dim_equipment.name}`}
          />
          <Row form>
            <Col md="6" className="form-group">
              <FormLabel htmlFor="Equipment">Equipment</FormLabel>
              <p>{factRequestGeneral.dim_equipment.name}</p>
            </Col>
          </Row>

          <Row form>
            <Col md="6" className="form-group">
              <FormLabel htmlFor="AccessDate">Access Date วันที่ขอเข้าใช้งาน</FormLabel>
              <p>{factRequestGeneral.access_date}</p>
            </Col>

            <Col md="4" className="form-group">
              <FormLabel htmlFor="access_time">Access time เวลาที่ขอเข้าใช้งาน</FormLabel>
              <p>{factRequestGeneral.access_time}</p>
            </Col>
            <Col md="2" className="form-group">
              <FormLabel htmlFor="access_time">Unit time หน่วย</FormLabel>
              <p>{factRequestGeneral.access_sort}</p>
            </Col>

          </Row>

          <Row form>
            <Col md="6" className="form-group">
              <FormLabel htmlFor="ReturnDate">Return Date วันที่คืน</FormLabel>
              <p>{factRequestGeneral.return_date}</p>
            </Col>
          </Row>
        </div>
      ))
    );
  }

  setModalShow(isOpen) {
    if(isOpen === false) {
      this.componentDidMount();
    }

    this.setState({
      isModelFactRequestGeneral: isOpen
    });
  }

  modalAddFactRequestGeneral() {
    return (
      <Modal
        show={this.state.isModelFactRequestGeneral}
        onHide={() => this.setModalShow(false)}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Equipment RequestGeneral
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddFactRequestGeneral
            request_general_id={this.state.getRequestGeneral.id}
          />
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        {this.modalAddFactRequestGeneral()}
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleUpdate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="name">Title</FormLabel>
                        <p>{this.state.name}</p>
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="status">Status</FormLabel>
                        <FormSelect
                          id="status"
                          name="status"
                          className={`form-control ${
                            this.hasErrorFor("status") ? "is-invalid" : ""
                          }`}
                          value={this.state.status}
                          onChange={this.handleSelectChange}
                        >
                          <option value="">Choose...</option>
                          <option value="Pending">Pending</option>
                          <option value="Approve">Approve</option>
                          <option value="Auditor">Auditor</option>
                          <option value="Rejected">Rejected</option>
                        </FormSelect>
                      </Col>
                    </Row>
                    {this.equipmentItems()}
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Update Request General</Button>
                    &nbsp;
                    <Button
                      type="button"
                      onClick={() => this.setModalShow(true)}

                    >
                      New Equipment
                    </Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(UpdateRequestGeneral);
