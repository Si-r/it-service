import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import { FormLabel } from "@material-ui/core";
import moment from "moment";
import HocValidateUser from "../../../HocValidateUser";

const today = new Date();
const date = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
const time = `${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;
const dateTime = `${date} ${time}`;

class ViewRequestGeneral extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",
      status: "",
      access_date: "",
      access_time: "",
      return_date: "",
      equipment_id: "",
      access_sort: "Hour",
      auditor_user_id: this.props.client_id,
      approval_user_id: this.props.client_id,
      getRequestGeneral: [],
      audit_timestamp: dateTime,
      approval_timestamp: dateTime,
      getEquipment: [],
      getFactRequestGeneral: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleChange(targetName, strDate) {
    this.setState({
      [targetName]: moment(strDate).format('YYYY-MM-DD HH:mm:ss')
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      name: this.state.name,
      status: this.state.status,
      access_date:  this.state.access_date,
      access_time:  this.state.access_time,
      return_date:  this.state.return_date,
      equipment_id: this.state.equipment_id,
      access_sort: this.state.access_sort,
    };

    axios
      .post("/api/requestgeneral/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return ("");
  }

  componentDidMount() {
    axios
    .get(`/api/requestgeneral/select/${this.props.match.params.id}`)
    .then(res => {
      this.setState({
        getRequestGeneral: res.data,
        name: res.data.name,
        status: res.data.status,
        getFactRequestGeneral: res.data.fact_request_general
      });
    })
    .catch(err => {
      console.log(err);
      this.setState({
        getRequestGeneral: []
      });
    });
  }

  equipmentItems() {
    const { getFactRequestGeneral } = this.state;
    return(
      getFactRequestGeneral.map((factRequestGeneral, idx) =>
      (
        <div key={idx.toString()}>
          <hr
            className="hr-text"
            data-content={`ข้อมูลใช้งานอุปกรณ์ ${factRequestGeneral.dim_equipment.name}`}
          />
          <Row form>
            <Col md="6" className="form-group">
              <FormLabel htmlFor="Equipment">Equipment</FormLabel>
              <p>{factRequestGeneral.dim_equipment.name}</p>
            </Col>
          </Row>

          <Row form>
            <Col md="6" className="form-group">
              <FormLabel htmlFor="AccessDate">Access Date วันที่ขอเข้าใช้งาน</FormLabel>
              <p>{factRequestGeneral.access_date}</p>
            </Col>

            <Col md="4" className="form-group">
              <FormLabel htmlFor="access_time">Access time เวลาที่ขอเข้าใช้งาน</FormLabel>
              <p>{factRequestGeneral.access_time}</p>
            </Col>
            <Col md="2" className="form-group">
              <FormLabel htmlFor="access_time">Unit time หน่วย</FormLabel>
              <p>{factRequestGeneral.access_sort}</p>
            </Col>

          </Row>

          <Row form>
            <Col md="6" className="form-group">
              <FormLabel htmlFor="ReturnDate">Return Date วันที่คืน</FormLabel>
              <p>{factRequestGeneral.return_date}</p>
            </Col>
          </Row>
        </div>
      ))
    );
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="name">Title</FormLabel>
                        <p>{this.state.name}</p>
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="status">Status</FormLabel>
                        <p>{this.state.status}</p>
                      </Col>
                    </Row>
                    {this.equipmentItems()}
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(ViewRequestGeneral);
