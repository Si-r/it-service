import React, * as react from "react";
import ImageUploader from "react-images-upload";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormGroup,
  FormSelect,
  Button,
  Container
} from "shards-react";
import { FormLabel } from "@material-ui/core";

import Swal from "sweetalert2";
import axios from "axios";
import HocValidateUser from "../../../HocValidateUser";

class AddUser extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      email: "",
      is_block: "block",
      user_right: "admin",
      image_show: "default",
      image_id: "1",
      image: [],
      client_id: sessionStorage.client_id,
      permission_id: "1",
      getpermission: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.onChangeUploadFile = this.onChangeUploadFile.bind(this);
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(pictureDataURLs) {
    this.setState({
      // eslint-disable-next-line react/no-access-state-in-setstate
      image: this.state.image.concat(pictureDataURLs)
    });
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  onChangeUploadFile(event) {
    this.setState({
      image: event.target.files[0]
    });
    console.log(this.state.selectedFile);
  }

  handleCreate(event) {
    event.preventDefault();

    const formData = {
      image: this.state.image[this.state.image.length - 1],
      client_id: sessionStorage.client_id
    };

    console.log(formData);

    axios
      .post("/api/uploadImage", formData)
      .then(res => {
        const insertdata = {
          username: this.state.username,
          password: this.state.password,
          email: this.state.email,
          is_block: this.state.is_block,
          user_right: this.state.user_right,
          image_show: this.state.image_show,
          image_id: this.state.image_id,
          client_id: sessionStorage.client_id,
          permission_id: this.state.permission_id
        };
        console.log(insertdata);
        axios
          .post("/api/user/store", insertdata)
          .then(() => {
            Swal.fire("Successfully", "Add data successfully ", "success");

            this.setState({
              username: "",
              password: "",
              email: "",
              is_block: "",
              user_right: "",
              image_show: "",
              image_id: "",
              client_id: "",
              permission_id: "",
              errors: []
            });
          })
          .catch(error => {
            this.setState({
              errors: error.response.data.errors
            });
            console.log(error.response.data.errors);

            Swal.fire("Errors", "check the value of a form field", "error");
          });
      })
      .catch(err => {
        console.log(err);
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get("/api/permission/index")
      .then(res => {

        this.setState({
          getpermission: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getpermission: []
        });
      });
  }

  render() {
    const { getpermission } = this.state;
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feUsername">Username</FormLabel>
                        <FormInput
                          id="feUsername"
                          name="username"
                          className={`form-control ${
                            this.hasErrorFor("username") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อผู้ใช้"
                          type="text"
                          value={this.state.username}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("username")}
                      </Col>
                      <Col md="6">
                        <FormLabel htmlFor="fePassword">Password</FormLabel>
                        <FormInput
                          id="fePassword"
                          name="password"
                          className={`form-control ${
                            this.hasErrorFor("password") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกรหัสผ่าน"
                          type="password"
                          value={this.state.password}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("password")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6">
                        <FormGroup>
                          <FormLabel htmlFor="feInputEmail">Email</FormLabel>
                          <FormInput
                            id="feInputEmail"
                            className={`form-control ${
                              this.hasErrorFor("email") ? "is-invalid" : ""
                            }`}
                            placeholder="กรอกอีเมล"
                            type="email"
                            name="email"
                            value={this.state.email}
                            onChange={this.handleFieldChange}
                          />
                          {this.renderErrorFor("email")}
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="4" className="form-group">
                        <FormLabel htmlFor="feBlock">Block</FormLabel>
                        <FormSelect
                          id="feBlock"
                          name="is_block"
                          value={this.state.is_block}
                          onChange={this.handleFieldChange}
                        >
                          <option value="block">block</option>
                          <option value="unblock">unblock</option>
                        </FormSelect>
                      </Col>

                      <Col md="4" className="form-group">
                        <FormLabel htmlFor="feUserRight">User Right</FormLabel>
                        <FormSelect
                          id="feUserRight"
                          name="user_right"
                          value={this.state.user_right}
                          onChange={this.handleFieldChange}
                        >
                          <option value="admin">admin</option>
                          <option value="staff">staff</option>
                          <option value="member">member</option>
                        </FormSelect>
                      </Col>

                      <Col md="4" className="form-group">
                        <FormLabel htmlFor="fePermission">Permission</FormLabel>
                        <FormSelect
                          id="fePermission"
                          name="permission_id"
                          className={`form-control ${
                            this.hasErrorFor("permission_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.permission_id}
                          onChange={this.handleFieldChange}
                        >
                          {getpermission.map((getpermission,idx) => (
                            <option key={idx} value={getpermission.id}>
                              {getpermission.permission_name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("permission_id")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feImageShow">Image Show</FormLabel>
                        <FormSelect
                          id="feImageShow"
                          name="image_show"
                          value={this.state.image_show}
                          onChange={this.handleFieldChange}
                        >
                          <option value="default">default</option>
                          <option value="image1">image1</option>
                          <option value="image2">image2</option>
                        </FormSelect>
                      </Col>

                      <Col md="12" className="form-group">
                        <div className="form-group">
                          <FormLabel htmlFor="feImage">เลือกรูปภาพโปรไฟล์</FormLabel>
                          <input
                            className={`form-control ${
                              this.hasErrorFor("image") ? "is-invalid" : ""
                            }`}
                            hidden
                          />
                          <ImageUploader
                            type="file"
                            withIcon
                            value={this.state.image}
                            buttonText="เลือกรูปภาพ"
                            onChange={this.onDrop}
                            imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                            maxFileSize={2020215}
                            singleImage
                            label="ไฟล์ขนาดต้องไม่เกิน 2 MB, สกุลไฟล์: jpg,png,gif"
                            withPreview
                            fileSizeError="ขนาดไฟล์ใหญ่เกินไป"
                            fileTypeError="ประเภทไฟล์ไม่ถูกต้อง"
                          />
                          {this.renderErrorFor("image")}
                        </div>
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Create New Account</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddUser);
