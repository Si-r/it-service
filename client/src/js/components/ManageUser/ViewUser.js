import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormGroup,
  FormSelect,
  Container,
  Button
} from "shards-react";

import { FormLabel } from "@material-ui/core";

import Swal from "sweetalert2";
import axios from "axios";
import HocValidateUser from "../../../HocValidateUser";

class ViewUser extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkboxpassword: "",
      username: "",
      password: "",
      email: "",
      is_block: "block",
      user_right: "admin",
      image_show: "default",
      image_id: "1",
      client_id: "1",
      permission_id: "",
      getpermission:[],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(pictureDataURLs) {
    this.setState({
      // eslint-disable-next-line react/no-access-state-in-setstate
      image: this.state.image.concat(pictureDataURLs)
    });
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSelectChange(event) {
    console.log(event.target.value);
    this.setState({
      checkboxpassword: !event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();
    const insertdata = {
      username: this.state.username,
      password: this.state.password,
      email: this.state.email,
      is_block: this.state.is_block,
      user_right: this.state.user_right,
      image_show: this.state.image_show,
      image_id: this.state.image_id,
      client_id: this.state.client_id,
      permission_id: this.state.permission_id
    };

    console.log(insertdata);

    axios
      .put(`/api/user/update/${this.props.match.params.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Update data successfully ", "success");
      })
      .catch(error => {
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
        this.setState({
          errors: error.response.data.errors
        });
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {

    axios
      .get(`/api/user/update/${this.props.match.params.id}`)
      .then(res => {
        this.setState({
          username: res.data.user.username,
          email: res.data.user.email,
          is_block: res.data.user.is_block,
          user_right: res.data.user.user_right,
          image_show: res.data.user.image_show,
          image_id: res.data.user.image_id,
          client_id: res.data.user.client_id,
          permission_id: res.data.user.permission_id
        });

        console.log(res.data.user);
      })
      .catch(er => {
        console.log(er);
      });

      axios
        .get("/api/permission/index")
        .then(res => {


          this.setState({
            getpermission: res.data
          });

          console.log(this.state.getpermission);
        })
        .catch(err => {
          console.log(err);
          this.setState({
            getpermission: []
          });
        });
  }

  render() {
    const {getpermission} = this.state
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="username">Username</FormLabel>
                        <FormInput
                          id="username"
                          name="username"
                          className={`form-control ${
                            this.hasErrorFor("username") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อผู้ใช้"
                          type="text"
                          value={this.state.username}
                          onChange={this.handleFieldChange}
                          disabled

                        />
                        {this.renderErrorFor("username")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6">
                        <FormGroup>
                          <FormLabel htmlFor="email">Email</FormLabel>
                          <FormInput
                            id="email"
                            className={`form-control ${
                              this.hasErrorFor("email") ? "is-invalid" : ""
                            }`}
                            placeholder="กรอกอีเมล"
                            type="email"
                            name="email"
                            value={this.state.email}
                            onChange={this.handleFieldChange}
                            disabled

                          />
                          {this.renderErrorFor("email")}
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="4" className="form-group">
                        <FormLabel htmlFor="feIsblock">Block</FormLabel>
                        <FormSelect
                          id="feIsblock"
                          name="is_block"
                          value={this.state.is_block}
                          onChange={this.handleFieldChange}
                          disabled
                        >
                          <option value="block">block</option>
                          <option value="unblock">unblock</option>
                        </FormSelect>
                      </Col>

                      <Col md="4" className="form-group">
                        <FormLabel htmlFor="feUserRight">User Right</FormLabel>
                        <FormSelect
                          id="feUserRight"
                          name="user_right"
                          value={this.state.user_right}
                          onChange={this.handleFieldChange}
                          disabled
                        >
                          <option value="admin">admin</option>
                          <option value="staff">staff</option>
                        </FormSelect>
                      </Col>

                      <Col md="4" className="form-group">
                        <FormLabel htmlFor="fePermission">Permission</FormLabel>
                        <FormSelect
                          id="fePermission"
                          name="permission_id"
                          value={this.state.permission_id}
                          onChange={this.handleFieldChange}
                          disabled
                        >
                          {
                          getpermission.map((name,index) => (
                            <option key={index.toString()} value={name.id}>
                              {name.permission_name}
                            </option>
                          ))

                        }

                        </FormSelect>
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feImageShow">Image Show</FormLabel>
                        <FormSelect
                          id="feImageShow"
                          name="image_show"
                          value={this.state.image_show}
                          onChange={this.handleFieldChange}
                          disabled
                        >
                          <option value="default">default</option>
                          <option value="image1">image1</option>
                          <option value="image2">image2</option>
                        </FormSelect>
                      </Col>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feInputState">Image</FormLabel>
                        <FormSelect
                          id="feInputState"
                          name="image_id"
                          value={this.state.image_id}
                          onChange={this.handleFieldChange}
                          disabled
                        >
                          <option value="1">1</option>
                        </FormSelect>
                      </Col>

                      <Col md="12" className="form-group" />
                      <Button
                        theme="secondary"
                        type="button"
                        onClick={() => this.props.history.goBack()}
                      >
                        Back
                      </Button>

                    &nbsp;
                    </Row>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(ViewUser);
