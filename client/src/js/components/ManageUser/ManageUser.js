import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManageUser() {
  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Username',
      field: 'username',
      sort: 'asc',
    },
    {
      label: 'E-mail',
      field: 'email',
      sort: 'asc',
    },
    {
      label: 'Active',
      field: 'is_block',
      sort: 'asc',
    },
    {
      label: 'User right',
      field: 'user_right',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/user"
        columns={columns}
        name="user"
        headname=" List User - ข้อมูลผู้ใช้งาน "
        headTablename="ตารางแสดงข้อมูลสมาชิก"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageUser/AddUser"
        addbutton="Add user"
        manage="ManageUser"
        updateurl="/ManageUser/UpdateUser"
        viewurl="/ManageUser/ViewUser"
      />
    </div>
  );
}

export default HocValidateUser(ManageUser);
