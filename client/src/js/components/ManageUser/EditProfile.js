import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  Container,
  Button
} from "shards-react";

import { FormLabel } from "@material-ui/core";

import Swal from "sweetalert2";
import axios from "axios";
import HocValidateUser from "../../../HocValidateUser";

class UpdateUser extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      firstName: "",
      lastName: "",
      nickName: "",
      telephone: "",
      telephoneEmergency: "",
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(pictureDataURLs) {
    this.setState({
      // eslint-disable-next-line react/no-access-state-in-setstate
      image: this.state.image.concat(pictureDataURLs)
    });
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleUpdate(event) {
    event.preventDefault();
    const insertdata = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      nickName: this.state.nickName,
      telephone: this.state.telephone,
      telephoneEmergency: this.state.telephoneEmergency,
    };

    axios
      .put(`/api/user/updateProfile/${sessionStorage.client_id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Update data successfully ", "success");
      })
      .catch(error => {

        Swal.fire("Errors", "check the value of a form field", "error");
        this.setState({
          errors: error.response.data.errors
        });
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get(`/api/user/profile/${sessionStorage.client_id}`)
      .then(res => {
        this.setState({
          firstName: res.data.first_name,
          lastName: res.data.last_name,
          nickName: res.data.nick_name,
          telephone: res.data.telephone,
          telephoneEmergency: res.data.telephone_emergency,
        });
      })
      .catch(er => {
        console.log(er);
      });
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleUpdate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="firstName">First Name</FormLabel>
                        <FormInput
                          id="firstName"
                          name="firstName"
                          className={`form-control ${
                            this.hasErrorFor("firstName") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อจริง"
                          type="text"
                          value={this.state.firstName}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("firstName")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="lastName">Last Name</FormLabel>
                        <FormInput
                          id="lastName"
                          name="lastName"
                          className={`form-control ${
                            this.hasErrorFor("lastName") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกนามสกุล"
                          type="text"
                          value={this.state.lastName}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("lastName")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="nickName">Nick Name</FormLabel>
                        <FormInput
                          id="nickName"
                          name="nickName"
                          className={`form-control ${
                            this.hasErrorFor("nickName") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อเล่น"
                          type="text"
                          value={this.state.nickName}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("nickName")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="telephone">Telephone</FormLabel>
                        <FormInput
                          id="telephone"
                          name="telephone"
                          className={`form-control ${
                            this.hasErrorFor("telephone") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกเบอร์โทรศัพท์"
                          type="text"
                          value={this.state.telephone}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("telephone")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="telephoneEmergency">Telephone Emergency</FormLabel>
                        <FormInput
                          id="telephoneEmergency"
                          name="telephoneEmergency"
                          className={`form-control ${
                            this.hasErrorFor("telephoneEmergency") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกเบอร์โทรศัพท์  กรณีฉุกเฉิน"
                          type="text"
                          value={this.state.telephoneEmergency}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("telephoneEmergency")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="12" className="form-group">
                        <Button
                          theme="secondary"
                          type="button"
                          onClick={() => this.props.history.goBack()}
                        >
                          Back
                        </Button>
                        &nbsp;
                        <Button type="submit">Update Profile</Button>
                      </Col>
                    </Row>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(UpdateUser);
