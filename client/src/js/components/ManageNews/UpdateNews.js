/* eslint-disable func-names */
import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import FroalaEditor from "react-froala-wysiwyg";
import { FormLabel } from "@material-ui/core";
import HocValidateUser from "../../../HocValidateUser";
import froalaConfig from "../../../data/froala-config";

class UpdateNews extends react.Component {
  constructor(props) {
    super(props);
    this.config = froalaConfig;
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",
      detail: "",
      setting_news_id: "",
      getsettingNews: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.handleModelChange = this.handleModelChange.bind(this);
  }

  handleModelChange(detail) {
    this.setState({
      detail
    });
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      name: this.state.name,
      detail: this.state.detail,
      setting_news_id: this.state.setting_news_id
    };
    console.log(insertdata);

    axios
      .put(`/api/news/update/${this.props.match.params.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          tital: "",
          detail: "",
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return "";
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get("/api/settingnews/index")
      .then(res => {
        this.setState({
          getsettingNews: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getsettingNews: []
        });
      });

    axios
      .get(`/api/news/update/${this.props.match.params.id}`)
      .then(res => {
        this.setState({
          name: res.data.user.name,
          detail: res.data.user.detail,
          setting_news_id: res.data.user.setting_news_id
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { getsettingNews } = this.state;
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="12" className="form-group">
                        <FormLabel htmlFor="name">News Title</FormLabel>
                        <FormInput
                          id="name"
                          name="name"
                          className={`form-control ${this.hasErrorFor("name") ? "is-invalid" : ""}`}
                          placeholder="หัวข้อข่าว"
                          type="text"
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("name")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col xs="12" className="form-group">
                        <div className="form-group">
                          <FormLabel htmlFor="detail">Detail</FormLabel>
                          <FroalaEditor
                            id="detail"
                            className={`form-control ${
                              this.hasErrorFor("detail") ? "is-invalid" : ""
                            }`}
                            tag="textarea"
                            config={this.config}
                            model={this.state.detail}
                            onModelChange={this.handleModelChange}
                          />
                          {this.renderErrorFor("detail")}
                        </div>
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="4" className="form-group">
                        <FormLabel htmlFor="setting_news_id">Setting News</FormLabel>
                        <FormSelect
                          id="setting_news_id"
                          name="setting_news_id"
                          className={`form-control ${
                            this.hasErrorFor("setting_news_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.setting_news_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getsettingNews.map((settingNews, idx) => (
                            <option key={idx.toString()} value={settingNews.id}>
                              {settingNews.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("setting_news_id")}
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Create New Message</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(UpdateNews);
