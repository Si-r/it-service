import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  Button,
  Container
} from "shards-react";
import axios from "axios";
import PropTypes from "prop-types";
import { FormLabel } from "@material-ui/core";
import HocValidateUser from "../../../HocValidateUser";
import froalaConfig from "../../../data/froala-config";

class ViewNews extends react.Component {
  constructor(props) {
    super(props);
    this.config = froalaConfig;
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",
      detail: "",
      setting_news: "",
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
    this.handleModelChange = this.handleModelChange.bind(this);
  }

  handleModelChange(detail) {
    this.setState({
      detail
    });
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return "";
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    let viewId;
    if (this.props.viewId === 0) {
      viewId = this.props.match.params.id;
    } else {
      // eslint-disable-next-line prefer-destructuring
      viewId = this.props.viewId;
    }

    axios
      .get(`/api/news/update/${viewId}`)
      .then(res => {
        this.setState({
          name: res.data.user.name,
          detail: res.data.user.detail,
          setting_news: res.data.user.setting_news
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form>
                    <Row form>
                      <Col md="12" className="form-group">
                        <FormLabel htmlFor="FormInput">News Title</FormLabel>
                        <p>{this.state.name}</p>
                      </Col>
                    </Row>
                    <Row form>
                      <Col xs="12" className="form-group">
                        <div className="form-group">
                          <FormLabel htmlFor="detail">Detail</FormLabel>
                          <div dangerouslySetInnerHTML={{ __html: this.state.detail }} />
                        </div>
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="4" className="form-group">
                        <FormLabel htmlFor="setting_news_id">Setting News</FormLabel>
                        <p>{this.state.setting_news.name}</p>
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

ViewNews.propTypes = {
  viewId: PropTypes.number
};

ViewNews.defaultProps = {
  viewId: 0
};

export default HocValidateUser(ViewNews);
