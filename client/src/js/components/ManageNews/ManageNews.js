import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManageNews() {
  // const columns = ["id", "setting_news_id", "name", "detail", "Action"];

  const columns = [
    {
      label: "ID",
      field: "id",
      sort: "asc"
    },
    {
      label: "Name",
      field: "name",
      sort: "asc"
    },
    {
      label: "status",
      field: "setting_news_id",
      sort: "asc",
      col: "10%"
    },
    {
      label: "Action",
      field: "Action",
      sort: "asc",
      col: "20%"
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/news"
        columns={columns}
        name="news"
        headname=" List News - ข้อมูลข่าว "
        headTablename="ตารางแสดงข้อมูลข่าว"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageNews/AddNews"
        addbutton="Add News"
        manage="ManageNews"
        updateurl="/ManageNews/UpdateNews"
        viewurl="/ManageNews/ViewNews"
      />
    </div>
  );
}

export default HocValidateUser(ManageNews);
