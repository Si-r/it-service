import React, * as react from "react";

import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import { FormLabel } from "@material-ui/core";
import HocValidateUser from "../../../HocValidateUser";

class UpdateAddress extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      province_id: "",
      district_id: "",
      sub_district_id: "",
      postal_code_id: "",

      getprovince: [],
      getdistrict: [],
      getsubDistrict: [],
      getpostalCode: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      province_id: this.state.province_id,
      district_id: this.state.district_id,
      sub_district_id: this.state.sub_district_id,
      postal_code_id: this.state.postal_code_id
    };

    axios
      .put(`/api/address/update/${this.props.match.params.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get(`/api/address/update/${this.props.match.params.id}`)
      .then(res => {
        this.setState({
          province_id: res.data.user.province_id,
          district_id: res.data.user.district_id,
          sub_district_id: res.data.user.sub_district_id,
          postal_code_id: res.data.user.postal_code_id
        });
      })
      .catch(err => {
        console.log(err);
      });

    axios
      .get("/api/province/index")
      .then(res => {
        this.setState({
          getprovince: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getprovince: []
        });
      });

    axios
      .get("/api/district/index")
      .then(res => {
        this.setState({
          getdistrict: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getdistrict: []
        });
      });

    axios
      .get("/api/subdistrict/index")
      .then(res => {
        this.setState({
          getsubDistrict: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getsubDistrict: []
        });
      });

    axios
      .get("/api/postalcode/index")
      .then(res => {
        this.setState({
          getpostalCode: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getpostalCode: []
        });
      });
  }

  render() {
    const { getprovince, getdistrict, getsubDistrict, getpostalCode } = this.state;
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feprovince">Province</FormLabel>
                        <FormSelect
                          id="feprovince"
                          name="province_id"
                          className={`form-control ${
                            this.hasErrorFor("province_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.province_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getprovince.map((province, idx) => (
                            <option key={idx.toString()} value={province.id}>
                              {province.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("province_id")}
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="fedistrict">District</FormLabel>
                        <FormSelect
                          id="fedistrict"
                          name="district_id"
                          className={`form-control ${
                            this.hasErrorFor("district_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.district_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getdistrict.map((district, idx) => (
                            <option key={idx.toString()} value={district.id}>
                              {district.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("district_id")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="fesubDistrictId">Sub District</FormLabel>
                        <FormSelect
                          id="fesubDistrictId"
                          name="sub_district_id"
                          className={`form-control ${
                            this.hasErrorFor("sub_district_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.sub_district_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getsubDistrict.map((subDistrict, idx) => (
                            <option key={idx.toString()} value={subDistrict.id}>
                              {subDistrict.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("sub_district_id")}
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="fepostalCode">Postal code</FormLabel>
                        <FormSelect
                          id="fepostalCode"
                          name="postal_code_id"
                          className={`form-control ${
                            this.hasErrorFor("postal_code_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.postal_code_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getpostalCode.map((postalCode, idx) => (
                            <option key={idx.toString()} value={postalCode.id}>
                              {postalCode.code}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("postal_code_id")}
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Update Address</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(UpdateAddress);
