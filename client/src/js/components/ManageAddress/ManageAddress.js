import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManageAddress() {
  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Province',
      field: 'province',
      subField: 'name',
      sort: 'asc',
    },
    {
      label: 'District',
      field: 'district',
      subField: 'name',
      sort: 'asc',
    },
    {
      label: 'Sub District',
      field: 'sub_district',
      subField: 'name',
      sort: 'asc',
    },
    {
      label: 'Postal Code',
      field: 'postal_code',
      subField: 'code',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/address"
        columns={columns}
        name="address"
        headname=" Management Address - จัดการข้อมูลที่อยู่ "
        headTablename="ตารางแสดงข้อมูลที่อยู่"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageAddress/AddAddress"
        addbutton="Add Address"
        manage="ManageAddress"
        updateurl="/ManageAddress/UpdateAddress"
        viewurl="/ManageAddress/ViewAddress"
      />
    </div>
  );
}

export default HocValidateUser(ManageAddress);
