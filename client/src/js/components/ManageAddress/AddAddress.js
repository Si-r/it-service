import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormSelect,
  Button,
  Container
} from "shards-react";
import { FormLabel } from "@material-ui/core";
import Swal from "sweetalert2";
import axios from "axios";

import HocValidateUser from "../../../HocValidateUser";

class AddAddress extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      province_id: "",
      district_id: "",
      sub_district_id: "",
      postal_code_id: "",

      getprovince: [],
      getdistrict: [],
      getsubDistrict: [],
      getpostalCode: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);

    this.handleProvince = this.handleProvince.bind(this);
    this.handleDistrict = this.handleDistrict.bind(this);
    this.handleSubDistrict = this.handleSubDistrict.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  async handleProvince(event) {
    this.setState({
      [event.target.name]: event.target.value
    });

    axios
      .get(`/api/district/select/${event.target.value}`)
      .then(res => {
        this.setState({
          getdistrict: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getdistrict: []
        });
      });
  }

  async handleDistrict(event) {
    this.setState({
      [event.target.name]: event.target.value
    });

    axios
      .get(`/api/subdistrict/select/${event.target.value}`)
      .then(res => {
        this.setState({
          getsubDistrict: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getsubDistrict: []
        });
      });
  }

  async handleSubDistrict(event) {
    this.setState({
      [event.target.name]: event.target.value
    });

    axios
      .get(`/api/postalcode/select/${event.target.value}`)
      .then(res => {
        this.setState({
          getpostalCode: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getpostalCode: []
        });
      });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      province_id: this.state.province_id,
      district_id: this.state.district_id,
      sub_district_id: this.state.sub_district_id,
      postal_code_id: this.state.postal_code_id
    };

    axios
      .post("/api/address/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          province_id: "",
          district_id: "",
          sub_district_id: "",
          postal_code_id: "",
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get("/api/province/index")
      .then(res => {
        this.setState({
          getprovince: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getprovince: []
        });
      });
  }

  render() {
    const { getprovince, getdistrict, getsubDistrict, getpostalCode } = this.state;

    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="province">Province</FormLabel>
                        <FormSelect
                          id="province"
                          name="province_id"
                          className={`form-control ${
                            this.hasErrorFor("province_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.province_id}
                          onChange={this.handleProvince}
                        >
                          <option value="">Choose...</option>

                          {getprovince.map((province, idx) => (
                            <option key={idx.toString()} value={province.id}>
                              {province.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("province_id")}
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="district">District</FormLabel>
                        <FormSelect
                          id="district"
                          name="district_id"
                          className={`form-control ${
                            this.hasErrorFor("district_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.district_id}
                          onChange={this.handleDistrict}
                        >
                          <option value="">Choose...</option>

                          {getdistrict.map((district, idx) => (
                            <option key={idx.toString()} value={district.id}>
                              {district.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("district_id")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="subDistrict">Sub District</FormLabel>
                        <FormSelect
                          id="subDistrict"
                          name="sub_district_id"
                          className={`form-control ${
                            this.hasErrorFor("sub_district_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.sub_district_id}
                          onChange={this.handleSubDistrict}
                        >
                          <option value="">Choose...</option>

                          {getsubDistrict.map((subDistrict, idx) => (
                            <option key={idx.toString()} value={subDistrict.id}>
                              {subDistrict.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("sub_district_id")}
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="postalCode">Postal code</FormLabel>
                        <FormSelect
                          id="postalCode"
                          name="postal_code_id"
                          className={`form-control ${
                            this.hasErrorFor("postal_code_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.postal_code_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getpostalCode.map((postalCode, idx) => (
                            <option key={idx.toString()} value={postalCode.id}>
                              {postalCode.code}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("postal_code_id")}
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Create New Address</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddAddress);
