/* eslint-disable react/no-danger */
import React, * as react from "react";
import PropTypes from "prop-types";
import { ListGroup, ListGroupItem, Row, Col, Container, Button } from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import HocValidateUser from "../../../HocValidateUser";

class ViewModify extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",
      contact_id: "",
      contact: [],
      getFactModify: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      name: this.state.name,
      contact_id: this.state.contact_id
    };
    console.log(insertdata);

    axios
      .put(`/api/modify/update/${this.props.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          name: "",
          contact_id: "",
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    let viewId;
    if(this.props.viewId === 0)
    {
      viewId = this.props.match.params.id;
    }else {
      // eslint-disable-next-line prefer-destructuring
      viewId = this.props.viewId;
    }

    axios
      .get(`/api/modify/update/${viewId}`)
      .then(res => {
        this.setState({
          name: res.data.name,
          contact: res.data.contact,
          contact_id: res.data.contact_id,
          getFactModify: res.data.fact_modify
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  commentItemList() {
    const { getFactModify } = this.state;
    return (
      <div>
        <Row className="p-t-10 p-b-10">
          <Col md="9">Comment</Col>
          <Col md="3">Price</Col>
        </Row>
        {getFactModify.map(factModify => (
          <Row className="p-t-5 p-b-5">
            <Col md="9">
              <div dangerouslySetInnerHTML={{ __html: factModify.comment }} />
            </Col>
            <Col md="3">
              <div dangerouslySetInnerHTML={{ __html: `${factModify.price} บาท` }} />
            </Col>
          </Row>
        ))}
      </div>
    );
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row className="p-t-10 p-b-10">
                <Col className="text-right" md="2">
                  Modify Title
                </Col>
                <Col md="6">{this.state.name}</Col>
              </Row>

              <Row className="p-t-10 p-b-10">
                <Col className="text-right" md="2">
                  Person Contact
                </Col>
                <Col md="6">
                  {this.state.contact.first_name}
                  &nbsp;
                  {this.state.contact.last_name}
                </Col>
              </Row>

              <hr className="hr-text" data-content="ข้อมูลซ่อมแซม" />

              {this.commentItemList()}

              <Row className="p-t-10 p-b-10">
                <Col md="3">
                  <Button
                    theme="secondary"
                    type="button"
                    onClick={() => this.props.history.goBack()}
                  >
                    Back
                  </Button>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

ViewModify.propTypes = {
  viewId: PropTypes.number
};

ViewModify.defaultProps = {
  viewId: 0
};

export default HocValidateUser(ViewModify);
