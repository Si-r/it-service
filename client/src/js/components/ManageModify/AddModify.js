import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import { FormLabel } from "@material-ui/core";
import HocValidateUser from "../../../HocValidateUser";

class AddModify extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",
      company_id: "",
      fact_contact_id: "",
      getCompayny: [],
      getPersonContact: [],
      isShowFactPersonContact: false,
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSelectChange(event) {
    this.setState(
      {
        [event.target.name]: event.target.value
      },
      () => {
        axios
          .get(`/api/personcontact/getPersonContactByCompany/${this.state.company_id}`)
          .then(res => {
            this.setState({
              isShowFactPersonContact: true,
              getPersonContact: res.data
            });
          })
          .catch(err => {
            console.log(err);
            this.setState({
              getPersonContact: []
            });
          });
      }
    );
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      name: this.state.name,
      fact_contact_id: this.state.fact_contact_id
    };

    axios
      .post("/api/modify/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          name: "",
          fact_contact_id: "",
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return "";
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get("/api/company/index")
      .then(res => {
        this.setState({
          getCompayny: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getCompayny: []
        });
      });
  }

  render() {
    const { getCompayny, getPersonContact } = this.state;
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="12" className="form-group">
                        <FormLabel htmlFor="name">Modify Title</FormLabel>
                        <FormInput
                          id="name"
                          name="name"
                          className={`form-control ${this.hasErrorFor("name") ? "is-invalid" : ""}`}
                          placeholder="หัวข้อการแก้ไข"
                          type="text"
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("name")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feInputState">Company Contact</FormLabel>
                        <FormSelect
                          id="feInputState"
                          name="company_id"
                          className={`form-control ${
                            this.hasErrorFor("company_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.company_id}
                          onChange={this.handleSelectChange}
                        >
                          <option value="">Choose...</option>

                          {getCompayny.map((compayny, idx) => (
                            <option key={idx.toString()} value={compayny.id}>
                              {compayny.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("company_id")}
                      </Col>
                    </Row>
                    {
                      this.state.isShowFactPersonContact && (
                        <Row form>
                          <Col md="6" className="form-group">
                            <FormLabel htmlFor="feInputState">Person Contact</FormLabel>
                            <FormSelect
                              id="feInputState"
                              name="fact_contact_id"
                              className={`form-control ${
                                this.hasErrorFor("fact_contact_id") ? "is-invalid" : ""
                              }`}
                              value={this.state.fact_contact_id}
                              onChange={this.handleSelectChange}
                            >
                              <option value="">Choose...</option>
                              {getPersonContact.map((personContact, idx) => (
                                personContact.fact_person_contact.map((factPersonContact, idx) => (
                                <option key={idx.toString()} value={factPersonContact.id}>
                                  {factPersonContact.first_name}
                                  &nbsp;
                                  {factPersonContact.last_name}
                                </option>
                                ))
                              ))}
                            </FormSelect>
                            {this.renderErrorFor("contact_id")}
                          </Col>
                        </Row>
                      )
                    }
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Create New Modify</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddModify);
