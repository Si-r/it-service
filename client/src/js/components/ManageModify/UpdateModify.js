/* eslint-disable react/no-danger */
import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import { FormLabel } from "@material-ui/core";
import FroalaEditor from "react-froala-wysiwyg";
import HocValidateUser from "../../../HocValidateUser";
import froalaConfig from "../../../data/froala-config";

class UpdateModify extends react.Component {
  constructor(props) {
    super(props);
    this.config = froalaConfig;
    this.state = {
      modify_id: this.props.match.params.id,
      client_id: sessionStorage.client_id,
      name: "",
      contact_id: "",
      comment: "",
      price: "",
      contact: [],
      getContact: [],
      getFactModify: [],
      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleModelChange = this.handleModelChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.handleCreateComment = this.handleCreateComment.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleModelChange(comment) {
    this.setState({
      comment
    });
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      name: this.state.name,
      contact_id: this.state.contact_id
    };

    axios
      .put(`/api/modify/update/${this.props.match.params.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  handleCreateComment(event) {
    event.preventDefault();
    const insertdata = {
      modify_id: this.state.modify_id,
      client_id: sessionStorage.client_id,
      comment: this.state.comment,
      price: this.state.price
    };

    axios
      .post("/api/modify/factStore", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          price: "",
          comment: "",
          errors: []
        });

        this.componentDidMount();
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get(`/api/modify/update/${this.props.match.params.id}`)
      .then(res => {
        this.setState({
          name: res.data.name,
          contact_id: res.data.contact_id,
          contact: res.data.contact,
          getFactModify: res.data.fact_modify
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  commentItemList() {
    const { getFactModify } = this.state;
    return (
      <div>
        <Row className="p-t-10 p-b-10">
          <Col md="9">
            Comment
          </Col>
          <Col md="3">
            Price
          </Col>
        </Row>
        { getFactModify.map((factModify) => (
          <Row className="p-t-5 p-b-5">
            <Col md="9">
              <div dangerouslySetInnerHTML={{ __html: factModify.comment}} />
            </Col>
            <Col md="3">
              <div dangerouslySetInnerHTML={{ __html: `${  factModify.price} บาท`}} />
            </Col>
          </Row>
        )) }
      </div>
    );
  }

  render() {
    return (
      <div style={{ paddingTop: "30px" }}>
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="12" className="form-group">
                        <FormLabel htmlFor="name">Modify Title</FormLabel>
                        <FormInput
                          id="name"
                          name="name"
                          className={`form-control ${this.hasErrorFor("name") ? "is-invalid" : ""}`}
                          placeholder="หัวข้อการแก้ไข"
                          type="text"
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("name")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel>Person Contact</FormLabel>
                        <p>
                          {this.state.contact.first_name}
                          &nbsp;
                          {this.state.contact.last_name}
                        </p>
                      </Col>
                    </Row>

                    <hr className="hr-text" data-content="ข้อมูลซ่อมแซม" />

                    {this.commentItemList()}

                    <hr className="hr-text" data-content="เพิ่มข้อมูลซ่อมแซม" />

                    <Row form>
                      <Col md="12" className="form-group">
                        <FormLabel>Modify Comment</FormLabel>
                        <FroalaEditor
                          id="comment"
                          className={`form-control ${
                            this.hasErrorFor("comment") ? "is-invalid" : ""
                          }`}
                          tag="textarea"
                          config={this.config}
                          model={this.state.comment}
                          onModelChange={this.handleModelChange}
                        />
                        {this.renderErrorFor("comment")}
                      </Col>
                    </Row>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="price">Modify Price</FormLabel>
                        <FormInput
                          id="price"
                          name="price"
                          className={`form-control ${
                            this.hasErrorFor("price") ? "is-invalid" : ""
                          }`}
                          placeholder="จำนวนค่าใช้จ่าย"
                          type="number"
                          value={this.state.price}
                          onInput={this.handleFieldChange}
                        />
                        {this.renderErrorFor("price")}
                      </Col>
                    </Row>

                    <Row className="p-t-10 p-b-10">
                      <Col md="3">
                        <Button
                          theme="secondary"
                          type="button"
                          onClick={() => this.props.history.goBack()}
                        >
                          Back
                        </Button>
                        &nbsp;
                        <Button type="button" onClick={this.handleCreateComment}>
                          Insert Comment Modify
                        </Button>
                        &nbsp;
                        <Button type="submit">Update New Modify</Button>
                      </Col>
                    </Row>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(UpdateModify);
