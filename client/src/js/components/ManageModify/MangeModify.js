/* eslint-disable no-unused-vars */
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManageModify() {
  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Title',
      field: 'name',
      sort: 'asc',
    },
    {
      label: 'Person Contact',
      field: 'full_name',
      sort: 'asc',
      col: '30%'
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
      col: '20%'
    }
  ];


  return (
    <div>
      <DataTable
        url="/api/modify"
        columns={columns}
        name="modify"
        headname=" List Modify - ข้อมูลการแก้ไข "
        headTablename="ตารางแสดงข้อมูลการแก้ไข"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageModify/AddModify"
        addbutton="Add Modify"
        manage="ManageModify"
        updateurl="/ManageModify/UpdateModify"
        viewurl="/ManageModify/ViewModify"
      />
    </div>
  );
}

export default HocValidateUser(ManageModify);
