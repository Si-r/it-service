/* eslint-disable import/order */
/* eslint-disable no-unused-vars */
import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManageSettingNews() {
  // const columns = ["id", "name", "is_close", "Action"];

  const columns = [
    {
      label: "ID",
      field: "id",
      sort: "asc"
    },
    {
      label: "Name",
      field: "name",
      sort: "asc"
    },
    {
      label: "status",
      field: "is_close",
      sort: "asc",
      col: "10%"
    },
    {
      label: "Action",
      field: "Action",
      sort: "asc",
      col: "20%"
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/settingnews"
        columns={columns}
        name="settingnews"
        headname=" List Setting News - ข้อมูลตั้งค่าข่าว"
        headTablename="ตารางแสดงข้อมูลตั้งค่าข่าว"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageSettingNews/AddSettingNews"
        addbutton="Add SettingNews"
        manage="ManageSettingNews"
        updateurl="/ManageSettingNews/UpdateSettingNews"
        viewurl="/ManageSettingNews/ViewSettingNews"
      />
    </div>
  );
}

export default HocValidateUser(ManageSettingNews);
