import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManagePersonResponsible() {
  // const columns = ["id", "first_name", "position", "email", "Action"];
  const columns = [
    {
      label: "ID",
      field: "id",
      sort: "asc"
    },
    {
      label: "Name",
      field: "full_name",
      sort: "asc"
    },
    {
      label: "Position",
      field: "position",
      sort: "asc"
    },
    {
      label: "Department",
      field: "department",
      subField: "name",
      sort: "name"
    },
    {
      label: "Company",
      field: "company",
      subField: "name",
      sort: "asc"
    },
    {
      label: "Action",
      field: "Action",
      sort: "asc",
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/personresponsible"
        columns={columns}
        name="personresponsible"
        headname=" List Person Responsible - ข้อมูลข้อมูลผู้รับผิดชอบ "
        headTablename="ตารางแสดงข้อมูลการผู้รับผิดชอบ"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManagePersonResponsible/AddPersonResponsible"
        addbutton="Add PersonResponsible"
        manage="ManagePersonResponsible"
        updateurl="/ManagePersonResponsible/UpdatePersonResponsible"
        viewurl="/ManagePersonResponsible/ViewPersonResponsible"
      />
    </div>
  );
}

export default HocValidateUser(ManagePersonResponsible);
