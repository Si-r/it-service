import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Container,
  Button
} from "shards-react";
import { FormLabel } from "@material-ui/core";

import Swal from "sweetalert2";
import axios from "axios";
import HocValidateUser from "../../../HocValidateUser";
import PageTitle from "../../../components/common/PageTitle";

class ViewPersonResponsible extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      first_name: "",
      last_name: "",
      nick_name: "",
      telephone: "",
      email: "",
      position: "",
      id_card: "",
      id_employee: "",
      location_id: "",
      company_id: "",
      department_id: "",
      getlocation: [],
      getcompany: [],
      getdepartment: [],

      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      nick_name: this.state.nick_name,
      telephone: this.state.telephone,
      email: this.state.email,
      position: this.state.position,
      card: this.state.id_card,
      employee: this.state.id_employee,
      location_id: this.state.location_id,
      company_id: this.state.company_id,
      department_id: this.state.department_id
    };

    axios
      .put(`/api/personresponsible/update/${this.props.match.params.id}`, insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }
  }

  componentDidMount() {
    axios
      .get("/api/location/index")
      .then(res => {
        this.setState({
          getlocation: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getlocation: []
        });
      });

    axios
      .get("/api/company/index")
      .then(res => {
        this.setState({
          getcompany: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getcompany: []
        });
      });

    axios
      .get("/api/department/index")
      .then(res => {
        this.setState({
          getdepartment: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getdepartment: []
        });
      });

    axios
      .get(`/api/personresponsible/update/${this.props.match.params.id}`)
      .then(res => {
        this.setState({
          first_name: res.data.user.first_name,
          last_name: res.data.user.last_name,
          nick_name: res.data.user.nick_name,
          telephone: res.data.user.telephone,
          email: res.data.user.email,
          position: res.data.user.position,
          id_card: res.data.user.id_card,
          id_employee: res.data.user.id_employee,
          location_id: res.data.user.location_id,
          company_id: res.data.user.company_id,
          department_id: res.data.user.department_id
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { getlocation, getcompany, getdepartment } = this.state;
    return (
      <div>
        <Container fluid className="main-content-container px-4">
          <Row noGutters className="page-header py-4">
            <PageTitle
              sm="4"
              title={`Person Responsible - ข้อมูลผู้รับผิดชอบ ${this.props.match.params.id}`}
              className="text-sm-left"
            />
          </Row>
        </Container>
        <Container>
          <ListGroup flush style={{borderRadius:'10px'}}>
            <ListGroupItem className="p-3" style={{borderRadius:'10px'}}>
              <Row>
                <Col>
                  <Form>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feFirstName">First Name</FormLabel>
                        <FormInput
                          id="feFirstName"
                          name="first_name"
                          className={`form-control ${
                            this.hasErrorFor("first_name") ? "is-invalid" : ""
                          }`}
                          type="text"
                          value={this.state.first_name}
                          onChange={this.handleFieldChange}
                          disabled
                        />
                        {this.renderErrorFor("first_name")}
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feLastName">Last Name</FormLabel>
                        <FormInput
                          id="feLastName"
                          name="last_name"
                          className={`form-control ${
                            this.hasErrorFor("last_name") ? "is-invalid" : ""
                          }`}
                          type="text"
                          value={this.state.last_name}
                          onChange={this.handleFieldChange}
                          disabled
                        />
                        {this.renderErrorFor("last_name")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feNickName">Nick Name</FormLabel>
                        <FormInput
                          id="feNickName"
                          name="nick_name"
                          className={`form-control ${
                            this.hasErrorFor("nick_name") ? "is-invalid" : ""
                          }`}
                          type="text"
                          value={this.state.nick_name}
                          onChange={this.handleFieldChange}
                          disabled
                        />
                        {this.renderErrorFor("nick_name")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feTelephone">Telephone</FormLabel>
                        <FormInput
                          id="feTelephone"
                          name="telephone"
                          className={`form-control ${
                            this.hasErrorFor("telephone") ? "is-invalid" : ""
                          }`}
                          type="text"
                          value={this.state.telephone}
                          onChange={this.handleFieldChange}
                          disabled
                        />
                        {this.renderErrorFor("telephone")}
                      </Col>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feEmail">Email</FormLabel>
                        <FormInput
                          id="feEmail"
                          name="email"
                          className={`form-control ${
                            this.hasErrorFor("email") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อผู้ใช้"
                          type="text"
                          value={this.state.email}
                          onChange={this.handleFieldChange}
                          disabled
                        />
                        {this.renderErrorFor("email")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="fePosition">Position</FormLabel>
                        <FormInput
                          id="fePosition"
                          name="position"
                          className={`form-control ${
                            this.hasErrorFor("position") ? "is-invalid" : ""
                          }`}
                          type="text"
                          value={this.state.position}
                          onChange={this.handleFieldChange}
                          disabled
                        />
                        {this.renderErrorFor("position")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feLocation">Location</FormLabel>
                        <FormSelect
                          id="feLocation"
                          name="location_id"
                          className={`form-control ${
                            this.hasErrorFor("location_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.location_id}
                          onChange={this.handleFieldChange}
                          disabled
                        >
                          <option value="">Choose...</option>

                          {getlocation.map((location, idx) => (
                            <option key={idx} value={location.id}>
                              {location.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("location_id")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feCompany">Company</FormLabel>
                        <FormSelect
                          id="feCompany"
                          name="company_id"
                          className={`form-control ${
                            this.hasErrorFor("company_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.company_id}
                          onChange={this.handleFieldChange}
                          disabled
                        >
                          <option value="">Choose...</option>

                          {getcompany.map((company, idx) => (
                            <option key={idx} value={company.id}>
                              {company.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("company_id")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feDepartment">Department</FormLabel>
                        <FormSelect
                          id="feDepartment"
                          name="department_id"
                          className={`form-control ${
                            this.hasErrorFor("department_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.department_id}
                          onChange={this.handleFieldChange}
                          disabled
                        >
                          <option value="">Choose...</option>

                          {getdepartment.map((getdepartment, idx) => (
                            <option key={idx} value={getdepartment.id}>
                              {getdepartment.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("department_id")}
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(ViewPersonResponsible);
