import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";

import { FormLabel, Icon } from "@material-ui/core";
import Swal from "sweetalert2";
import axios from "axios";
import clsx from "clsx";
import Modal from "react-bootstrap/Modal";
import AddLocation from "../ManageLocation/AddLocation";
import AddCompany from "../ManageCompany/AddCompany";
import AddDepartment from "../ManageDepartment/AddDepartment";
import PageTitle from "../../../components/common/PageTitle";
import HocValidateUser from "../../../HocValidateUser";

class AddPersonResponsible extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      first_name: "",
      last_name: "",
      nick_name: "",
      telephone: "",
      email: "",
      position: "",
      id_card: "",
      id_employee: "",
      location_id: "",
      company_id: "",
      department_id: "",
      getlocation: [],
      getcompany: [],
      getdepartment: [],

      isModalLocation: false,
      isModelCompany: false,
      isModelDepartment: false,

      errors: []
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);

    this.setModalShow = this.setModalShow.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      nick_name: this.state.nick_name,
      telephone: this.state.telephone,
      email: this.state.email,
      position: this.state.position,
      card: this.state.id_card,
      employee: this.state.id_employee,
      location_id: this.state.location_id,
      company_id: this.state.company_id,
      department_id: this.state.department_id,
      id_card: this.state.id_card,
      id_employee: this.state.id_employee
    };
    console.log(insertdata);

    axios
      .post("/api/personresponsible/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          first_name: "",
          last_name: "",
          nick_name: "",
          telephone: "",
          email: "",
          position: "",
          card: "",
          employee: "",
          location_id: "",
          company_id: "",
          department_id: "",
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return ("");
  }

  componentDidMount() {
    axios
      .get("/api/location/index")
      .then(res => {
        this.setState({
          getlocation: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getlocation: []
        });
      });

    axios
      .get("/api/company/index")
      .then(res => {
        this.setState({
          getcompany: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getcompany: []
        });
      });

    axios
      .get("/api/department/index")
      .then(res => {
        this.setState({
          getdepartment: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getdepartment: []
        });
      });
  }

  setModalShow(isOpen, isItem) {
    if (isOpen === false) {
      this.componentDidMount();
    }
    if(isItem === "Location") {
      this.setState({
        isModalLocation: isOpen
      });
    }else if(isItem === "Company") {
      this.setState({
        isModelCompany: isOpen
      });
    }else{
      this.setState({
        isModelDepartment: isOpen
      });
    }
  }

  modalAddLocation() {
    return (
      <Modal
        show={this.state.isModalLocation}
        onHide={() => this.setModalShow(false, 'Location')}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Location (เพิ่มข้อมูลตำเหน่ง)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddLocation />
        </Modal.Body>
      </Modal>
    );
  }

  modalAddCompany() {
    return (
      <Modal
        show={this.state.isModelCompany}
        onHide={() => this.setModalShow(false, 'Company')}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Company (เพิ่มข้อมูลบริษัท)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddCompany />
        </Modal.Body>
      </Modal>
    );
  }

  modalAddDepartment() {
    return (
      <Modal
        show={this.state.isModelDepartment}
        onHide={() => this.setModalShow(false, 'Department')}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Department (เพิ่มข้อแผนก)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddDepartment />
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    const { getlocation, getcompany, getdepartment } = this.state;
    return (
      <div>
        {this.modalAddDepartment()}
        {this.modalAddLocation()}
        {this.modalAddCompany()}
        <Container>
          <Row className="page-header py-4">
            <PageTitle
              title='Add Person Responsible (เพิ่มข้อมูลผู้รับผิดชอบ)'
              className="text-sm-left"
            />
          </Row>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feFirstName">First Name (ชื่อ)</FormLabel>
                        <FormInput
                          id="feFirstName"
                          name="first_name"
                          className={`form-control ${
                            this.hasErrorFor("first_name") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อ"
                          type="text"
                          value={this.state.first_name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("first_name")}
                      </Col>

                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feLastName">Last Name (นามสกุล)</FormLabel>
                        <FormInput
                          id="feLastName"
                          name="last_name"
                          className={`form-control ${
                            this.hasErrorFor("last_name") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกนามสกุล"
                          type="text"
                          value={this.state.last_name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("last_name")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feNickName">NickName (ชื่อเล่น)</FormLabel>
                        <FormInput
                          id="feNickName"
                          name="nick_name"
                          className={`form-control ${
                            this.hasErrorFor("nick_name") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อเล่น"
                          type="text"
                          value={this.state.nick_name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("nick_name")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feTelephone">Telephone (เบอร์โทร)</FormLabel>
                        <FormInput
                          id="feTelephone"
                          name="telephone"
                          className={`form-control ${
                            this.hasErrorFor("telephone") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกเบอร์โทร"
                          type="text"
                          value={this.state.telephone}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("telephone")}
                      </Col>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feEmail">Email (อีเมลล์)</FormLabel>
                        <FormInput
                          id="feEmail"
                          name="email"
                          className={`form-control ${
                            this.hasErrorFor("email") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกอีเมลล์"
                          type="text"
                          value={this.state.email}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("email")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feIdentificationNo">
                          Identification No  (เลขบัตรประชาชน)
                        </FormLabel>
                        <FormInput
                          id="feIdentificationNo"
                          name="id_card"
                          className={`form-control ${
                            this.hasErrorFor("telephone") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกเลขบัตรประชาชน"
                          type="text"
                          value={this.state.id_card}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("telephone")}
                      </Col>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feEmployee">Employee No (เลขประจำตัวพนักงาน)</FormLabel>
                        <FormInput
                          id="feEmployee"
                          name="id_employee"
                          className={`form-control ${
                            this.hasErrorFor("email") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกเลขประจำตัวพนักงาน"
                          type="text"
                          value={this.state.id_employee}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("email")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="fePosition">Position (ตำเหน่งงาน)</FormLabel>
                        <FormInput
                          id="fePosition"
                          name="position"
                          className={`form-control ${
                            this.hasErrorFor("position") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกตำเหน่งงาน"
                          type="text"
                          value={this.state.position}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("position")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feLocation">Location (ตำเหน่งที่ตั้ง)</FormLabel>
                        <Button
                          theme="link"
                          className="float-right p-b-0 iconAddOption"
                          size="sm"
                          type="button"
                          onClick={() => this.setModalShow(true, 'Location')}
                        >
                          <Icon
                            className={clsx("iconHover", "fa fa-plus-circle")}
                            color="error"
                            style={{ fontSize: 24 }}
                          />
                        </Button>
                        <FormSelect
                          id="feLocation"
                          name="location_id"
                          className={`form-control ${
                            this.hasErrorFor("location_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.location_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getlocation.map((location, idx) => (
                            <option key={idx.toString()} value={location.id}>
                              {location.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("location_id")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feCompany">Company (บริษัท)</FormLabel>
                        <Button
                          theme="link"
                          className="float-right p-b-0 iconAddOption"
                          size="sm"
                          type="button"
                          onClick={() => this.setModalShow(true, 'Company')}
                        >
                          <Icon
                            className={clsx("iconHover", "fa fa-plus-circle")}
                            color="error"
                            style={{ fontSize: 24 }}
                          />
                        </Button>
                        <FormSelect
                          id="feCompany"
                          name="company_id"
                          className={`form-control ${
                            this.hasErrorFor("company_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.company_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">บริษัท...</option>

                          {getcompany.map((company, idx) => (
                            <option key={idx.toString()} value={company.id}>
                              {company.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("company_id")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feDepartment">Department (แผนก)</FormLabel>
                        <Button
                          theme="link"
                          className="float-right p-b-0 iconAddOption"
                          size="sm"
                          type="button"
                          onClick={() => this.setModalShow(true, 'Department')}
                        >
                          <Icon
                            className={clsx("iconHover", "fa fa-plus-circle")}
                            color="error"
                            style={{ fontSize: 24 }}
                          />
                        </Button>
                        <FormSelect
                          id="department_id"
                          name="department_id"
                          className={`form-control ${
                            this.hasErrorFor("department_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.department_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">เลือกแผนก...</option>

                          {getdepartment.map((department, idx) => (
                            <option key={idx.toString()} value={department.id}>
                              {department.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("department_id")}
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Create New Person Responsible</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddPersonResponsible);
