import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormCheckbox,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import HocValidateUser from "../../../HocValidateUser";

class ViewPermission extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      permission_name: "",
      ManageUser: false,
      ManageUserView: false,
      ManageUserEdit: false,
      ManageUserDelete: false,
      ManagePermission: false,
      ManagePermissionView: false,
      ManagePermissionEdit: false,
      ManagePermissionDelete: false,
      ManageAddress: false,
      ManageAddressView: false,
      ManageAddressEdit: false,
      ManageAddressDelete: false,
      ManageCompany: false,
      ManageCompanyView: false,
      ManageCompanyEdit: false,
      ManageCompanyDelete: false,
      ManageDepartment: false,
      ManageDepartmentView: false,
      ManageDepartmentEdit: false,
      ManageDepartmentDelete: false,
      ManageDistrict: false,
      ManageDistrictView: false,
      ManageDistrictEdit: false,
      ManageDistrictDelete: false,
      ManageEquipment: false,
      ManageEquipmentView: false,
      ManageEquipmentEdit: false,
      ManageEquipmentDelete: false,
      ManageImage: false,
      ManageImageView: false,
      ManageImageEdit: false,
      ManageImageDelete: false,
      ManageImpact: false,
      ManageImpactView: false,
      ManageImpactEdit: false,
      ManageImpactDelete: false,
      ManageLocation: false,
      ManageLocationView: false,
      ManageLocationEdit: false,
      ManageLocationDelete: false,
      ManageMessage: false,
      ManageMessageView: false,
      ManageMessageEdit: false,
      ManageMessageDelete: false,
      ManageModify: false,
      ManageModifyView: false,
      ManageModifyEdit: false,
      ManageModifyDelete: false,
      ManageNews: false,
      ManageNewsView: false,
      ManageNewsEdit: false,
      ManageNewsDelete: false,
      ManagePersonContact: false,
      ManagePersonContactView: false,
      ManagePersonContactEdit: false,
      ManagePersonContactDelete: false,
      ManagePersonResponsible: false,
      ManagePersonResponsibleView: false,
      ManagePersonResponsibleEdit: false,
      ManagePersonResponsibleDelete: false,
      ManagePostalCode: false,
      ManagePostalCodeView: false,
      ManagePostalCodeEdit: false,
      ManagePostalCodeDelete: false,
      ManagePriority: false,
      ManagePriorityView: false,
      ManagePriorityEdit: false,
      ManagePriorityDelete: false,
      ManageProvince: false,
      ManageProvinceView: false,
      ManageProvinceEdit: false,
      ManageProvinceDelete: false,
      ManageRequestGeneral: false,
      ManageRequestGeneralView: false,
      ManageRequestGeneralEdit: false,
      ManageRequestGeneralDelete: false,
      ManageRequestIssuses: false,
      ManageRequestIssusesView: false,
      ManageRequestIssusesEdit: false,
      ManageRequestIssusesDelete: false,
      ManageSettingNews: false,
      ManageSettingNewsView: false,
      ManageSettingNewsEdit: false,
      ManageSettingNewsDelete: false,
      Report: false,
      errors: []
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      permission_name: event.target.value
    });
  }

  handleChange(fruit) {
    const newState = {};
    newState[fruit] = !this.state[fruit];
    this.setState({
      // eslint-disable-next-line react/no-access-state-in-setstate
      ...this.state,
      ...newState
    });
  }

  handleCreate(event) {
    event.preventDefault();


    const listRole = this.state
    var insertdata = {
      permission_name: this.state.permission_name,

    };
    let arrrole = []


    Object.keys(listRole).map((role,index) => {
      if(listRole[role] === true){
        arrrole.push(role);

      }
      return arrrole
    })

      // console.log(updaterole);

      axios
        .put(`/api/permission/update/${this.props.match.params.id}`, insertdata)
        .then(response => {
          console.log(response.data);
          Swal.fire("Successfully", "Update data successfully ", "success");

        })
        .catch(error => {
          this.setState({
            errors: error.response.data.errors
          });
          console.log(error.response.data.errors);

          Swal.fire("Errors", "check the value of a form field", "error");
        });


        var updaterole = {
          permission_id: this.props.match.params.id,
          role: arrrole

        };



    axios
      .put(`/api/rolepermission/update/${this.props.match.params.id}`, updaterole)
      .then(response => {
        console.log(response.data);

      })
      .catch(error => {
        console.log(error);
      });




  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong> {this.state.errors[field][0]} </strong>
        </span>
      );
    }
  }

  componentDidMount(){
    axios
      .get(`/api/rolepermission/getRolePermission/${this.props.match.params.id}`)
      .then(res => {

        console.log(res.data[0].role);

        const listRole = res.data
        let arrRole = []

        listRole.map((role) => {
          arrRole.push(role.role);
          return "";
        })

        for(var i = 0; i < arrRole.length;i++){
          const manage = arrRole[i]
          const setstate = {
           [`${manage}`]: true
          }
          this.setState(setstate)
        }
      })
      .catch(err => {
        console.log(err);
      });

      axios.get(`/api/permission/update/${this.props.match.params.id}`).then(res => {
        this.setState({
          permission_name: res.data.user.permission_name,
        })
      }).catch(err =>{
        console.log(err);

      })

  }

  render() {
    return (
      <div
        style={{
          paddingTop: "30px"
        }}
      >
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="12" className="form-group">
                        <label htmlFor="feEmailAddress"> Permission Name </label>

                        <FormInput
                          name="permission_name"
                          className={`form-control ${
                            this.hasErrorFor("permission_name") ? "is-invalid" : ""
                          }`}
                          placeholder="กรอกชื่อผู้ใช้"
                          type="text"
                          value={this.state.permission_name}
                          onChange={this.handleFieldChange}
                          disable
                        />

                        {this.renderErrorFor("permission_name")}
                      </Col>

                      <Col>
                        <ListGroupItem className="px-3 pb-3">
                          <FormCheckbox
                            checked={this.state.ManageUser}
                            onChange={() => this.handleChange("ManageUser")}
                          >
                            User
                          </FormCheckbox>

                          {this.state.ManageUser === true ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageUser}
                                  onChange={() => this.handleChange("ManageUser")}
                                  className="mystyle"
                                >
                                  ManageUser
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageUserView}
                                  onChange={() => this.handleChange("ManageUserView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageUserEdit}
                                  onChange={() => this.handleChange("ManageUserEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageUserDelete}
                                  onChange={() => this.handleChange("ManageUserDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePermission}
                                  onChange={() => this.handleChange("ManagePermission")}
                                  className="mystyle"
                                >
                                  ManagePermission
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePermissionView}
                                  onChange={() => this.handleChange("ManagePermissionView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePermissionEdit}
                                  onChange={() => this.handleChange("ManagePermissionEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePermissionDelete}
                                  onChange={() => this.handleChange("ManagePermissionDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}
                          <FormCheckbox
                            checked={this.state.ManagePersonContact}
                            onChange={() => this.handleChange("ManagePersonContact")}
                          >
                            Person
                          </FormCheckbox>

                          {this.state.ManagePersonContact === true ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePersonContact}
                                  onChange={() => this.handleChange("ManagePersonContact")}
                                  className="mystyle"
                                >
                                  ManageUser
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePersonContactView}
                                  onChange={() => this.handleChange("ManagePersonContactView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePersonContactEdit}
                                  onChange={() => this.handleChange("ManagePersonContactEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePersonContactDelete}
                                  onChange={() => this.handleChange("ManagePersonContactDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePersonResponsible}
                                  onChange={() => this.handleChange("ManagePersonResponsible")}
                                  className="mystyle2"
                                >
                                  ManagePersonResponsible
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePersonResponsibleView}
                                  onChange={() => this.handleChange("ManagePersonResponsibleView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePersonResponsibleEdit}
                                  onChange={() => this.handleChange("ManagePersonResponsibleEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePersonResponsibleDelete}
                                  onChange={() =>
                                    this.handleChange("ManagePersonResponsibleDelete")
                                  }
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}
                          <FormCheckbox
                            checked={this.state.ManageAddress}
                            onChange={() => this.handleChange("ManageAddress")}
                          >
                            Address
                          </FormCheckbox>
                          {this.state.ManageAddress === true ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageAddress}
                                  onChange={() => this.handleChange("ManageAddress")}
                                  className="mystyle"
                                >
                                  ManageAddress
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageAddressView}
                                  onChange={() => this.handleChange("ManageAddressView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageAddressEdit}
                                  onChange={() => this.handleChange("ManageAddressEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageAddressDelete}
                                  onChange={() => this.handleChange("ManageAddressDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageCompany}
                                  onChange={() => this.handleChange("ManageCompany")}
                                  className="mystyle"
                                >
                                  ManageCompany
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageCompanyView}
                                  onChange={() => this.handleChange("ManageCompanyView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageCompanyEdit}
                                  onChange={() => this.handleChange("ManageCompanyEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageCompanyDelete}
                                  onChange={() => this.handleChange("ManageCompanyDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>

                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageDepartment}
                                  onChange={() => this.handleChange("ManageDepartment")}
                                  className="mystyle"
                                >
                                  ManageDepartment
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageDepartmentView}
                                  onChange={() => this.handleChange("ManageDepartmentView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageDepartmentEdit}
                                  onChange={() => this.handleChange("ManageDepartmentEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageDepartmentDelete}
                                  onChange={() => this.handleChange("ManageDepartmentDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageProvince}
                                  onChange={() => this.handleChange("ManageProvince")}
                                  className="mystyle"
                                >
                                  ManageProvince
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageProvinceView}
                                  onChange={() => this.handleChange("ManageProvinceView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageProvinceEdit}
                                  onChange={() => this.handleChange("ManageProvinceEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageProvinceDelete}
                                  onChange={() => this.handleChange("ManageProvinceDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageDistrict}
                                  onChange={() => this.handleChange("ManageDistrict")}
                                  className="mystyle"
                                >
                                  ManageDistrict
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageDistrictView}
                                  onChange={() => this.handleChange("ManageDistrictView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageDistrictEdit}
                                  onChange={() => this.handleChange("ManageDistrictEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageDistrictDelete}
                                  onChange={() => this.handleChange("ManageDistrictDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePostalCode}
                                  onChange={() => this.handleChange("ManagePostalCode")}
                                  className="mystyle"
                                >
                                  ManagePostalCode
                                </FormCheckbox>


                              </div>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageLocation}
                                  onChange={() => this.handleChange("ManageLocation")}
                                  className="mystyle"
                                >
                                  ManageLocation
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageLocationView}
                                  onChange={() => this.handleChange("ManageLocationView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageLocationEdit}
                                  onChange={() => this.handleChange("ManageLocationEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageLocationDelete}
                                  onChange={() => this.handleChange("ManageLocationDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}
                          <FormCheckbox
                            checked={this.state.ManageEquipment}
                            onChange={() => this.handleChange("ManageEquipment")}
                          >
                            Equipment
                          </FormCheckbox>

                          {this.state.ManageEquipment ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageEquipment}
                                  onChange={() => this.handleChange("ManageEquipment")}
                                  className="mystyle"
                                >
                                  ManageEquipment
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageEquipmentView}
                                  onChange={() => this.handleChange("ManageEquipmentView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageEquipmentEdit}
                                  onChange={() => this.handleChange("ManageEquipmentEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageEquipmentDelete}
                                  onChange={() => this.handleChange("ManageEquipmentDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}
                          <FormCheckbox
                            checked={this.state.ManageRequestGeneral}
                            onChange={() => this.handleChange("ManageRequestGeneral")}
                          >
                            Request
                          </FormCheckbox>

                          {this.state.ManageRequestGeneral ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageRequestGeneral}
                                  onChange={() => this.handleChange("ManageRequestGeneral")}
                                  className="mystyle"
                                >
                                  ManageRequestGeneral
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageRequestGeneralView}
                                  onChange={() => this.handleChange("ManageRequestGeneralView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageRequestGeneralEdit}
                                  onChange={() => this.handleChange("ManageRequestGeneralEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageRequestGeneralDelete}
                                  onChange={() => this.handleChange("ManageRequestGeneralDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageRequestIssuses}
                                  onChange={() => this.handleChange("ManageRequestIssuses")}
                                  className="mystyle"
                                >
                                  ManageRequestIssuses
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageRequestIssusesView}
                                  onChange={() => this.handleChange("ManageRequestIssusesView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageRequestIssusesEdit}
                                  onChange={() => this.handleChange("ManageRequestIssusesEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageRequestIssusesDelete}
                                  onChange={() => this.handleChange("ManageRequestIssusesDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}

                          <FormCheckbox
                            checked={this.state.ManageNews}
                            onChange={() => this.handleChange("ManageNews")}
                          >
                            News
                          </FormCheckbox>
                          {this.state.ManageNews ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageNews}
                                  onChange={() => this.handleChange("ManageNews")}
                                  className="mystyle"
                                >
                                  ManageNews
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageNewsView}
                                  onChange={() => this.handleChange("ManageNewsView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageNewsEdit}
                                  onChange={() => this.handleChange("ManageNewsEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageNewsDelete}
                                  onChange={() => this.handleChange("ManageNewsDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageSettingNews}
                                  onChange={() => this.handleChange("ManageSettingNews")}
                                  className="mystyle"
                                >
                                  ManageSettingNews
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageSettingNewsView}
                                  onChange={() => this.handleChange("ManageSettingNewsView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageSettingNewsEdit}
                                  onChange={() => this.handleChange("ManageSettingNewsEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageSettingNewsDelete}
                                  onChange={() => this.handleChange("ManageSettingNewsDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}
                          {/* <FormCheckbox
                            checked={this.state.ManageImage}
                            onChange={() => this.handleChange("ManageImage")}
                          >
                            Image
                          </FormCheckbox>
                          {this.state.ManageImage === true ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageImage}
                                  onChange={() => this.handleChange("ManageImage")}
                                  className="mystyle"
                                >
                                  ManageImage
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageImageView}
                                  onChange={() => this.handleChange("ManageImageView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageImageEdit}
                                  onChange={() => this.handleChange("ManageImageEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageImageDelete}
                                  onChange={() => this.handleChange("ManageImageDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null} */}

                          <FormCheckbox
                            checked={this.state.ManageMessage}
                            onChange={() => this.handleChange("ManageMessage")}
                          >
                            Message
                          </FormCheckbox>
                          {this.state.ManageMessage === true ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageMessage}
                                  onChange={() => this.handleChange("ManageMessage")}
                                  className="mystyle"
                                >
                                  ManageMessage
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageMessageView}
                                  onChange={() => this.handleChange("ManageMessageView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageMessageEdit}
                                  onChange={() => this.handleChange("ManageMessageEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageMessageDelete}
                                  onChange={() => this.handleChange("ManageMessageDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}
                          <FormCheckbox
                            checked={this.state.ManageModify}
                            onChange={() => this.handleChange("ManageModify")}
                          >
                            Modify
                          </FormCheckbox>
                          {this.state.ManageModify === true ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageModify}
                                  onChange={() => this.handleChange("ManageModify")}
                                  className="mystyle"
                                >
                                  ManageModify
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageModifyView}
                                  onChange={() => this.handleChange("ManageModifyView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageModifyEdit}
                                  onChange={() => this.handleChange("ManageModifyEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageModifyDelete}
                                  onChange={() => this.handleChange("ManageModifyDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}
                          <FormCheckbox
                            checked={this.state.ManagePriority}
                            onChange={() => this.handleChange("ManagePriority")}
                          >
                            Priority
                          </FormCheckbox>
                          {this.state.ManagePriority === true ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePriority}
                                  onChange={() => this.handleChange("ManagePriority")}
                                  className="mystyle"
                                >
                                  ManagePriority
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePriorityView}
                                  onChange={() => this.handleChange("ManagePriorityView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePriorityEdit}
                                  onChange={() => this.handleChange("ManagePriorityEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManagePriorityDelete}
                                  onChange={() => this.handleChange("ManagePriorityDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}
                          <FormCheckbox
                            checked={this.state.ManageImpact}
                            onChange={() => this.handleChange("ManageImpact")}
                          >
                            Impact
                          </FormCheckbox>
                          {this.state.ManageImpact === true ? (
                            <Col>
                              <div>
                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageImpact}
                                  onChange={() => this.handleChange("ManageImpact")}
                                  className="mystyle"
                                >
                                  ManageMessage
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageImpactView}
                                  onChange={() => this.handleChange("ManageImpactView")}
                                >
                                  View
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageImpactEdit}
                                  onChange={() => this.handleChange("ManageImpactEdit")}
                                >
                                  Edit
                                </FormCheckbox>

                                <FormCheckbox
                                  inline
                                  checked={this.state.ManageImpactDelete}
                                  onChange={() => this.handleChange("ManageImpactDelete")}
                                >
                                  Delete
                                </FormCheckbox>
                              </div>
                            </Col>
                          ) : null}
                          <FormCheckbox
                            checked={this.state.Report}
                            onChange={() => this.handleChange("Report")}
                          >
                            Report
                          </FormCheckbox>
                        </ListGroupItem>
                      </Col>
                    </Row>

                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(ViewPermission);
