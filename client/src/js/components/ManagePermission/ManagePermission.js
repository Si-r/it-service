import React from "react";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

function ManagePermission() {
  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Permission',
      field: 'permission_name',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
      col: '20%'
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/permission"
        columns={columns}
        name="permission"
        headname=" List Permission - ข้อมูลสิทธิ์ผู้ใช้งาน "
        headTablename="ตารางแสดงข้อมูลสิทธิ์ผู้ใช้งาน"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManagePermission/AddPermission"
        addbutton="Add Permission"
        manage="ManagePermission"
        updateurl="/ManagePermission/UpdatePermission"
        viewurl="/ManagePermission/ViewPermission"
      />
    </div>
  );
}

export default HocValidateUser(ManagePermission);
