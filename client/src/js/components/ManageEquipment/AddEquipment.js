import React, * as react from "react";
import {
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
  FormSelect,
  Button,
  Container
} from "shards-react";
import Swal from "sweetalert2";
import axios from "axios";
import clsx from "clsx";
import { FormLabel, Icon } from "@material-ui/core";
import Modal from "react-bootstrap/Modal";
import HocValidateUser from "../../../HocValidateUser";
import AddLocation from "../ManageLocation/AddLocation";

class AddEquipment extends react.Component {
  constructor(props) {
    super(props);
    this.state = {
      client_id: sessionStorage.client_id,
      name: "",
      location_id: "",
      getlocation: [],
      errors: [],
      isModalLocation: false
    };

    this.handleFieldChange = this.handleFieldChange.bind(this);
    this.setModalShow = this.setModalShow.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.hasErrorFor = this.hasErrorFor.bind(this);
    this.renderErrorFor = this.renderErrorFor.bind(this);
  }

  handleFieldChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleCreate(event) {
    event.preventDefault();

    const insertdata = {
      client_id: sessionStorage.client_id,
      name: this.state.name,
      location_id: this.state.location_id
    };
    console.log(insertdata);

    axios
      .post("/api/equipment/store", insertdata)
      .then(() => {
        Swal.fire("Successfully", "Add data successfully ", "success");

        this.setState({
          name: "",
          location_id: "",
          errors: []
        });
      })
      .catch(error => {
        this.setState({
          errors: error.response.data.errors
        });
        console.log(error.response.data.errors);

        Swal.fire("Errors", "check the value of a form field", "error");
      });
  }

  hasErrorFor(field) {
    try {
      return !!this.state.errors[field];
    } catch (error) {
      return ""
    }
  }

  renderErrorFor(field) {
    if (this.hasErrorFor(field)) {
      return (
        <span className="invalid-feedback">
          <strong>{this.state.errors[field][0]}</strong>
        </span>
      );
    }

    return "";
  }

  componentDidMount() {
    axios
      .get("/api/location/index")
      .then(res => {
        this.setState({
          getlocation: res.data
        });
      })
      .catch(err => {
        console.log(err);
        this.setState({
          getlocation: []
        });
      });
  }

  setModalShow(isOpen) {
    if (isOpen === false) {
      this.componentDidMount();
    }

    this.setState({
      isModalLocation: isOpen
    });
  }

  modalAddLocation() {
    return (
      <Modal
        show={this.state.isModalLocation}
        onHide={() => this.setModalShow(false, 'Location')}
        size="lg"
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Add Location (เพิ่มข้อมูลตำเหน่ง)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <AddLocation />
        </Modal.Body>
      </Modal>
    );
  }

  render() {
    const { getlocation } = this.state;
    return (
      <div style={{ paddingTop: "30px" }}>
        {this.modalAddLocation()}
        <Container>
          <ListGroup flush>
            <ListGroupItem className="p-3">
              <Row>
                <Col>
                  <Form onSubmit={this.handleCreate}>
                    <Row form>
                      <Col md="12" className="form-group">
                        <FormLabel htmlFor="feNameEquipment">Equipment Name</FormLabel>
                        <FormInput
                          id="feNameEquipment"
                          name="name"
                          className={`form-control ${this.hasErrorFor("name") ? "is-invalid" : ""}`}
                          placeholder="กรอกชื่ออุปกรณ์"
                          type="text"
                          value={this.state.name}
                          onChange={this.handleFieldChange}
                        />
                        {this.renderErrorFor("name")}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md="6" className="form-group">
                        <FormLabel htmlFor="feLocation">Location</FormLabel>
                        <Button
                          theme="link"
                          className="float-right p-b-0 iconAddOption"
                          size="sm"
                          type="button"
                          onClick={() => this.setModalShow(true)}
                        >
                          <Icon
                            className={clsx("iconHover", "fa fa-plus-circle")}
                            color="error"
                            style={{ fontSize: 24 }}
                          />
                        </Button>
                        <FormSelect
                          id="feLocation"
                          name="location_id"
                          className={`form-control ${
                            this.hasErrorFor("location_id") ? "is-invalid" : ""
                          }`}
                          value={this.state.location_id}
                          onChange={this.handleFieldChange}
                        >
                          <option value="">Choose...</option>

                          {getlocation.map((location, idx) => (
                            <option key={idx.toString()} value={location.id}>
                              {location.name}
                            </option>
                          ))}
                        </FormSelect>
                        {this.renderErrorFor("location_id")}
                      </Col>
                    </Row>
                    <Button
                      theme="secondary"
                      type="button"
                      onClick={() => this.props.history.goBack()}
                    >
                      Back
                    </Button>
                    &nbsp;
                    <Button type="submit">Create New Equipment</Button>
                  </Form>
                </Col>
              </Row>
            </ListGroupItem>
          </ListGroup>
        </Container>
      </div>
    );
  }
}

export default HocValidateUser(AddEquipment);
