/* eslint-disable no-unused-vars */
/* eslint-disable import/no-extraneous-dependencies */
import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

const drawerWidth = 240;

function ManageEquipment() {
  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Name',
      field: 'name',
      sort: 'asc',
    },
    {
      label: 'Location',
      field: 'location',
      subField: 'name',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
      col: '20%'
    }
  ];

  return (
    <div>
      <DataTable
        url="/api/equipment"
        columns={columns}
        name="equipment"
        headname=" List Equipment - ข้อมูลอุปกรณ์ "
        headTablename="ตารางแสดงข้อมูลอุปกรณ์"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageEquipment/AddEquipment"
        addbutton="Add Equipment"
        manage="ManageEquipment"
        updateurl="/ManageEquipment/UpdateEquipment"
        viewurl="/ManageEquipment/ViewEquipment"
      />
    </div>
  );
}

export default HocValidateUser(ManageEquipment);
