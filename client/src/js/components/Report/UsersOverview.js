/* eslint-disable no-loop-func */
/* eslint-disable vars-on-top */
/* eslint-disable no-var */
/* eslint-disable no-console */
/* eslint-disable no-plusplus */
/* eslint-disable eqeqeq */
/* eslint-disable camelcase */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-array-constructor */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable react/prop-types */
/* eslint-disable react/no-unused-state */
import React from "react";
import Swal from "sweetalert2";
import axios from "axios";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
  InputGroup,
  DatePicker,
  InputGroupAddon,
  InputGroupText
} from "shards-react";
import classNames from "classnames";

import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from "recharts";
import "../../../assets/range-date-picker.css";

class UsersOverview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      startDate: "",
      endDate: "",
      arrayday: "",
      numday: 28,
      name: "test",
      getrequestgeneral: [],
      getrequestissues: [],
      width: window.innerWidth,
      data: [{ name: "", คำร้องทั่วไป: 0 }, { name: "", คำร้องปัญหาต่างๆ: 0 }]
    };

    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.ChangeFormatTime = this.ChangeFormatTime.bind(this);
    this.appendLeadingZeroes = this.appendLeadingZeroes.bind(this);

    this.canvasRef = React.createRef();
  }

  handleStartDateChange(value) {
    this.setState({
      startDate: new Date(value)
    },() => {
      console.log(this.state.startDate)
    });
  }

  handleEndDateChange(value) {
    this.setState({
      endDate: new Date(value)
    });
  }

  appendLeadingZeroes(n) {
    if (n <= 9) {
      return `0${n}`;
    }
    return n;
  }

  async ChangeFormatTime() {
    this.setState({
      data: []
    });

    const arr = new Array();
    const dt = new Date(this.state.startDate);
    let count = 0;

    let reportGeneral = [];
    let reportIssues = [];
    // let formatted_endDate = this.state.startDate;
    // let formatted_startDate = this.state.endDate;

    // const timeDiff = Math.abs(this.state.endDate.getTime() - this.state.startDate.getTime());
    // const diffNumDay = Math.ceil(timeDiff / (1000 * 3600 * 24));

    while (dt <= this.state.endDate) {
      arr.push(new Date(dt));
      dt.setDate(dt.getDate() + 1);
      count += 1;
    }

    this.setState({
      numday: count,
      arrayday: arr
    });
    // if (this.state.startDate != "") {
    //   const today = this.state.startDate;
    //   formatted_endDate = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
    // }

    // if (this.state.endDate != "") {
    //   const today = this.state.endDate;
    //   formatted_startDate = `${today.getFullYear()}-${today.getMonth() + 1}-${today.getDate()}`;
    // }

    // console.log(formatted_endDate)
    // console.log(formatted_startDate)

    // if (this.state.startDate && this.state.endDate != "") {
    //   for (let i = 0; i < this.state.getrequestgeneral.length; i++) {
    //     const currentDate = new Date(this.state.getrequestgeneral[i].created_at);
    //     console.log(currentDate)
    //     const timeDiff = Math.abs(currentDate.getTime() - this.state.startDate.getTime());
    //     const endDiff = Math.abs(this.state.endDate.getTime() - currentDate.getTime());
    //     const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    //     const enddiffDays = Math.ceil(endDiff / (1000 * 3600 * 24));
    //     console.log(diffDays + " : " + enddiffDays)
    //     if (diffDays >= 0 || enddiffDays <= 0) {
    //       reportGeneral.push(this.state.getrequestgeneral[i]);
    //     }
    //   }

    //   for (let i = 0; i < this.state.getrequestissues.length; i++) {
    //     const currentDate = new Date(this.state.getrequestissues[i].created_at);
    //     const timeDiff = Math.abs(currentDate.getTime() - this.state.startDate.getTime());
    //     const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    //     if (diffDays - diffNumDay >= 0) {
    //       reportIssues.push(this.state.getrequestissues[i]);
    //     }
    //   }
    // }


    const endDate = `${this.state.endDate.getFullYear()}-${this.state.endDate.getMonth() + 1}-${this.state.endDate.getDate()}`
    const startDate = `${this.state.startDate.getFullYear()}-${this.state.startDate.getMonth() + 1}-${this.state.startDate.getDate()}`
    await axios
      .get(`/api/requestgeneral/report/${startDate}/${endDate}`)
      .then(res => {
        reportGeneral = res.data
      })
      .catch(err => {
        console.log(err);
      });

    await axios
      .get(`/api/requestissues/report/${startDate}/${endDate}`)
      .then(res => {
        reportIssues = res.data
      })
      .catch(err => {
        console.log(err);
      });
    
    const setdata = [];

    for (let i = 0; i < count; i++) {
      console.log(reportGeneral.length)
      var daymonthgeneral = `${this.appendLeadingZeroes(
        arr[i].getDate()
      )}/${this.appendLeadingZeroes(arr[i].getMonth() + 1)}`;

      const valuegenaral = reportGeneral.filter(item => {
        const daydbrequestgeneral = new Date(item.created_at);
        const dayformatdb = `${this.appendLeadingZeroes(
          daydbrequestgeneral.getDate()
        )}/${this.appendLeadingZeroes(daydbrequestgeneral.getMonth() + 1)}`;

        return daymonthgeneral == dayformatdb;
      });

      var daymonthissues = `${this.appendLeadingZeroes(
        arr[i].getDate()
      )}/${this.appendLeadingZeroes(arr[i].getMonth() + 1)}`;

      const valueissues = reportIssues.filter(item => {
        const daydbrequestissues = new Date(item.created_at);
        const dayformatdb = `${this.appendLeadingZeroes(
          daydbrequestissues.getDate()
        )}/${this.appendLeadingZeroes(daydbrequestissues.getMonth() + 1)}`;

        return daymonthissues == dayformatdb;
      });

      setdata.push({
        name: daymonthgeneral,
        คำร้องทั่วไป: valuegenaral.length,
        คำร้องปัญหาต่างๆ: valueissues.length
      });
    }

    this.setState({
      data: setdata
    });

    if (setdata == "") {
      Swal.fire("ไม่พบข้อมูล", "ไม่พบข้อมูลในวันที่ท่านเลือก กรุณาตรวจสอบใหม่อีกครั้ง", "error");
    }
  }

  componentDidMount() {
    
  }

  render() {
    const { className } = this.props;
    const classes = classNames(className, "d-flex", "my-auto", "date-range");
    return (
      <Col xl="12" lg="12" md="12" sm="12" className="mb-4">
        <Card small className="h-100">
          <CardHeader className="border-bottom">
            <h6 className="m-0">ดูรายงานคำร้อง</h6>
          </CardHeader>
          <CardBody className="pt-0">
            <Row className="border-bottom py-2 bg-light">
              <Col sm="6" className="d-flex mb-2 mb-sm-0">
                <InputGroup className={classes}>
                  <DatePicker
                    size="sm"
                    selected={this.state.startDate}
                    onChange={this.handleStartDateChange}
                    placeholderText="Start Date"
                    dropdownMode="select"
                    className="text-center"
                    dateFormat="dd-MM-yyyy"
                  />
                  <DatePicker
                    size="sm"
                    selected={this.state.endDate}
                    onChange={this.handleEndDateChange}
                    placeholderText="End Date"
                    dropdownMode="select"
                    className="text-center"
                    dateFormat="dd-MM-yyyy"
                  />
                  <InputGroupAddon type="append">
                    <InputGroupText>
                      <i className="material-icons">&#xE916;</i>
                    </InputGroupText>
                  </InputGroupAddon>

                  <Button
                    size="sm"
                    className="d-flex btn-white ml-auto mr-auto ml-sm-auto mr-sm-0 mt-3 mt-sm-0"
                    onClick={this.ChangeFormatTime}
                  >
                    ค้นหารายงานคำร้อง &rarr;
                  </Button>
                </InputGroup>
              </Col>
            </Row>

            {this.state.data == "" ? (
              <div>
                <h4>กรุณากำหนดช่วงเวลาในการ</h4>
              </div>
            ) : (
              <LineChart
                width={(window.innerWidth * 3) / 4}
                height={400}
                data={this.state.data}
                onChange={this.ChangeFormatTime}
                margin={{ top: 50, right: 10, left: 5, bottom: 5 }}
              >
                <XAxis dataKey="name" />
                <YAxis />
                <CartesianGrid strokeDasharray="3 3" />
                <Tooltip />
                <Legend />
                <Line
                  type="monotone"
                  dataKey="คำร้องปัญหาต่างๆ"
                  stroke="#ef0909"
                  activeDot={{ r: 8 }}
                />
                <Line type="monotone" dataKey="คำร้องทั่วไป" stroke="#0be232" />
              </LineChart>
            )}
          </CardBody>
        </Card>
      </Col>
    );
  }
}

export default UsersOverview;
