/* eslint-disable no-unused-vars */
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataTable from "../DataTable";
import HocValidateUser from "../../../HocValidateUser";

const drawerWidth = 240;


function ManageCompany() {
  // const columns = ["id", "name", "location_id", "Action"];
  const columns = [
    {
      label: 'ID',
      field: 'id',
      sort: 'asc',
    },
    {
      label: 'Company Name',
      field: 'name',
      sort: 'asc',
    },
    {
      label: 'Address',
      field: 'full_address',
      sort: 'asc',
    },
    {
      label: 'Action',
      field: 'Action',
      sort: 'asc',
      col: '20%',
    }
  ];
  return (
    <div>
      <DataTable
        url="/api/company"
        columns={columns}
        name="company"
        headname=" List Company - ข้อมูลบริษัท "
        headTablename="ตารางแสดงข้อมูลบริษัท"
        edit="แก้ไข"
        delete="ลบ"
        addlink="/ManageCompany/AddCompany"
        addbutton="Add Company"
        manage="ManageCompany"
        updateurl="/ManageCompany/UpdateCompany"
        viewurl="/ManageCompany/ViewCompany"
      />
    </div>
  );
}

export default HocValidateUser(ManageCompany);
