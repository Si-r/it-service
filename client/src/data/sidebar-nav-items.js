/* eslint-disable func-names */
/* eslint-disable eqeqeq */
/* eslint-disable array-callback-return */
export default function() {
  const permission = [];
  const role = {
    WELCOME: [{ TITLE: "Blog Dashboard", LINK: "/blog-overview" }]
  };

  // User
  if (
    sessionStorage.getItem("ManageUser") === "1" ||
    sessionStorage.getItem("ManagePermission") === "1"
  ) {
    role.User = [];
    if (sessionStorage.getItem("ManageUser") === "1") {
      role.User.push({ TITLE: "ManageUser", LINK: "/ManageUser" });
    }
    // Permission
    if (sessionStorage.getItem("ManagePermission") === "1") {
      role.User.push({ TITLE: "ManagePermission", LINK: "/ManagePermission" });
    }
  }

  // PERSON
  if (
    sessionStorage.getItem("ManagePersonContact") === "1" ||
    sessionStorage.getItem("ManagePersonResponsible") === "1"
  ) {
    role.PERSON = [];
    if (sessionStorage.getItem("ManagePersonContact") === "1") {
      role.PERSON.push({ TITLE: "ManagePersonContact", LINK: "/ManagePersonContact" });
    }
    if (sessionStorage.getItem("ManagePersonResponsible") === "1") {
      role.PERSON.push({ TITLE: "ManagePersonResponsible", LINK: "/ManagePersonResponsible" });
    }
  }

  // ADDRESS
  if (
    sessionStorage.getItem("ManageAddress") === "1" ||
    sessionStorage.getItem("ManageCompany") === "1" ||
    sessionStorage.getItem("ManageDepartment") === "1" ||
    sessionStorage.getItem("ManageProvince") === "1" ||
    sessionStorage.getItem("ManageDistrict") === "1" ||
    sessionStorage.getItem("ManagePostalCode") === "1" ||
    sessionStorage.getItem("ManageLocation") === "1"
  ) {
    role.ADDRESS = [];
    if (sessionStorage.getItem("ManageAddress") === "1") {
      role.ADDRESS.push({ TITLE: "ManageAddress", LINK: "/ManageAddress" });
    }
    if (sessionStorage.getItem("ManageCompany") === "1") {
      role.ADDRESS.push({ TITLE: "ManageCompany", LINK: "/ManageCompany" });
    }
    if (sessionStorage.getItem("ManageDepartment") === "1") {
      role.ADDRESS.push({ TITLE: "ManageDepartment", LINK: "/ManageDepartment" });
    }
    if (sessionStorage.getItem("ManageProvince") === "1") {
      role.ADDRESS.push({ TITLE: "ManageProvince", LINK: "/ManageProvince" });
    }
    if (sessionStorage.getItem("ManageDistrict") === "1") {
      role.ADDRESS.push({ TITLE: "ManageDistrict", LINK: "/ManageDistrict" });
    }
    if (sessionStorage.getItem("ManagePostalCode") === "1") {
      role.ADDRESS.push({ TITLE: "ManagePostalCode", LINK: "/ManagePostalCode" });
    }
    if (sessionStorage.getItem("ManageLocation") === "1") {
      role.ADDRESS.push({ TITLE: "ManageLocation", LINK: "/ManageLocation" });
    }
  }

  // EQUIPMENT
  if (sessionStorage.getItem("ManageEquipment") === "1") {
    role.Equipment = [];
    if (sessionStorage.getItem("ManageEquipment") === "1") {
      role.Equipment.push({ TITLE: "ManageEquipment", LINK: "/ManageEquipment" });
    }
  }

  // REQUEST
  if (
    sessionStorage.getItem("ManageRequestGeneral") === "1" ||
    sessionStorage.getItem("ManageRequestIssuses") === "1"
  ) {
    role.REQUEST = [];
    if (sessionStorage.getItem("ManageRequestGeneral") === "1") {
      role.REQUEST.push({ TITLE: "ManageRequestGeneral", LINK: "/ManageRequestGeneral" });
    }
    if (sessionStorage.getItem("ManageRequestIssuses") === "1") {
      role.REQUEST.push({ TITLE: "ManageRequestIssuses", LINK: "/ManageRequestIssuses" });
    }
  }

  // NEWS
  if (
    sessionStorage.getItem("ManageNews") === "1" ||
    sessionStorage.getItem("ManageRequestIssuses") === "1"
  ) {
    role.NEWS = [];
    if (sessionStorage.getItem("ManageNews") === "1") {
      role.NEWS.push({ TITLE: "ManageNews", LINK: "/ManageNews" });
    }
    if (sessionStorage.getItem("ManageRequestIssuses") === "1") {
      role.NEWS.push({ TITLE: "ManageSettingNews", LINK: "/ManageSettingNews" });
    }
  }

  // IMAGE

  if (sessionStorage.getItem("ManageImage") === "1") {
    role.IMAGE = [];
    role.IMAGE.push({ TITLE: "ManageImage", LINK: "/ManageImage" });
  }

  // MESSAGE

  if (sessionStorage.getItem("ManageMessage") === "1") {
    role.MESSAGE = [];
    role.MESSAGE.push({ TITLE: "ManageMessage", LINK: "/ManageMessage" });
  }

  // MODIFY

  if (sessionStorage.getItem("ManageModify") === "1") {
    role.MODIFY = [];
    role.MODIFY.push({ TITLE: "ManageModify", LINK: "/ManageModify" });
  }

  // PRIORITY

  if (sessionStorage.getItem("ManagePriority") === "1") {
    role.PRIORITY = [];
    role.PRIORITY.push({ TITLE: "ManagePriority", LINK: "/ManagePriority" });
  }

  // IMPACT

  if (sessionStorage.getItem("ManageImpact") === "1") {
    role.IMPACT = [];
    role.IMPACT.push({ TITLE: "ManageImpact", LINK: "/ManageImpact" });
  }

  // IMPACT

  if (sessionStorage.getItem("Report") === "1") {
    role.REPORT = [];
    role.REPORT.push({ TITLE: "Report", LINK: "/Report" });
  }

  console.log(role);

  permission.push(role);

  //
  // {
  //
  //   USER: [
  //     {TITLE : "ManageUser", LINK : "/ManageUser"},
  //     {TITLE : "ManagePermission", LINK : "/ManagePermission"}
  //   ],
  //   PERSON: [
  //     {TITLE : "ManagePersonContact", LINK : "/ManagePersonContact"},
  //     {TITLE : "ManagePersonResponsible", LINK : "/ManagePersonResponsible"}
  //   ],
  //   ADDRESS: [
  //     {TITLE : "ManageAddress", LINK : "/ManageAddress"},
  //     {TITLE : "ManageCompany", LINK : "/ManageCompany"},
  //     {TITLE : "ManageDepartment", LINK : "/ManageDepartment"},
  //     {TITLE : "ManageProvince", LINK : "/ManageProvince"},
  //     {TITLE : "ManageDistrict", LINK : "/ManageDistrict"},
  //     {TITLE : "ManagePostalCode", LINK : "/ManagePostalCode"},
  //     {TITLE : "ManageLocation", LINK : "/ManageLocation"}
  //   ],
  //   EQUIPMENT: [
  //     {TITLE : "ManageEquipment", LINK : "/ManageEquipment"},
  //   ],
  //   REQUEST: [
  //     {TITLE : "ManageRequestGeneral", LINK : "/ManageRequestGeneral"},
  //     {TITLE : "ManageRequestIssuses", LINK : "/ManageRequestIssuses"},
  //   ],
  //   NEWS: [
  //     {TITLE : "ManageNews", LINK : "/ManageNews"},
  //     {TITLE : "ManageSettingNews", LINK : "/ManageSettingNews"}
  //   ],
  //   IMAGE: [
  //     {TITLE : "ManageImage", LINK : "/ManageImage"}
  //   ],
  //   MESSAGE: [
  //     {TITLE : "ManageMessage", LINK : "/ManageMessage"}
  //   ],
  //   MODIFY: [
  //     {TITLE : "ManageModify", LINK : "/ManageModify"}
  //   ],
  //   PRIORITY: [
  //     {TITLE : "ManagePriority", LINK : "/ManagePriority"}
  //   ],
  //   IMPACT: [
  //     {TITLE : "ManageImpact", LINK : "/ManageImpact"}
  //   ],
  //   REPORT: [
  //     {TITLE : "Report", LINK : "/Report"}
  //   ],
  // }

  return permission;
}
