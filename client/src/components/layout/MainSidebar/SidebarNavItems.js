/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable no-new-wrappers */
/* eslint-disable func-names */
/* eslint-disable class-methods-use-this */
import React from "react";
import { Nav } from "shards-react";

import { Collapse } from "reactstrap";
import SidebarNavItem from "./SidebarNavItem";
import { Store } from "../../../flux";

class SidebarNavItems extends React.Component {
  constructor(props) {
    super(props);

    const currentUrl = window.location.pathname;
    const childeUrl = currentUrl.split("/");

    this.state = {
      navItems: Store.getSidebarItems(),
      collapsed: childeUrl[1]
    };

    // this.toggleNavbar = this.toggleNavbar.bind(this);
    // this.onChange = this.onChange.bind(this);
  }

  componentWillMount() {
    Store.addChangeListener(this.onChange);
  }

  componentWillUnmount() {
    Store.removeChangeListener(this.onChange);
  }

  onChange() {
    this.setState({
      ...this.state,
      navItems: Store.getSidebarItems()
    });
  }

  toggleNavbar(keyName) {
    let { collapsed } = this.state;

    collapsed = keyName;

    this.setState({
      collapsed,
    });
  }

  toggleIsOpen(keyName) {

    if(this.state.collapsed === keyName) {
      return true;
    }


    const { navItems: permission } = this.state;
    const correntKey = this.state.collapsed;
    let status = false;
    permission.map(item => {
      Object.entries(item).map(([key, values]) => {
        if(key === keyName) {
          Object.values(values).forEach(function(value) {
            if(value.TITLE === correntKey) {
              status = true;
            }
          })
        }
        return false;
      })
      return false;
    });

    return status;

    // return true;
  }

  itemNav(keyName, values) {
    const arr = [];
    Object.values(values).forEach(function(value) {
      if (sessionStorage[value.TITLE] === "1") {
        arr.push({
          title: value.TITLE,
          htmlBefore: '<i class="material-icons">table_chart</i>',
          htmlAfter: "",
          to: value.LINK
        });
      } else if (keyName === "WELCOME") {
        arr.push({
          title: value.TITLE,
          htmlBefore: '<i class="material-icons">table_chart</i>',
          htmlAfter: "",
          to: value.LINK
        });
      }
    });

    return (
      <div key={keyName}>
        <h4
          id={keyName}
          onClick={() => this.toggleNavbar(keyName)}
          style={{
            borderTopStyle: "solid",
            borderTopWidth: "thin",
            textTransform: "uppercase",
            fontWeight: "600",
            fontSize: "12px",
            letterSpacing: "1px",
            color: "rgb(138, 147, 165)",
            padding: "10px 20px 0px",
            margin: "10px 0px 0px"
          }}
        >
          {keyName}
        </h4>
        <Collapse isOpen={this.toggleIsOpen(keyName)}>
          {arr.map((item, idx) => (
            <SidebarNavItem key={idx} item={item} />
          ))}
        </Collapse>
      </div>
    );
  }

  render() {
    const { navItems: permission } = this.state;

    return (
      <div className="nav-wrapper">
        <Nav className="nav--no-borders flex-column">
          {permission.map(item =>
            Object.entries(item).map(([keyName, values]) => this.itemNav(keyName, values))
          )
          }
        </Nav>
      </div>
    );
  }
}

export default SidebarNavItems;
