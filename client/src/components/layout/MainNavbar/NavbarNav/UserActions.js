/* eslint-disable class-methods-use-this */
import React from "react";
import { Link } from "react-router-dom";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink
} from "shards-react";

export default class UserActions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false
    };

    this.toggleUserActions = this.toggleUserActions.bind(this);
    this.logout = this.logout.bind(this);
  }

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible
    });
  }

  logout() {
    sessionStorage.clear();
    sessionStorage.removeItem("Token");
  }

  render() {
    return (
      <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
        <DropdownToggle caret tag={NavLink} className="text-nowrap px-3">
          <img
            className="user-avatar rounded-circle mr-2"
            src={require("./../../../../images/avatars/3.jpg")}
            alt="User Avatar"
          />
          <span className="d-none d-md-inline-block">{sessionStorage.email}</span>
        </DropdownToggle>
        <Collapse tag={DropdownMenu} right small open={this.state.visible}>
          <DropdownItem tag={Link} to="/Welcome/Profile">
            <i className="material-icons">&#xE7FD;</i>
            Profile
          </DropdownItem>
          <DropdownItem tag={Link} to="/Welcome/ProfileEdit">
            <i className="material-icons">&#xE8B8;</i>
            Edit Profile
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem className="text-danger">
            <Link to="/" onClick={() => this.logout()}>
              <i className="material-icons text-danger">&#xE879;</i>
              Logout
            </Link>
          </DropdownItem>
        </Collapse>
      </NavItem>
    );
  }
}
