import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem,
} from "shards-react";
import img from '../../images/contact/55339.jpg'

const SidebarCategories = ({ title }) => (
  <Card small className="mb-3">
    <CardHeader className="border-bottom">
      <h6 className="m-0">{title}</h6>
    </CardHeader>
    <CardBody className="p-0">
    <ListGroup flush>
        <ListGroupItem className="px-3 pb-2">
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">local_phone</i>
            089-1115321
          </span>
        </ListGroupItem>

        <ListGroupItem className="px-3 pb-2">
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">local_phone</i>
            061-4194106
          </span>
        </ListGroupItem>
    </ListGroup>
      <img src={img}></img>


    </CardBody>
  </Card>
);

SidebarCategories.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

SidebarCategories.defaultProps = {
  title: "Contact"
};

export default SidebarCategories;
