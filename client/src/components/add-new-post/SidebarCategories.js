import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardHeader,
  CardBody,
  ListGroup,
  ListGroupItem,
} from "shards-react";

import { Link } from "react-router-dom";

const SidebarCategories = ({ title }) => (
  <Card small className="mb-3">
    <CardHeader className="border-bottom">
      <h6 className="m-0">{title}</h6>
    </CardHeader>
    <CardBody className="p-0">
      <ListGroup flush>
        <ListGroupItem className="px-3 pb-2">
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">person</i>
            <Link
              to="/Welcome/ProfileEdit"
            >
              แก้ไขข้อมูลส่วนตัว
            </Link>
          </span>


          <span className="d-flex mb-2">
            <i className="material-icons mr-1">email</i>
            <Link
              to="/ManageRequestGeneral"
            >
              ส่งคำร้องขอใช้งาน
            </Link>
          </span>

          <span className="d-flex mb-2">
            <i className="material-icons mr-1">email</i>
            <Link
              to="/ManageRequestIssuses"
            >
              ส่งคำร้องแจ้งปัญหา
            </Link>
          </span>


        </ListGroupItem>


      </ListGroup>
    </CardBody>
  </Card>
);

SidebarCategories.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string
};

SidebarCategories.defaultProps = {
  title: "ระบบนำทาง"
};

export default SidebarCategories;
