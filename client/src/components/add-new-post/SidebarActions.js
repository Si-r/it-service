/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/require-default-props */
/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import PropTypes from "prop-types";
import { Card, CardHeader, CardBody, ListGroup, ListGroupItem, Button } from "shards-react";
import { Link } from "@material-ui/core";

const SidebarActions = ({ title, firstName, lastName}) => (
  <Card small className="mb-3">
    <CardHeader className="border-bottom">
      <h6 className="m-0">{title}</h6>
    </CardHeader>

    <CardBody className="p-0">
      <ListGroup flush>
        <ListGroupItem className="p-3">
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">person</i>
            <strong className="mr-1">ชื่อ-นามสกุล:</strong>
            {firstName}
            {' '}
            {lastName}
          </span>
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">person</i>
            <strong className="mr-1">Username:</strong>
            {sessionStorage.username}
          </span>
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">flag</i>
            <strong className="mr-1">Email:</strong>
            {sessionStorage.email}
          </span>
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">visibility</i>
            <strong className="mr-1">Permission:</strong>
            <strong className="text-success">{sessionStorage.permission_name}</strong>
          </span>
          <span className="d-flex mb-2">
            <i className="material-icons mr-1">calendar_today</i>
            <strong className="mr-1">Last login:</strong>
            {sessionStorage.last_login}
          </span>
          <span className="d-flex">
            <i className="material-icons mr-1">score</i>
            <strong className="mr-1">Status:</strong>
            <strong className="text-warning">
              {sessionStorage.is_block}
            </strong>
          </span>
        </ListGroupItem>
        <ListGroupItem className="d-flex px-3 border-0">
          <Link
            className="ml-auto"
            href="/"
            onClick={() => {sessionStorage.clear();
              sessionStorage.removeItem("Token");
            }}
          >
            <Button
              theme="danger"
              size="sm"
            >
              <i className="material-icons">lock</i>
              logout
            </Button>
          </Link>
        </ListGroupItem>
      </ListGroup>
    </CardBody>
  </Card>
);

SidebarActions.propTypes = {
  title: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
};

SidebarActions.defaultProps = {
  title: "Profile"
};

export default SidebarActions;
