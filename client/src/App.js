/* eslint-disable react/no-array-index-key */
/* eslint-disable import/order */
import React from 'react'
import routes from "./routes";
import withTracker from "./withTracker";
import { BrowserRouter, Route } from 'react-router-dom'

import Login from "./js/components/auth/Login";

import "./styles/css/login.css";
import "./styles/css/util.css";
import "react-bootstrap-table/dist/react-bootstrap-table-all.min.css";
import 'rc-datepicker/lib/style.css';
import "./styles/main.css";

export default () => (
  <BrowserRouter basename={process.env.REACT_APP_BASENAME || ""}>
    <Route exact path="/" component={Login} />
    <div>
      {routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={withTracker(props => {
              return (
                <route.layout {...props}>
                  <route.component {...props} />
                </route.layout>
              );
            })}
          />
        );
      })}

    </div>
  </BrowserRouter>
);
