/* eslint-disable max-len */
import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout } from "./layouts";

// Route Views
// import BlogOverview from "./views/BlogOverview";

// Address Route
import ManageAddress from "./js/components/ManageAddress/ManageAddress";
import ViewAddress from "./js/components/ManageAddress/ViewAddress";
import UpdateAddress from "./js/components/ManageAddress/UpdateAddress";
import AddAddress from "./js/components/ManageAddress/AddAddress";

// Location
import ManageLocation from "./js/components/ManageLocation/ManageLocation";
import ViewLocation from "./js/components/ManageLocation/ViewLocation";
import UpdateLocation from "./js/components/ManageLocation/UpdateLocation";
import AddLocation from "./js/components/ManageLocation/AddLocation";

// Company
import ManageCompany from "./js/components/ManageCompany/ManageCompany";
import ViewCompany from "./js/components/ManageCompany/ViewCompany";
import UpdateCompany from "./js/components/ManageCompany/UpdateCompany";
import AddCompany from "./js/components/ManageCompany/AddCompany";

// Department
import ManageDepartment from "./js/components/ManageDepartment/ManageDepartment";
import AddDepartment from "./js/components/ManageDepartment/AddDepartment";
import UpdateDepartment from "./js/components/ManageDepartment/UpdateDepartment";
import ViewDepartment from "./js/components/ManageDepartment/ViewDepartment";

// User
import ManageUser from "./js/components/ManageUser/ManageUser";
import ViewUser from "./js/components/ManageUser/ViewUser";
import AddUser from "./js/components/ManageUser/AddUser";
import UpdateUser from "./js/components/ManageUser/UpdateUser";

// Permission
import ManagePermission from "./js/components/ManagePermission/ManagePermission";
import AddPermission from "./js/components/ManagePermission/AddPermission";
import UpdatePermission from "./js/components/ManagePermission/UpdatePermission";
import ViewPermission from "./js/components/ManagePermission/ViewPermission";

// Person Responsible

import ManagePersonResponsible from "./js/components/ManagePersonResponsible/ManagePersonResonsible";
import AddPersonResponsible from "./js/components/ManagePersonResponsible/AddPersonResponsible";
import UpdatePersonResponsible from "./js/components/ManagePersonResponsible/UpdatePersonResponsible";
import ViewPersonResponsible from "./js/components/ManagePersonResponsible/ViewPersonResponsible";

// Person Contact
import ManagePersonContact from "./js/components/ManagePersonContact/ManagePersonContact";
import AddPersonContact from "./js/components/ManagePersonContact/AddPersoncontact";
import UpdatePersonContact from "./js/components/ManagePersonContact/UpdatePersoncontact";
import ViewPersonContact from "./js/components/ManagePersonContact/ViewPersonContact";

// Province

import ManageProvince from "./js/components/ManageProvince/ManageProvince";
import AddProvince from "./js/components/ManageProvince/AddProvince";
import UpdateProvince from "./js/components/ManageProvince/UpdateProvince";
import ViewProvince from "./js/components/ManageProvince/ViewProvince";

// District
import ManageDistrict from "./js/components/ManageDistrict/ManageDistrict";
import AddDistrict from "./js/components/ManageDistrict/AddDistrict";
import UpdateDistrict from "./js/components/ManageDistrict/UpdateDistrict";
import ViewDistrict from "./js/components/ManageDistrict/ViewDistrict";

// PostalCode
import ManagePostalCode from "./js/components/ManagePostalCode/ManagePostalCode";
import AddPostalCode from "./js/components/ManagePostalCode/AddPostalcode";
// import UpdatePostalCode from "./js/components/ManagePostalCode/UpdatePostalcode";
// import ViewPostalCode from "./js/components/ManagePostalCode/ViewPostalcode";

// Equipment
import ManageEquipment from "./js/components/ManageEquipment/ManageEquipment";
import AddEquipment from "./js/components/ManageEquipment/AddEquipment";
import UpdateEquipment from "./js/components/ManageEquipment/UpdateEquipment";
import ViewEquipment from "./js/components/ManageEquipment/ViewEquipment";

// RequestGeneral
import ManageRequestGeneral from "./js/components/ManageRequestGeneral/ManageRequestGeneral";
import AddRequestGeneral from "./js/components/ManageRequestGeneral/AddRequestGeneral";
import UpdateRequestGeneral from "./js/components/ManageRequestGeneral/UpdateRequestGeneral";
import ViewRequestGeneral from "./js/components/ManageRequestGeneral/ViewRequestGeneral";

// RequestIssuses
import ManageRequestIssuses from "./js/components/ManageRequestIssuses/ManageRequestIssuses";
import AddRequestIssues from "./js/components/ManageRequestIssuses/AddRequestIssuses";
import UpdateRequestIssuses from "./js/components/ManageRequestIssuses/UpdateRequestissuses";
import ViewRequestIssuses from "./js/components/ManageRequestIssuses/ViewRequestIssuses";

// News
import ManageNews from "./js/components/ManageNews/ManageNews";
import AddNews from "./js/components/ManageNews/AddNews";
import UpdateNews from "./js/components/ManageNews/UpdateNews";
import ViewNews from "./js/components/ManageNews/ViewNews";

// SettingNews
import ManageSettingNews from "./js/components/ManageSettingNews/ManageSettingNews";
import AddSettingNews from "./js/components/ManageSettingNews/AddSettingNews";
import UpdateSettingnews from "./js/components/ManageSettingNews/UpdateSettingnews";
import ViewSettingNews from "./js/components/ManageSettingNews/ViewSettingNews";

// Image
import ManageImage from "./js/components/ManageImage/ManageImage";

// Message
import ManageMessage from "./js/components/ManageMessage/ManageMessage";
import AddMessage from "./js/components/ManageMessage/AddMessage";
import UpdateMessage from "./js/components/ManageMessage/UpdateMessage";
import ViewMessage from "./js/components/ManageMessage/ViewMessage";

// Modify
import ManageModify from "./js/components/ManageModify/MangeModify";
import AddModify from "./js/components/ManageModify/AddModify";
import UpdateModify from "./js/components/ManageModify/UpdateModify";
import ViewModify from "./js/components/ManageModify/ViewModify";

// Priority
import ManagePriority from "./js/components/ManagePriority/ManagePriority";
import AddPriority from "./js/components/ManagePriority/AddPriority";
import UpdatePriority from "./js/components/ManagePriority/UpdatePriority";
import ViewPriority from "./js/components/ManagePriority/ViewPriority";

// Impact
import ManageImpact from "./js/components/ManageImpact/ManageImpact";
import AddImpact from "./js/components/ManageImpact/AddImpact";
import UpdateImpact from "./js/components/ManageImpact/UpdateImpact";
import ViewImpact from "./js/components/ManageImpact/ViewImpact";

import Report from "./js/components/Report/ReportIncident";

import Profile from "./js/components/Welcome/Profile"
import EditProfile from "./js/components/ManageUser/EditProfile";

export default [
  {
    path: "/Dashboard",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/Welcome/Profile" />
  },
  {
    path: "/blog-overview",
    exact: true,
    layout: DefaultLayout,
    component: Profile
  },
  {
    path: "/Welcome/Profile",
    layout: DefaultLayout,
    component: Profile,
  },
  {
    path: "/Welcome/ProfileEdit",
    layout: DefaultLayout,
    component: EditProfile,
  },

  // Company
  {
    path: "/ManageCompany",
    layout: DefaultLayout,
    component: ManageCompany,
    exact: true
  },
  {
    path: "/ManageCompany/ViewCompany/:id",
    layout: DefaultLayout,
    component: ViewCompany
  },
  {
    path: "/ManageCompany/UpdateCompany/:id",
    layout: DefaultLayout,
    component: UpdateCompany
  },
  {
    path: "/ManageCompany/AddCompany",
    layout: DefaultLayout,
    component: AddCompany
  },

  // User

  {
    path: "/ManageUser",
    layout: DefaultLayout,
    component: ManageUser,
    exact: true
  },
  {
    path: "/ManageUser/AddUser",
    layout: DefaultLayout,
    component: AddUser
  },
  {
    path: "/ManageUser/ViewUser/:id",
    layout: DefaultLayout,
    component: ViewUser
  },
  {
    path: "/ManageUser/UpdateUser/:id",
    layout: DefaultLayout,
    component: UpdateUser
  },

  // Permission
  {
    path: "/ManagePermission",
    layout: DefaultLayout,
    component: ManagePermission,
    exact: true
  },
  {
    path: "/ManagePermission/AddPermission",
    layout: DefaultLayout,
    component: AddPermission
  },
  {
    path: "/ManagePermission/UpdatePermission/:id",
    layout: DefaultLayout,
    component: UpdatePermission
  },
  {
    path: "/ManagePermission/ViewPermission/:id",
    layout: DefaultLayout,
    component: ViewPermission
  },

  // Person

  // Person Responsible
  {
    path: "/ManagePersonResponsible",
    layout: DefaultLayout,
    component: ManagePersonResponsible,
    exact: true
  },
  {
    path: "/ManagePersonResponsible/AddPersonresponsible",
    layout: DefaultLayout,
    component: AddPersonResponsible
  },
  {
    path: "/ManagePersonResponsible/UpdatePersonResponsible/:id",
    layout: DefaultLayout,
    component: UpdatePersonResponsible
  },
  {
    path: "/ManagePersonResponsible/ViewPersonResponsible/:id",
    layout: DefaultLayout,
    component: ViewPersonResponsible
  },

  // Person Contact

  {
    path: "/ManagePersonContact",
    layout: DefaultLayout,
    component: ManagePersonContact,
    exact: true
  },
  {
    path: "/ManagePersonContact/AddPersonContact",
    layout: DefaultLayout,
    component: AddPersonContact
  },
  {
    path: "/ManagePersonContact/UpdatePersonContact/:id",
    layout: DefaultLayout,
    component: UpdatePersonContact
  },
  {
    path: "/ManagePersonContact/ViewPersonContact/:id",
    layout: DefaultLayout,
    component: ViewPersonContact
  },

  // Department

  {
    path: "/ManageDepartment",
    layout: DefaultLayout,
    component: ManageDepartment,
    exact: true
  },
  {
    path: "/ManageDepartment/AddDepartment",
    layout: DefaultLayout,
    component: AddDepartment
  },
  {
    path: "/ManageDepartment/UpdateDepartment/:id",
    layout: DefaultLayout,
    component: UpdateDepartment
  },
  {
    path: "/ManageDepartment/ViewDepartment/:id",
    layout: DefaultLayout,
    component: ViewDepartment
  },

  // Equipment

  {
    path: "/ManageEquipment",
    layout: DefaultLayout,
    component: ManageEquipment,
    exact: true
  },
  {
    path: "/ManageEquipment/AddEquipment",
    layout: DefaultLayout,
    component: AddEquipment
  },
  {
    path: "/ManageEquipment/UpdateEquipment/:id",
    layout: DefaultLayout,
    component: UpdateEquipment
  },
  {
    path: "/ManageEquipment/ViewEquipment/:id",
    layout: DefaultLayout,
    component: ViewEquipment
  },

  // Province
  {
    path: "/ManageProvince",
    layout: DefaultLayout,
    component: ManageProvince,
    exact: true
  },
  {
    path: "/ManageProvince/AddProvince",
    layout: DefaultLayout,
    component: AddProvince
  },
  {
    path: "/ManageProvince/UpdateProvince/:id",
    layout: DefaultLayout,
    component: UpdateProvince
  },
  {
    path: "/ManageProvince/ViewProvince/:id",
    layout: DefaultLayout,
    component: ViewProvince
  },

  // District

  {
    path: "/ManageDistrict",
    layout: DefaultLayout,
    component: ManageDistrict,
    exact: true
  },
  {
    path: "/ManageDistrict/AddDistrict",
    layout: DefaultLayout,
    component: AddDistrict
  },
  {
    path: "/ManageDistrict/UpdateDistrict/:id",
    layout: DefaultLayout,
    component: UpdateDistrict
  },
  {
    path: "/ManageDistrict/ViewDistrict/:id",
    layout: DefaultLayout,
    component: ViewDistrict
  },

  // PostalCode
  {
    path: "/ManagePostalCode",
    layout: DefaultLayout,
    component: ManagePostalCode,
    exact: true
  },

  {
    path: "/ManagePostalCode/AddPostalCode",
    layout: DefaultLayout,
    component: AddPostalCode
  },

  // RequestGeneral

  {
    path: "/ManageRequestGeneral",
    layout: DefaultLayout,
    component: ManageRequestGeneral,
    exact: true
  },
  {
    path: "/ManageRequestGeneral/AddRequestGeneral",
    layout: DefaultLayout,
    component: AddRequestGeneral
  },
  {
    path: "/ManageRequestGeneral/UpdateRequestGeneral/:id",
    layout: DefaultLayout,
    component: UpdateRequestGeneral
  },
  {
    path: "/ManageRequestGeneral/ViewRequestGeneral/:id",
    layout: DefaultLayout,
    component: ViewRequestGeneral
  },
  // {
  //   path: "/ManageSubDistrict",
  //   layout: DefaultLayout,
  //   component: ManageSubDistrict
  // },

  // RequestIssuses

  {
    path: "/ManageRequestIssuses",
    layout: DefaultLayout,
    component: ManageRequestIssuses,
    exact: true
  },
  {
    path: "/ManageRequestIssuses/AddRequestIssues",
    layout: DefaultLayout,
    component: AddRequestIssues
  },
  {
    path: "/ManageRequestIssuses/UpdateRequestIssues/:id",
    layout: DefaultLayout,
    component: UpdateRequestIssuses
  },

  {
    path: "/ManageRequestIssuses/ViewRequestIssues/:id",
    layout: DefaultLayout,
    component: ViewRequestIssuses
  },

  // Message
  {
    path: "/ManageMessage",
    layout: DefaultLayout,
    component: ManageMessage,
    exact: true
  },
  {
    path: "/ManageMessage/AddMessage",
    layout: DefaultLayout,
    component: AddMessage
  },
  {
    path: "/ManageMessage/UpdateMessage/:id",
    layout: DefaultLayout,
    component: UpdateMessage
  },
  {
    path: "/ManageMessage/ViewMessage/:id",
    layout: DefaultLayout,
    component: ViewMessage
  },

  // Image
  {
    path: "/ManageImage",
    layout: DefaultLayout,
    component: ManageImage
  },

  // Impact
  {
    path: "/ManageImpact",
    layout: DefaultLayout,
    component: ManageImpact,
    exact: true
  },
  {
    path: "/ManageImpact/AddImpact",
    layout: DefaultLayout,
    component: AddImpact
  },
  {
    path: "/ManageImpact/UpdateImpact/:id",
    layout: DefaultLayout,
    component: UpdateImpact
  },
  {
    path: "/ManageImpact/ViewImpact/:id",
    layout: DefaultLayout,
    component: ViewImpact
  },

  // Modify
  {
    path: "/ManageModify",
    layout: DefaultLayout,
    component: ManageModify,
    exact: true
  },
  {
    path: "/ManageModify/AddModify",
    layout: DefaultLayout,
    component: AddModify,
  },
  {
    path: "/ManageModify/UpdateModify/:id",
    layout: DefaultLayout,
    component: UpdateModify,
  },
  {
    path: "/ManageModify/ViewModify/:id",
    layout: DefaultLayout,
    component: ViewModify,

  },

  // News
  {
    path: "/ManageNews",
    layout: DefaultLayout,
    component: ManageNews,
    exact: true
  },
  {
    path: "/ManageNews/AddNews",
    layout: DefaultLayout,
    component: AddNews
  },
  {
    path: "/ManageNews/UpdateNews/:id",
    layout: DefaultLayout,
    component: UpdateNews
  },
  {
    path: "/ManageNews/ViewNews/:id",
    layout: DefaultLayout,
    component: ViewNews
  },

  // SettingNews
  {
    path: "/ManageSettingNews",
    layout: DefaultLayout,
    component: ManageSettingNews,
    exact: true
  },
  {
    path: "/ManageSettingNews/AddSettingNews",
    layout: DefaultLayout,
    component: AddSettingNews,
  },
  {
    path: "/ManageSettingNews/UpdateSettingnews/:id",
    layout: DefaultLayout,
    component: UpdateSettingnews,
  },
  {
    path: "/ManageSettingNews/ViewSettingNews/:id",
    layout: DefaultLayout,
    component: ViewSettingNews,
  },

  // Priority
  {
    path: "/ManagePriority",
    layout: DefaultLayout,
    component: ManagePriority,
    exact: true
  },
  {
    path: "/ManagePriority/AddPriority",
    layout: DefaultLayout,
    component: AddPriority,
  },
  {
    path: "/ManagePriority/UpdatePriority/:id",
    layout: DefaultLayout,
    component: UpdatePriority,
  },
  {
    path: "/ManagePriority/ViewPriority/:id",
    layout: DefaultLayout,
    component: ViewPriority,
  },

  // Address Group
  {
    path: "/ManageAddress",
    layout: DefaultLayout,
    exact: true,
    component: ManageAddress
  },
  {
    path: "/ManageAddress/AddAddress",
    layout: DefaultLayout,
    component: AddAddress
  },
  {
    path: "/ManageAddress/UpdateAddress/:id",
    layout: DefaultLayout,
    component: UpdateAddress
  },
  {
    path: "/ManageAddress/ViewAddress/:id",
    layout: DefaultLayout,
    component: ViewAddress
  },

  // Location Group
  {
    path: "/ManageLocation",
    layout: DefaultLayout,
    component: ManageLocation,
    exact: true
  },
  {
    path: "/ManageLocation/AddLocation",
    layout: DefaultLayout,
    component: AddLocation
  },
  {
    path: "/ManageLocation/UpdateLocation/:id",
    layout: DefaultLayout,
    component: UpdateLocation
  },
  {
    path: "/ManageLocation/ViewLocation/:id",
    layout: DefaultLayout,
    component: ViewLocation
  },

  {
    path: "/Report",
    layout: DefaultLayout,
    component: Report
  }
];
