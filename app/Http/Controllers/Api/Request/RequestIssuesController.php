<?php

namespace App\Http\Controllers\Api\Request;

use App\Http\Controllers\Controller;
use App\Library\LineNotifyModel as AppLineNotifyModel;
use App\Models\DimRequestIssuesModel;
use App\Models\DimUserModel;
use App\Models\FactRequestIssuesModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RequestIssuesController extends Controller
{
    public function index(Request $request)
    {
        $client_id = $request->header('User');
        if($client_id != null) {
            $dimUser = DimUserModel::where(['id' => $client_id])->first();

            if ($dimUser['user_right'] == "admin" || $dimUser['user_right'] == "staff") {
                $getdata = DimRequestIssuesModel::with(['client', 'FactRequestIssues', 'approvalUser', 'auditorUser'])->whereNull('deleted_at')->get();
            } else {
                $getdata = DimRequestIssuesModel::with(['client', 'FactRequestIssues', 'approvalUser', 'auditorUser'])
                    ->whereNull('deleted_at')
                    ->where(['client_id' => $client_id])
                    ->get();
            }
        }else {
            $getdata = DimRequestIssuesModel::with(['client', 'FactRequestIssues', 'approvalUser', 'auditorUser'])->whereNull('deleted_at')->get();
        }

        return $getdata->toJson();
    }

    public function report($start , $end)
    {
        $getdata = DimRequestIssuesModel::with(['client', 'FactRequestIssues', 'approvalUser', 'auditorUser'])
        ->whereBetween('created_at', [$start, $end])
        ->whereNull('deleted_at')->get();

        return $getdata->toJson();
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'client_id' => 'required',
            'name' => 'required',
            'status' => 'required',
            'auditor_user_id' => 'nullable',
            'approval_user_id' => 'nullable',
            'audit_timestamp' => 'nullable|date',
            'approval_timestamp' => 'nullable|date',

            'equipment_id' => 'required',
            'issues_name' => 'required',
            'issues_comment' => 'required',
            'issues_level' => 'required',

            'modify_id' => 'nullable',
            'modify_status' => 'nullable',
            'impact_id' => 'nullable',
            'priority_id' => 'nullable',
        ]);

        try {
            DB::beginTransaction();

            $dimRequestIssues = DimRequestIssuesModel::create([
                'client_id' => $validatedData['client_id'],
                'name' => $validatedData['name'],
                'status' => $validatedData['status'],
            ]);

            $factRequestIssues = FactRequestIssuesModel::create([
                'client_id' => $validatedData['client_id'],
                'request_issues_id' => $dimRequestIssues->id,
                'equipment_id' => $validatedData['equipment_id'],
                'issues_name' => $validatedData['issues_name'],
                'issues_comment' => $validatedData['issues_comment'],
                'issues_level' => $validatedData['issues_level'],
                'request_sort' => $validatedData['client_id'],
            ]);

            DB::commit();

            $lineNotifyModel = new AppLineNotifyModel;
            $lineNotifyModel->notify_message("มีการเพิ่ม รายงานปัญหา เลขที่ #" . $dimRequestIssues['id']);
        } catch (\Exception $th) {
            Log::info($th);
            DB::rollBack();
            // Log::info($th);

            return response()->json(array(
                'code' => 404,
                'message' => 'can not insert data',
            ), 404);
        }

        return response()->json(compact(['dimRequestIssues', 'factRequestIssues']));
    }

    public function edit($id)
    {
        $user = DimRequestIssuesModel::with(
            [
                'client',
                'factRequestIssues',
                'factRequestIssues.dimEquipment',
                'factRequestIssues.modify',
                'factRequestIssues.impact',
                'factRequestIssues.priority',
            ])->findOrFail($id);

        return response()->json(
            $user
        );
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'issues_name' => 'required',
            'status' => 'required',
            'auditor_user_id' => 'required',
            'approval_user_id' => 'required',
            'audit_timestamp' => 'required',
            'approval_timestamp' => 'required',
            'auditor_user_id' => 'required',
            'auditor_user_id' => 'required',
        ]);

        $updateData = [
            'name' => $validatedData['issues_name'],
            'status' => $validatedData['status'],
        ];

        if ($validatedData['status'] == 'Approve') {
            $updateData['approval_user_id'] = $validatedData['approval_user_id'];
            $updateData['approval_timestamp'] = $validatedData['approval_timestamp'];
            $validatedFactData = $request->validate([
                'modify_id' => 'required',
                'impact_id' => 'required',
                'priority_id' => 'required',
            ]);

            $updateFactData['modify_id'] = $validatedFactData['modify_id'];
            $updateFactData['impact_id'] = $validatedFactData['impact_id'];
            $updateFactData['priority_id'] = $validatedFactData['priority_id'];

            $factRequestIssues = FactRequestIssuesModel::where('request_issues_id', $id)->update([
                'modify_id' => $updateFactData['modify_id'],
                'impact_id' => $updateFactData['impact_id'],
                'priority_id' => $updateFactData['priority_id'],
            ]);

            $lineNotifyModel = new AppLineNotifyModel;
            $lineNotifyModel->notify_message("รายงานปัญหา เลขที่ #" . $id . " ได้รับการอนุมัติแล้ว");
        }

        if ($validatedData['status'] == 'Auditor') {
            $updateData['auditor_user_id'] = $validatedData['auditor_user_id'];
            $updateData['audit_timestamp'] = $validatedData['audit_timestamp'];
        }

        if ($validatedData['status'] == 'Rejected') {
            $updateData['approval_user_id'] = $validatedData['approval_user_id'];
            $updateData['approval_timestamp'] = $validatedData['approval_timestamp'];

            $lineNotifyModel = new AppLineNotifyModel;
            $lineNotifyModel->notify_message("มีการเพิ่ม รายงานปัญหา เลขที่ #" . $id . " ถูกปฏิเสธ");
        }

        $dimRequestIssues = DimRequestIssuesModel::where('id', $id)->update($updateData);

        return $dimRequestIssues;
    }

    public function updateAudit(Request $request, $id)
    {
        $validatedData = $request->validate([
            'auditor_user_id' => 'required',
            'audit_timestamp' => 'required',
        ]);

        $dimRequestIssuesModel = DimRequestIssuesModel::where('id', $id)->update([
            'status' => 'Auditor',
            'auditor_user_id' => $validatedData['auditor_user_id'],
            'audit_timestamp' => $validatedData['audit_timestamp'],
        ]);

        return response()->json($dimRequestIssuesModel);
    }

    public function destroy($id)
    {
        DimRequestIssuesModel::destroy($id);
        return response()->json('delete successfully' . $id);
    }
    public function destroy_select(Request $request)
    {
        DimRequestIssuesModel::destroy($request->foo);

        return response()->json([
            'data' => $request->foo,
        ]);
    }
}
