<?php

namespace App\Http\Controllers\Api\Request;

use App\Http\Controllers\Controller;
use App\Library\LineNotifyModel as AppLineNotifyModel;
use App\Models\DimRequestGeneralModel;
use App\Models\DimUserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\FactRequestGeneralModel;
use Illuminate\Support\Facades\Log;

class RequestGeneralController extends Controller
{
    public function index(Request $request)
    {
        $client_id = $request->header('User');
        if($client_id != null) {
            $dimUser = DimUserModel::where(['id' => $client_id])->first();

            if($dimUser['user_right'] == "admin" || $dimUser['user_right'] == "staff") {
                $getdata = DimRequestGeneralModel::with(['client', 'auditorUser', 'approvalUser'])->whereNull('deleted_at')->get();
            }else{
                $getdata = DimRequestGeneralModel::with(['client', 'auditorUser', 'approvalUser'])
                    ->whereNull('deleted_at')
                    ->where(['client_id' => $client_id])
                    ->get();
            }   
        }else {
            $getdata = DimRequestGeneralModel::with(['client', 'auditorUser', 'approvalUser'])->whereNull('deleted_at')->get();
        }
        

        return $getdata->toJson();
    }

    public function report($start , $end)
    {
        $getdata = DimRequestGeneralModel::with(['client', 'auditorUser', 'approvalUser'])
        ->whereBetween('created_at', [$start, $end])
        ->whereNull('deleted_at')->get();

        return $getdata->toJson();
    }

    public function select($id)
    {
        $getdata = DimRequestGeneralModel::with(['factRequestGeneral', 'factRequestGeneral.dimEquipment'])->whereNull('deleted_at')->findOrFail($id);
        return $getdata->toJson();
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'client_id' => 'required',
            'name' => 'required',
            'status' => 'required',
            'equipment_id' => 'required',
            'access_date' => 'required|date',
            'access_time' => 'required|integer',
            'access_sort' => 'required',
            'return_date' => 'required|date',
        ]);

        try {
            DB::beginTransaction();
            $dimRequestGeneral = DimRequestGeneralModel::create([
                'client_id' => $validatedData['client_id'],
                'name' => $validatedData['name'],
                'status' => $validatedData['status'],
            ]);

            $factRequestGeneral = FactRequestGeneralModel::create([
                'client_id' => $validatedData['client_id'],
                'request_general_id' => $dimRequestGeneral->id,
                'equipment_id' => $validatedData['equipment_id'],
                'return_date' => $validatedData['return_date'],
                'access_date' => $validatedData['access_date'],
                'access_time' => $validatedData['access_time'],
                'access_sort' => $validatedData['access_sort'],
            ]);


            DB::commit();

            $lineNotifyModel = new AppLineNotifyModel;
            $lineNotifyModel->notify_message("มีการเพิ่ม คำข้อทั่วไป เลขที่ #". $dimRequestGeneral['id']);

        } catch (\Exception $th) {
            Log::info($th);
            DB::rollBack();
            // Log::info($th);

            return response()->json(array(
                'code'      =>  404,
                'message'   =>  'can not insert data'
            ), 404);
        }

        return response()->json(compact(['dimRequestGeneral', 'factRequestGeneral']));
    }

    public function factStore(Request $request)
    {
        $validatedData = $request->validate([
            'client_id' => 'required',
            'request_general_id' => 'required',
            'equipment_id' => 'required',
            'access_date' => 'required|date',
            'access_time' => 'required|integer',
            'access_sort' => 'required',
            'return_date' => 'required|date',
        ]);

        try {
            DB::beginTransaction();

            $factRequestGeneral = FactRequestGeneralModel::create([
                'client_id' => $validatedData['client_id'],
                'request_general_id' => $validatedData['request_general_id'],
                'equipment_id' => $validatedData['equipment_id'],
                'return_date' => $validatedData['return_date'],
                'access_date' => $validatedData['access_date'],
                'access_time' => $validatedData['access_time'],
                'access_sort' => $validatedData['access_sort'],
            ]);


            DB::commit();
        } catch (\Exception $th) {
            Log::info($th);
            DB::rollBack();
            // Log::info($th);

            return response()->json(array(
                'code'      =>  404,
                'message'   =>  'can not insert data'
            ), 404);
        }

        return response()->json(compact(['factRequestGeneral']));
    }

    public function edit($id)
    {
        $user = DimRequestGeneralModel::findOrFail($id);

        return response()->json([
            'user' => $user,
        ]);
    }

    public function update(Request $request, $id)
    {
        if($request['status'] == "Approve" || $request['status'] == "Rejected") {
            $validatedData = $request->validate([
                'name' => 'required',
                'status' => 'required',
                'approval_user_id' => 'required',
                'approval_timestamp' => 'required',
            ]);

            $data = [
                'name' => $validatedData['name'],
                'status' => $validatedData['status'],
                'approval_user_id' => $validatedData['approval_user_id'],
                'approval_timestamp' => $validatedData['approval_timestamp'],
            ];

            $lineNotifyModel = new AppLineNotifyModel;
            $lineNotifyModel->notify_message("คำข้อทั่วไป เลขที่ #". $id . " ได้รับการอนุมัติแล้ว");
        }else {
            $validatedData = $request->validate([
                'name' => 'required',
                'status' => 'required'
            ]);

            $data = [
                'name' => $validatedData['name'],
                'status' => $validatedData['status']
            ];

            if($validatedData['status'] == 'Rejected') {
                $lineNotifyModel = new AppLineNotifyModel;
                $lineNotifyModel->notify_message("คำข้อทั่วไป เลขที่ #". $id . " ถูกปฏิเสธ");
            }
        }

        $dimRequestGeneralModel = DimRequestGeneralModel::where('id', $id)->update($data);

        return response()->json($dimRequestGeneralModel);
    }

    public function updateAudit(Request $request, $id)
    {
        $validatedData = $request->validate([
            'auditor_user_id' => 'required',
            'audit_timestamp' => 'required'
        ]);

        $dimRequestGeneralModel = DimRequestGeneralModel::where('id', $id)->update([
            'status' => 'Auditor',
            'auditor_user_id' => $validatedData['auditor_user_id'],
            'audit_timestamp' => $validatedData['audit_timestamp'],
        ]);

        return response()->json($dimRequestGeneralModel);
    }

    public function destroy($id)
    {
        DimRequestGeneralModel::destroy($id);
        return response()->json('delete successfully' . $id);
    }
    public function destroy_select(Request $request)
    {
        DimRequestGeneralModel::destroy($request->foo);

        return response()->json([
            'data' => $request->foo,
        ]);
    }
}
