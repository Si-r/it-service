<?php

namespace App\Http\Controllers\Api\Modify;

use App\Http\Controllers\Controller;
use App\Models\DimModifyModel;
use App\Models\DimUserModel;
use App\Models\FactModifyModel;
use Illuminate\Http\Request;

class ModifyController extends Controller
{
    public function index(Request $request)
    {
        $client_id = $request->header('User');
        if($client_id != null) {
            $dimUser = DimUserModel::where(['id' => $client_id])->first();

            if($dimUser['user_right'] == "admin" || $dimUser['user_right'] == "staff") {
                $getdata = DimModifyModel::with(['contact', 'factModify'])->whereNull('deleted_at')->get();
            }else {
                $getdata = DimModifyModel::with(['contact', 'factModify'])
                    ->where('client_id', $client_id)
                    ->whereNull('deleted_at')->get();
            }
        }else {
            $getdata = DimModifyModel::with(['contact', 'factModify'])
            ->whereNull('deleted_at')->get();
        }
        
        for ($i = 0; $i < count($getdata); $i++) {
            $getdata[$i]['full_name'] = $getdata[$i]->contact->first_name . ' ' . $getdata[$i]->contact->last_name;
        }

        return $getdata->toJson();
    }

    public function indexWelcome()
    {
        $getdata = DimModifyModel::with(['contact', 'factModify'])
            ->orderBy('created_at', 'DESC')
            ->limit(10)
            ->whereNull('deleted_at')
            ->get();

        for ($i = 0; $i < count($getdata); $i++) {
            $getdata[$i]['full_name'] = $getdata[$i]->contact->first_name . ' ' . $getdata[$i]->contact->last_name;
        }

        return $getdata->toJson();
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'client_id' => 'required',
            'name' => 'required',
            'fact_contact_id' => 'required',
        ]);

        $datainsert = DimModifyModel::create([
            'client_id' => $validatedData['client_id'],
            'name' => $validatedData['name'],
            'contact_id' => $validatedData['fact_contact_id'],

        ]);

        return response()->json(compact('datainsert'));
    }

    public function factStore(Request $request)
    {
        $validatedData = $request->validate([
            'modify_id' => 'required',
            'client_id' => 'required',
            'comment' => 'required',
            'price' => 'required',
        ]);

        $datainsert = FactModifyModel::create([
            'modify_id' => $validatedData['modify_id'],
            'client_id' => $validatedData['client_id'],
            'comment' => $validatedData['comment'],
            'price' => $validatedData['price'],
        ]);

        return response()->json(compact('datainsert'));
    }

    public function edit($id)
    {
        $data = DimModifyModel::with(['contact', 'factModify'])->findOrFail($id);

        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'client_id' => 'required',
            'name' => 'required',
            'contact_id' => 'required',
        ]);

        $user = DimModifyModel::where('id', $id)->update([
            'client_id' => $validatedData['client_id'],
            'name' => $validatedData['name'],
            'contact_id' => $validatedData['contact_id'],
        ]);
    }

    public function destroy($id)
    {
        DimModifyModel::destroy($id);
        return response()->json('delete successfully' . $id);
    }

    public function destroy_select(Request $request)
    {
        DimModifyModel::destroy($request->foo);

        return response()->json([
            'data' => $request->foo,
        ]);
    }
}
