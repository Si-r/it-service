<?php

namespace App\Http\Controllers\Api\Permission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PermissionModel;
use Illuminate\Support\Facades\DB;


class PermissionController extends Controller
{



  public function index()
  {
      $getdata = DB::table('permission')->whereNull('deleted_at')->get();

      return $getdata->toJson();
  }
    public function getPermission(Request $request)
    {
        $getpermission = DB::table('permission')->where('id',$request->permission_id)->first();

        return response()->json(compact('getpermission'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
          'permission_name' => 'required|unique:permission|max:255',

   ]);

      $insert =   PermissionModel::insertGetId([
     'permission_name' => $validatedData['permission_name'],

   ]);

      return response()->json($insert);
    }

    public function edit($id)
    {
        $listdata = DB::table('permission')->whereNull('deleted_at')->get();

        $user = PermissionModel::findOrFail($id);

        return response()->json([
            'user' => $user,
        ]);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
          'permission_name' => 'required|max:255',


   ]);

        PermissionModel::findOrFail($id)
            ->update([
              'permission_name' => $validatedData['permission_name'],

          ]);
    }

    public function destroy($id)
    {
      PermissionModel::destroy($id);
      return response()->json('delete successfully'.$id);
    }

    public function destroy_select(Request $request)
    {
      PermissionModel::destroy($request->foo);

      return response()->json([
     'data' => $request->foo,
   ]);
  }
}
