<?php

namespace App\Http\Controllers\api\permission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DimRolePermissionModel;
use Illuminate\Support\Facades\DB;



class RolePermissionController extends Controller
{

  public function index()
  {
      $getdata = DB::table('dim_role_permission')->whereNull('deleted_at')->get();

      return $getdata->toJson();
  }

  public function getRolePermission($id)
  {


      $role = DB::table('dim_role_permission')->where('permission_id', $id)->get();

      return response()->json($role);
  }



  public function store(Request $request)
  {
      $validatedData = $request->validate([
        'permission_id' => 'required',
        'role' => 'required',

 ]);

 for($x = 0; $x < count($request->role); $x++){
   DimRolePermissionModel::create([
  'permission_id' => $validatedData['permission_id'],
  'role' => $request->role[$x],
]);
}


    return response()->json($request);
  }

  public function update(Request $request, $id)
  {

    $deleteID = DB::table('dim_role_permission')->where('permission_id',$id)->get();

    $array = [];

    for($i = 0; $i < count($deleteID); $i++){
      array_push($array,$deleteID[$i]->id);

    }

    //
    DimRolePermissionModel::destroy($array);
    for($k = 0; $k < count($array); $k++){
      $task = DimRolePermissionModel::withTrashed()->findOrFail($array[$k]);
  if(!$task->trashed()){
    $task->delete();
  }
  else {
    $task->forceDelete();
  }
    }

    // DimRolePermissionModel::forceDelete($array);


    $validatedData = $request->validate([
      'permission_id' => 'required',
      'role' => 'required',

]);

for($x = 0; $x < count($request->role); $x++){
 DimRolePermissionModel::create([
'permission_id' => $validatedData['permission_id'],
'role' => $request->role[$x],
]);
}


  return response()->json($array);

  }

}
