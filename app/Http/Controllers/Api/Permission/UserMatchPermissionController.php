<?php

namespace App\Http\Controllers\Api\Permission;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserpermissionModel;
use Illuminate\Support\Facades\DB;


class UserMatchPermissionController extends Controller
{


  public function GetPermissionByUsername(Request $request)
  {
    $Permission = DB::table('userpermission')->where('username', $request->username)->first();

    return response()->json($Permission);
  }

}
