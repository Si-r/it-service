<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\DimUserModel;
use App\Models\FactUserModel;
use App\Models\UserpermissionModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
//use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{

    public function index()
    {
        $getdata = DB::table('users')->whereNull('deleted_at')->get();
        return $getdata->toJson();
    }

    public function profile($id)
    {
        $getdata = FactUserModel::where(['user_id' => $id])->whereNull('deleted_at')->first();
        return $getdata->toJson();
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6',
            'email' => 'required|string|email|max:255|unique:users',
            'is_block' => 'required',
            'user_right' => 'required',
            'image_show' => 'required',
            'image_id' => 'required',
            'client_id' => 'required',
            'permission_id' => 'required',
        ]);

        $passwordhash = Hash::make($validatedData['password']);
        $hashed_email = Hash::make($validatedData['email']);

        $user = DimUserModel::create([
            'client_id' => $validatedData['client_id'],
            'username' => $validatedData['username'],
            'password' => $passwordhash,
            'email' => $validatedData['email'],
            'hashed_email' => $hashed_email,
            'image_id' => $validatedData['image_id'],
            'client_id' => $validatedData['client_id'],
            'permission_id' => $validatedData['permission_id'],
            'is_block' => $validatedData['is_block'],
            'user_right' => $validatedData['user_right'],
        ]);

        $userpermission = UserpermissionModel::create([
            'username' => $validatedData['username'],
            'permission_id' => $validatedData['permission_id'],
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user', 'token'), 201);
    }

    public function login(Request $request)
    {
        $credentials = $request->json()->all();
        $checkusername = DimUserModel::where('username', '=', $request->username)->get();
        if ($checkusername !== null) {
            $checkpassword = Hash::check($request->password, $checkusername[0]->password);
            if ($checkpassword) {
                try {
                    // attempt to verify the credentials and create a token for the user
                    if (!$token = JWTAuth::attempt(['username' => $request->username, 'password' => $request->password, 'permission_id' => $checkusername[0]->permission_id])) {
                        return response()->json(['error' => 'invalid_username/passoword'], 401);
                    }
                } catch (JWTException $e) {
                    // something went wrong whilst attempting to encode the token
                    return response()->json(['error' => 'could_not_create_token'], 500);
                }
                DimUserModel::where('id', $checkusername[0]->id)
                    ->update(['is_block' => 'unblock']);
                return response()->json(['success' => true, 'data' => ['token' => $token]]);
            }
            return response()->json(['error' => 'invalid password']);
        }
    }

    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    public function edit($id)
    {
        $user = DimUserModel::findOrFail($id);
        return response()->json([
            'user' => $user,
        ]);
    }

    public function update(Request $request, $id)
    {
        if ($request->password == '') {
            $validatedData = $request->validate([
                'username' => 'required|string|max:255|',
                'email' => 'required|string|email|max:255|',
                'is_block' => 'required',
                'user_right' => 'required',
                'image_show' => 'required',
                'image_id' => 'required',
                'client_id' => 'required',
                'permission_id' => 'required',

            ]);
            $hashed_email = Hash::make($validatedData['email']);
            $user = DimUserModel::where('id', $id)->update([
                'client_id' => $validatedData['client_id'],
                'username' => $validatedData['username'],
                'email' => $validatedData['email'],
                'hashed_email' => $hashed_email,
                'image_id' => $validatedData['image_id'],

                'permission_id' => $validatedData['permission_id'],
                'is_block' => $validatedData['is_block'],
                'user_right' => $validatedData['user_right'],

            ]);
            $userpermission = UserpermissionModel::where('username', $validatedData['username'])->update([
                'permission_id' => $validatedData['permission_id'],
            ]);

        } else {
            $validatedData = $request->validate([
                'username' => 'required|string|max:255|',
                'password' => 'required|string|min:6',
                'email' => 'required|string|email|max:255|',
                'is_block' => 'required',
                'user_right' => 'required',
                'image_show' => 'required',
                'image_id' => 'required',
                'client_id' => 'required',
                'permission_id' => 'required',
            ]);
            $passwordhash = Hash::make($validatedData['password']);
            $hashed_email = Hash::make($validatedData['email']);
            $user = DimUserModel::where('id', $id)->update([
                'client_id' => $validatedData['client_id'],
                'username' => $validatedData['username'],
                'password' => $passwordhash,
                'email' => $validatedData['email'],
                'hashed_email' => $hashed_email,
                'image_id' => $validatedData['image_id'],
                'permission_id' => $validatedData['permission_id'],
                'is_block' => $validatedData['is_block'],
                'user_right' => $validatedData['user_right'],
            ]);

            $userpermission = UserpermissionModel::where('id', $validatedData['username'])->update([
                'permission_id' => $validatedData['permission_id'],
            ]);

        }
    }
    public function updateProfile(Request $request, $id)
    {
        $validatedData = $request->validate([
            'firstName' => 'required|string|max:255|',
            'lastName' => 'required|string|max:255|',
            'nickName' => 'required',
            'telephone' => 'required',
            'telephoneEmergency' => 'required',
        ]);

        $factUserModel = FactUserModel::where('user_id', $id)->first();
        if ($factUserModel == null) {
            $factUser = FactUserModel::create([
                'user_id' => $id,
                'first_name' => $validatedData['firstName'],
                'last_name' => $validatedData['lastName'],
                'nick_name' => $validatedData['nickName'],
                'telephone' => $validatedData['telephone'],
                'telephone_emergency' => $validatedData['telephoneEmergency'],
            ]);
        } else {
            $factUser = FactUserModel::where('user_id', $id)->update([
                'first_name' => $validatedData['firstName'],
                'last_name' => $validatedData['lastName'],
                'nick_name' => $validatedData['nickName'],
                'telephone' => $validatedData['telephone'],
                'telephone_emergency' => $validatedData['telephoneEmergency'],
            ]);
        }

        return response()->json($factUser);
    }

    public function destroy($id)
    {
        DimUserModel::destroy($id);
        return response()->json('delete successfully' . $id);
    }
    public function destroy_select(Request $request)
    {
        DimUserModel::destroy($request->foo);

        return response()->json([
            'data' => $request->foo,
        ]);
    }
}
