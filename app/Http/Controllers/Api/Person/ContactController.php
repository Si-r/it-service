<?php

namespace App\Http\Controllers\Api\Person;

use App\Http\Controllers\Controller;
use App\Models\DimPersonContactModel;
use App\Models\DimUserModel;
use App\Models\FactPersonContactModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        $client_id = $request->header('User');
        if($client_id != null) {
            $dimUser = DimUserModel::where(['id' => $client_id])->first();

        if ($dimUser['user_right'] == "admin" || $dimUser['user_right'] == "staff") {
            $getdata = DimPersonContactModel::with([
                'factPersonContact',
                'client',
                'dimPersonResponsible',
                'dimCompany',
            ])->whereNull('deleted_at')->get();
        }else {
            $getdata = DimPersonContactModel::with([
                'factPersonContact',
                'client',
                'dimPersonResponsible',
                'dimCompany',
            ])->where('client_id', $client_id)
            ->whereNull('deleted_at')->get();
        }

        }else {
            $getdata = DimPersonContactModel::with([
                'factPersonContact',
                'client',
                'dimPersonResponsible',
                'dimCompany',
            ])->whereNull('deleted_at')->get();
        }
        
        for ($i = 0; $i < count($getdata); $i++) {
            $getdata[$i]['person_responsible_full_name'] = $getdata[$i]->dimPersonResponsible->fullName();
        }

        return $getdata->toJson();
    }

    public function personContact()
    {
        $getdata = DimPersonContactModel::whereNull('deleted_at')->get();

        return $getdata->toJson();
    }

    public function personContactByCompany($id)
    {
        $getdata = DimPersonContactModel::with([
            'factPersonContact'
        ])->where([
            'company_id' => $id,
        ])->whereNull('deleted_at')->get();

        return $getdata->toJson();
    }

    public function factPersonContact($id)
    {
        $getdata = FactPersonContactModel::where([
            'person_contact_id' => $id,
        ])->get();

        return $getdata->toJson();
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'client_id' => 'required',
            'company_id' => 'required',
            'person_responsible_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'nick_name' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'id_card' => 'required',
            'id_employee' => 'required',
            'location_id' => 'required',
        ]);

        // Log::info($validatedData);

        try {
            DB::beginTransaction();
            $dimPersonContact = DimPersonContactModel::create([
                'client_id' => $validatedData['client_id'],
                'company_id' => $validatedData['company_id'],
                'person_responsible_id' => $validatedData['person_responsible_id'],
            ]);

            $factPersonContact = FactPersonContactModel::create([
                'client_id' => $validatedData['client_id'],
                'person_contact_id' => $dimPersonContact->id,
                'document_number' => 1,
                'first_name' => $validatedData['first_name'],
                'last_name' => $validatedData['last_name'],
                'nick_name' => $validatedData['nick_name'],
                'telephone' => $validatedData['telephone'],
                'email' => $validatedData['email'],
                'id_card' => $validatedData['id_card'],
                'id_employee' => $validatedData['id_employee'],
                'location_id' => $validatedData['location_id'],
            ]);
            DB::commit();
        } catch (\Exception $th) {
            DB::rollBack();
            // Log::info($th);

            return response()->json(array(
                'code' => 404,
                'message' => 'can not insert data',
            ), 404);
        }

        return response()->json(compact(['dimPersonContact', 'factPersonContact']));
    }

    public function factStore(Request $request)
    {
        $validatedData = $request->validate([
            'client_id' => 'required',
            'person_contact_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'nick_name' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'id_card' => 'required',
            'id_employee' => 'required',
            'location_id' => 'required',
        ]);

        Log::info($validatedData);

        try {
            DB::beginTransaction();
            $factPersonContact = FactPersonContactModel::create([
                'client_id' => $validatedData['client_id'],
                'person_contact_id' => $validatedData['person_contact_id'],
                'document_number' => 1,
                'first_name' => $validatedData['first_name'],
                'last_name' => $validatedData['last_name'],
                'nick_name' => $validatedData['nick_name'],
                'telephone' => $validatedData['telephone'],
                'email' => $validatedData['email'],
                'id_card' => $validatedData['id_card'],
                'id_employee' => $validatedData['id_employee'],
                'location_id' => $validatedData['location_id'],
            ]);
            DB::commit();
        } catch (\Exception $th) {
            Log::info($th);
            DB::rollBack();
            // Log::info($th);

            return response()->json(array(
                'code' => 404,
                'message' => 'can not insert data',
            ), 404);
        }

        return response()->json(compact(['dimPersonContact', 'factPersonContact']));
    }

    public function edit($id)
    {
        $getdata = DimPersonContactModel::with([
            'factPersonContact',
            'client',
            'dimPersonResponsible',
            'dimCompany',
        ])->whereNull('deleted_at')->findOrFail($id);

        $getdata['person_responsible_full_name'] = $getdata->dimPersonResponsible->fullName();

        return $getdata->toJson();
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'client_id' => 'required',
            'company_id' => 'required',
            'person_responsible_id' => 'required',
        ]);

        $user = DimPersonContactModel::where('id', $id)->update([
            'client_id' => $validatedData['client_id'],
            'company_id' => $validatedData['company_id'],
            'person_responsible_id' => $validatedData['person_responsible_id'],
        ]);
    }

    public function destroy($id)
    {
        DimPersonContactModel::destroy($id);
        return response()->json('delete successfully' . $id);
    }

    public function factdelete($id)
    {
        FactPersonContactModel::destroy($id);
        return response()->json('delete successfully' . $id);
    }

    public function destroy_select(Request $request)
    {
        DimPersonContactModel::destroy($request->foo);

        return response()->json([
            'data' => $request->foo,
        ]);
    }
}
