<?php
namespace App\Library;

class LineNotifyModel {
    private $line_api = "https://notify-api.line.me/api/notify";
    private $token = "HBmFKdm6GhEvSzrVLVzFffsgW9ycqFeFTgfT3CZf5ym"; //ใส่Token ที่copy เอาไว้
    private $str = ""; //ข้อความที่ต้องการส่ง สูงสุด 1000 ตัวอักษร

    function notify_message($message){
        $queryData = array('message' => $message);
        $queryData = http_build_query($queryData,'','&');
        $headerOptions = array(
            'http'=>array(
                'method'=>'POST',
                'header'=> "Content-Type: application/x-www-form-urlencoded\r\n"
                ."Authorization: Bearer ".$this->token."\r\n"
                ."Content-Length: ".strlen($queryData)."\r\n",
                'content' => $queryData
            ),
        );
        $context = stream_context_create($headerOptions);
        $result = file_get_contents($this->line_api,FALSE,$context);
        $res = json_decode($result);

        return $res;
    }
}
