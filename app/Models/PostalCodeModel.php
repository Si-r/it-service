<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class PostalCodeModel extends Model
{
    use SoftDeletes;


    protected $table = 'postal_code';
    public $primaryKey = 'id';
    public $timestamps = true;

    protected $softDelete = true;

    protected $fillable = ['code', 'sub_district_id', 'district_id', 'province_id'];

    public function district()
    {
        return $this->hasOne(DistrictModel::class, 'id', 'district_id');
    }

    public function subDistrict()
    {
        return $this->hasOne(SubDistrictModel::class, 'id', 'sub_district_id');
    }

    public function province()
    {
        return $this->hasOne(ProvinceModel::class, 'id', 'province_id');
    }
}
