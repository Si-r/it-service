<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class DimAddressModel extends Model
{
    use SoftDeletes;


    protected $table = 'dim_address';
    public $primaryKey = 'id';
    public $timestamps = true;

    protected $softDelete = true;

    protected $fillable = [
            'province_id',
            'district_id',
            'sub_district_id',
            'postal_code_id',
    ];

    public function fullAddress()
    {
        $districtName       = $this->district->name;
        $subDistrictName    = $this->subDistrict->name;
        $provinceName       = $this->province->name;
        $postalCode         = $this->postalCode->code;

        $fullAddress = 'จังหวัด ' . $provinceName . ' อำเภอ '. $districtName . ' ตำบล '. $subDistrictName . ' รหัสไปรษณีย์ '.$postalCode;

        return $fullAddress;
    }

    public function province()
    {
        return $this->hasOne(ProvinceModel::class, 'id', 'province_id');
    }

    public function district()
    {
        return $this->hasOne(DistrictModel::class, 'id', 'district_id');
    }

    public function subDistrict()
    {
        return $this->hasOne(SubDistrictModel::class, 'id', 'sub_district_id');
    }

    public function postalCode() {
        return $this->hasOne(PostalCodeModel::class, 'id', 'postal_code_id');
    }
}
