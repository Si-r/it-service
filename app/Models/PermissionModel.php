<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PermissionModel extends Model
{
    use SoftDeletes;


    protected $table = 'permission';
    public $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = [
            'permission_name',
        
    ];

    protected $softDelete = true;



}
