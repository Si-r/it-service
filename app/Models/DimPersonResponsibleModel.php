<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class DimPersonResponsibleModel extends Model
{
    use SoftDeletes;


    protected $table = 'dim_person_responsible';
    protected $softDelete = true;

    public $timestamps = true;
    public $primaryKey = 'id';

    protected $fillable = [
        'client_id',
        'first_name',
        'last_name',
        'nick_name',
        'telephone',
        'email',
        'position',
        'id_card',
        'id_employee',
        'location_id',
        'company_id',
        'department_id',
    ];

    public function fullName()
    {
        $fullName = $this->first_name;
        $fullName .= " ";
        $fullName .= $this->last_name;

        return $fullName;
    }
    public function department()
    {
        return $this->hasOne(DimDepartmentModel::class, 'id', 'department_id');
    }

    public function company()
    {
        return $this->hasOne(DimCompanyModel::class, 'id', 'company_id');
    }

    public function location()
    {
        return $this->hasOne(DimLocationModel::class, 'id', 'location_id');
    }

    public function client()
    {
        return $this->hasOne(DimUserModel::class, 'id', 'client_id');
    }
}
