<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserpermissionModel extends Model
{
  use SoftDeletes;


  protected $table = 'userpermission';
  public $primaryKey = 'id';
  public $timestamps = true;

  protected $fillable = [
          'username',
          'permission_id',

  ];

  protected $softDelete = true;



}
