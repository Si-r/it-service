<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DimPersonContactModel extends Model
{
    use SoftDeletes;

    protected $table = 'dim_person_contact';
    protected $softDelete = true;

    public $primaryKey = 'id';
    public $timestamps = true;

    protected $fillable = ['client_id', 'company_id', 'person_responsible_id'];

    public function client()
    {
        return $this->hasOne(DimUserModel::class, 'id', 'client_id');
    }

    public function factPersonContact()
    {
        return $this->hasMany(FactPersonContactModel::class, 'person_contact_id', 'id');
    }

    public function dimPersonResponsible()
    {
        return $this->hasOne(DimPersonResponsibleModel::class, 'id', 'person_responsible_id');
    }

    public function dimCompany()
    {
        return $this->hasOne(DimCompanyModel::class, 'id', 'company_id');
    }
}
