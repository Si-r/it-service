<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DimRequestIssuesModel extends Model
{
    use SoftDeletes;


    protected $table = 'dim_request_issues';
    protected $softDelete = true;

    public $timestamps = true;
    public $primaryKey = 'id';

    protected $fillable = ['client_id',
            'name',
            'status',
            'auditor_user_id',
            'approval_user_id',
            'audit_timestamp',
            'approval_timestamp',
        ];

    public function client()
    {
        return $this->hasOne(DimUserModel::class, 'id', 'client_id');
    }

    public function auditorUser()
    {
        return $this->hasOne(DimUserModel::class, 'id', 'auditor_user_id');
    }

    public function approvalUser()
    {
        return $this->hasOne(DimUserModel::class, 'id', 'approval_user_id');
    }

    public function factRequestIssues()
    {
        return $this->hasMany(FactRequestIssuesModel::class, 'request_issues_id', 'id');
    }
}
