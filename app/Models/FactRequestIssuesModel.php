<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FactRequestIssuesModel extends Model
{
    use SoftDeletes;

    protected $table = 'fact_request_issues';
    protected $softDelete = true;

    public $timestamps = true;
    public $primaryKey = 'id';

    protected $fillable = [
        'client_id',
        'request_issues_id',
        'equipment_id',
        'issues_name',
        'issues_comment',
        'issues_level',
        'modify_id',
        'modify_status',
        'impact_id',
        'priority_id',
    ];

    public function dimRequestIssues()
    {
        return $this->hasOne(DimRequestGeneralModel::class, 'id', 'request_issues_id');
    }

    public function dimEquipment()
    {
        return $this->hasOne(DimEquipmentModel::class, 'id', 'equipment_id');
    }

    public function factEquipment()
    {
        return $this->hasMany(FactEquipmentModel::class, 'equipment_id', 'equipment_id');
    }

    public function modify()
    {
        return $this->hasOne(DimModifyModel::class, 'id', 'modify_id');
    }

    public function impact()
    {
        return $this->hasOne(DimImpactModel::class, 'id', 'impact_id');
    }

    public function priority()
    {
        return $this->hasOne(DimPriorityModel::class, 'id', 'priority_id');
    }
}
