<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DimRolePermissionModel extends Model
{
  use SoftDeletes;


  protected $table = 'dim_role_permission';
  public $primaryKey = 'id';
  public $timestamps = true;

  protected $fillable = [
          'permission_id',
          'role',
        
  ];

  protected $softDelete = true;
}
