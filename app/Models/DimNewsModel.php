<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class DimNewsModel extends Model
{
    use SoftDeletes;


    protected $table = 'dim_news';
    protected $softDelete = true;

    public $timestamps = true;
    public $primaryKey = 'id';

    protected $fillable = ['client_id',
            'setting_news_id',
            'name',
            'detail',
        ];

    public function client()
    {
        return $this->hasOne(DimUserModel::class, 'id', 'client_id');
    }

    public function factNews()
    {
        return $this->hasMany(FactNewsModel::class, 'id', 'news_id');
    }

    public function settingNews()
    {
        return $this->hasOne(SettingNewsModel::class, 'id', 'setting_news_id');
    }
}
