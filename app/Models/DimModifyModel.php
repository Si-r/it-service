<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class DimModifyModel extends Model
{
    use SoftDeletes;


    protected $table = 'dim_modify';
    protected $softDelete = true;

    public $timestamps = true;
    public $primaryKey = 'id';

    protected $fillable = ['client_id',
            'name',
            'contact_id',
        ];

    public function client()
    {
        return $this->hasOne(DimUserModel::class, 'id', 'client_id');
    }

    public function contact()
    {
        return $this->hasOne(FactPersonContactModel::class, 'id', 'contact_id');
    }

    public function factModify()
    {
        return $this->hasMany(FactModifyModel::class, 'modify_id', 'id');
    }
}
