<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuids;

class FactRequestGeneralModel extends Model
{
    use SoftDeletes;


    protected $table = 'fact_request_general';
    protected $softDelete = true;

    public $timestamps = true;
    public $primaryKey = 'id';

    protected $fillable = ['client_id',
            'request_general_id',
            'equipment_id',
            'return_date',
            'access_date',
            'access_time',
            'access_sort',
        ];

    public function client()
    {
        return $this->hasOne(DimUserModel::class, 'id', 'client_id');
    }

    public function dimRequestGeneral()
    {
        return $this->hasOne(DimRequestGeneralModel::class, 'id', 'request_general_id');
    }

    public function dimEquipment()
    {
        return $this->hasOne(DimEquipmentModel::class, 'id', 'equipment_id');
    }

    public function gactEquipment()
    {
        return $this->hasOne(FactEquipmentModel::class, 'equipment_id', 'equipment_id');
    }
}
